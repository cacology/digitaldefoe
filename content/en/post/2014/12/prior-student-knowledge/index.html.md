---
layout: post
title: "Prior Student Knowledge - Klein"
date: 2014-12-02
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

**Who would take the course? What did they know before entering the classroom? **

This course focused on introducing students to various aspects of literary feminist interpretation and research through the poetry of eighteenth-century women, primarily those from Great Britain. It was also one of the [goals of the course](http://digitaldefoe.org/?p=252) to introduce the students to eighteenth-century literature and culture more generally. It ran at Stony Brook University as a summer course, which meant it was constrained by a six-week time frame as well as by a lack of prerequisites for the course. The course officially ran as an upper-level, cross-listed English and Women's and Gender Studies course. I assumed correctly that for most of the students in the class, this course would be their first introduction to eighteenth-century literature and culture, and that their knowledge of the position and role of women and women writers would be equally small. The course content and structure was therefore designed to take these limitations into account and to address them as thoroughly as possible in the time allotted.

**What is the eighteenth century? What were the lives of eighteenth-century women like? **

The class size was relatively small; there were twelve students in the class, about half of whom were English majors. There were also a couple of Women's Studies majors, and about four students from various other disciplines, including psychology, biology, and engineering. This was not unexpected, as the course also functioned as an upper-level general education credit. It was important to me, in this case, to give the students an understanding of the eighteenth century and the position of women during this time.

Students were introduced to the eighteenth century through:

» instructor-led lectures on the time period and on pertinent aspects of the culture;
» documentary and narrative film on eighteenth-century subjects; and
» student research on individual eighteenth-century authors.

_Instructor-led Lectures_

Class usually began with short PowerPoint presentations (see examples for [Lady Mary Wortley Montagu](http://digitaldefoe.org/wp-content/files/teaching/Lady Mary Wortley Montagu.pdf), [Phillis Wheatley](http://digitaldefoe.org/wp-content/files/teaching/Phillis Wheatley.pdf), and [Anne Finch](http://digitaldefoe.org/wp-content/files/teaching/Anne Finch.pdf), attached) by the instructor on the time period, the position of women in eighteenth-century Britain, or the specific authors being studied that day. The presentations included images as well as bullet point information about the eighteenth century. This information included political, economic, social, medical, and everyday aspects of eighteenth-century life, mostly in England.

_Documentary and Narrative Film_

The course used two films to supplement student understanding of women in the eighteenth century. One was a film made to accompany an exhibit of eighteenth-century costumes at the Metropolitan Museum of Art, entitled _[The Eighteenth-Century Woman](https://www.youtube.com/watch?v=ObPDOe-yWfg)_. The other film was the much newer production, _[The Duchess](https://www.youtube.com/watch?v=FFg-oduezDs)_, which presents the life of Georgiana, Duchess of Devonshire, during the early years of her marriage to the Duke of Devonshire. The first film introduced students to the material culture of the eighteenth century as well as to a variety of important and powerful women to whom the costumes from the Met exhibit belonged. The second film illustrated the limits that even powerful, wealthy women faced in the eighteenth century. The Duchess was also appropriate as students read [poems by the Duchess of Devonshire](http://blog.catherinedelors.com/a-prayer-for-a-child-by-georgiana-duchess-of-devonshire/) prior to watching the film.

_Student Research on Individual Authors_

Students were also personally involved in supplementing their understanding of the eighteenth century and eighteenth-century women through biographical research on authors that they carried out during the course of the class. In the second half of the course, students conducted research on a poet of their choice and presented this information to the class. They were required to provide a visual aide, as the instructor had done in the first half of the course.

**Next Section: [Structure & Framework](http://digitaldefoe.org/?p=256)** . . .