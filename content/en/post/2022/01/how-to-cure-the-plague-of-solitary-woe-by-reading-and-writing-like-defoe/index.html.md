---
layout: post
title: "How to Cure the Plague of Solitary Woe by Reading and Writing like Defoe"
date: 2022-01-02
tags: ["Issue 13.1-Fall 2021","Reflections.2021","Issues"]
---

As the coronavirus pandemic escalated in the winter of 2020, I found myself reflecting on Mary Shelley's _The Last Man_ (1826) as a classic of epidemic literature in the tradition of Daniel Defoe's _A Journal of the Plague Year_ (1722). Prior to the pandemic, I had thought of her second great work of "political science fiction" after _Frankenstein_ (1818) in other terms. Her novel about a global plague that triggers a near-extinction event for humanity then seemed to me to be Shelley's unwieldy metaphor for all forms of artificially-made disaster, not a realistic literary depiction of the spread of a lethal contagion across national borders to overwhelm the whole world.

By mid-March 2020, I learned to read Shelley's "PLAGUE" on two levels at once: metaphorical and realistic (vol. 4, 139). Like _A Journal of the Plague Year_, on which it was partly based, _The Last Man _told a grimly realistic political story about how human beings catch and transmit infectious diseases as a result of the drama of their interpersonal and international conflicts. As I prepared to teach Defoe's novel as a source for Shelley's _poliscifi,_ I also began to appreciate the metaphorical layers of meaning in his fictional re-working of his uncle's journal of survival of the Great Plague of London of 1665-66.

A fictional version of his uncle Henry Foe, Defoe's narrator H.F. offers a poetic definition of plague near the end of his tale: "A Plague is a formidable Enemy, and is arm'd with Terrors, that every Man is not sufficiently fortified to resist, or prepar'd to stand the Shock against" (271). Plague, in this sense, is not so much a pathogen or a disease as it is a psychological test of the individual to face the worst and deepest of their fears. With this metaphor, Defoe returned to Biblical conceptions of plague, as found in Job and Exodus. In those ancient texts, plague is a metaphor for being beset by hardship, feeling the blows of fate, or suffering tragedy beyond one's control. The ten plagues of Egypt were not solely infectious diseases, but rather a range of terrifying and life-endangering hardships inflicted by the Hebrew God upon the Egyptian people to compel them to free the Israelites from slavery.

While it can be read on both levels-historical and literary-_A Journal of the Plague Year_ is ultimately a novel like Defoe's earlier classic, _Robinson Crusoe_ (1719). H.F., like the shipwrecked sailor Crusoe striving to escape from the cannibal island with the aid of purchased servants, endures the plague of solitary woe, but never faces up to the limits of his solidarity with other human sufferers. H.F. ended his months-long quarantine with the verse, "A dreadful plague in London was/ In the year sixty-five,/Which swept a hundred thousand souls/ Away; yet I alive!" (287). With this concluding quatrain, H.F. does not dwell so much on the immensity of the loss of life as he does the good luck of his own survival.

When Shelley read Defoe's _Plague Year_ in 1817, she may have found in its closing poem the narrative kernel for _The Last Man_. The ostensible sole survivor of her fictional global plague is Lionel Verney, who is an avatar for the author herself in this _roman à clef_. Like the young Shelley mourning the death of her husband Percy and three of their children, Verney suffers solitary woe in the extreme as do H.F. and Crusoe. But Verney survives the global plague to develop a truly solidaristic vision of his relationship with the whole planet and all of its life forms: he summons the hope that a new Adam and Eve might be out there, somewhere, already with child, and looking for others with whom to rebuild human society in a way that is humane.

While the solitary woe of Crusoe led him to escape from the cannibal island, and the solitary woe of H.F. pushed him to survive the plague intact, the solitary woe of Verney inspired him to set sail upon the sea: not only in search of other survivors, but also on a quest to discover a whole new way of life that might sustain the best in humanity while leaving the worst of its conflicts and other social diseases behind.

It is this global vision of solidarity with life itself that inspired me to ask my students at Notre Dame to emulate Defoe and Shelley during our plague year of 2020 to 2021. As part of my undergraduate and graduate seminars on political thought and plague literature, they wrote a series of 1000-word "[pandemoirs](https://sites.google.com/nd.edu/honorshumanitiesfall2020/newsletter)" (pandemic memoirs) that wove together their difficult and sometimes even harrowing experiences of infection, quarantine, contact tracing, social distancing, vaccination, isolation, depression, loss, and mourning, alongside their readings of classics of plague literature from Sophocles' _Oedipus Rex_ (429 BCE) to Margaret Atwood's _MaddAddam_ (2003-2013) and_ Handmaid's Tale_ (1985-2019) series.

In a poignant moment of communal reflection, my first-year students and I looked back on our plague year of Zoom seminars by sitting down for an international Zoom call with anthropologist Eben Kirksey. In early May, he taped the deeply personal interview with the students for public viewing on YouTube as part of his "[Multispecies Coronavirus Reading Group](https://www.youtube.com/watch?v=9QP3YaPVmrE)" series. The hour-long session provides a deep-dive into the psyches of the students, who had only recently been vaccinated in a mass campus drive, as they shared the real-life stories of resilience behind the personal and creative essays they wrote for our year-long humanities seminar. Though the technology had changed since Defoe and Shelley composed their plague memoirs with pen and paper, the purpose of our digitally-preserved _pandemoirs_ was the same: to cure the plague of solitary woe with the incisive power of words to tie humanity together through time, space, and the depths of sorrow.

University of Notre Dame

WORKS CITED

Botting, Eileen Hunt. [_Artificial Life After Frankenstein_](https://www.upenn.edu/pennpress/book/16165.html). Philadelphia: Penn Press, 2020.

---. "[Mary Shelley Created Frankenstein-And Then a Pandemic](https://www.nytimes.com/2020/03/13/opinion/mary-shelley-sc-fi-pandemic-novel.html)." _The New York Times_. 13 March 2020.

Defoe, Daniel. _A Journal of the Plague Year: Being Observations Or Memorials of the Most Remarkable Occurrences, as Well Publick as Private, Which Happened in London During the Last Great Visitation in 1665_. London: E. Nutt, 1722.

Shelley, Mary. _The Last Man_, in _The Novels and Selected Works of Mary Shelley_. Ed. Jane Blumberg with Nora Crook. London: Pickering & Chatto, (1996) 2001.

&nbsp;