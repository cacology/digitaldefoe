---
layout: post
title: "The Courtly Closet and the Closet of Devotion - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<span class="style1 style12">**Readings:
➢ Anthony Hamilton, _Memoirs of Count Grammont_, selections
➢ Edward Wettenhall, _Enter into thy Closet_, selections **</span>

&nbsp;
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="right" valign="top" width="27%">

<span class="style43 style44">[![Miss Hobart and Miss Temple](http://digitaldefoe.org/wp-content/files/teaching/bobker/11misshobart.png)](11misshobart.png) </span><span class="style1 style46">Fig. 11. C. Delort, _Miss Hobart and Miss Temple_.
From Anthony Hamilton, _Memoirs of the Count de Grammont_, Walter Scott, ed. (New York: Dutton, 1905).</span>

[![Miss Hobart and Miss Temple](http://digitaldefoe.org/wp-content/files/teaching/bobker/12misshobartmisstemple.png)](12misshobartmisstemple.png)
<span class="style1 style46">Fig. 12. L. Boisson after C. Delort, _Miss Hobart and Miss Temple._
From Anthony Hamilton, _Memoirs of Count Grammont_, Henry Vizetelly, ed. (London: Vizetelly, 1889).</span>

</td>
<td valign="top" width="73%">

<span class="style1 style12">Excerpts from Anthony Hamilton's _Memoirs of the Count Grammont_, a secret history of the Restoration court, and Edward Wettenhall's _Enter into thy Closet_, a frequently republished prayer manual, open up distinctive but overlapping modes of political and spiritual privacy: court favouritism and closet devotion. At court, decisions about when and to whom to grant access to the closet were exercises in arbitrary power and the status and roles of secretaries and other royal favorites were explicitly defined in relation to the closet. As one sixteenth-century secretary had put it: "To a _Closet_, there belongeth properly, a _doore_, a _locke_, and a _key_: to a _Secretorie_, there appertaineth incidently, _Honestie, Troth_, and _Fidelitie_." We consider the many examples of closet relations in Hamilton's _Memoirs_, focusing on (1) a funny and puzzling episode involving the Duchess of York, Miss Hobart (the Duchess's favourite), Miss Temple (the Duchess's favorite's favorite), and the Restoration's most notorious rake, the Earl of Rochester ([Figures 11](http://digitaldefoe.org/wp-content/files/teaching/bobker/11misshobart.png) and [12](http://digitaldefoe.org/wp-content/files/teaching/bobker/12misshobartmisstemple.png)), (2) the author's bond with his biographical subject, his brother-in-law Philibert de Comte de Gramont, and (3) the virtual transfer of favor to readers throughout this text and in the genre of secret history in general.</span>

We especially consider the politics of same-sex closet relations: Who gains what through relations of patronage and favoritism between people of the same sex? Under what circumstances and in what way do these relationships become erotic? What are the broader social and political implications of this kind of ambitious intimacy? At first glance, the prayer closet seems a very different space from the courtly closet. Satisfying the basic Protestant impulse to strip away Catholic mediations, the King James translation of the Bible (1611) gave a new specificity to the injunction to pray alone in Matthew 6.6: "But when thou prayest enter into thy Closet..." Along with new modes of self-examination, closet prayer formalized a special kind of closeness to God and Jesus. With reference to Wettenhall's manual, we parse out the key components of closet prayer and the interesting notions of time and timelessness associated with this practice. Wettenhall writes that the most powerful prayers belong to those "whose daily and frequent application of themselves to the throne of grace hath rendred them there well acquainted and favourites" (29). Students are asked to think about how the discourse of favouritism connects the prayer closet to the courtly closet. We also discuss the homoerotics of closet prayer with reference to Richard Rambuss's _Closet Devotions_, which argues that the prayer closet was an important site for the internalization of sexuality.

&nbsp;

**Suggested Presentation Topics:**

The history of court favoritism
The history of the secretary
The secret history and court memoir
The homoerotics of the prayer closet

&nbsp;

[![](http://digitaldefoe.org/wp-content/files/teaching/bobker/13.jpg)](13.jpg)

<span class="style47">Fig. 13: _Enter into thy Closet_.
Frontispiece from Edward Wettenhall, _Enter into thy Closet_</span>.

&nbsp;

&nbsp;</td>
</tr>
</tbody>
</table>
&nbsp;