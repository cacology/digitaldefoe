---
layout: post
title: "Final Student Assignment - Klein"
date: 2014-12-02
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

**Assignments**  

The final project for the course was creating a course wiki in which students would combine their biographical research on the author along with research on her literary accomplishments and some close readings of her poetry.   

The Wikipedia- style format would give students a sense of how [literary canons are formed](http://digitaldefoe.org/?p=256) through literary encyclopedias and what information author entries include and (sometimes) exclude. Students had to look up extant Wikipedia entries on their authors, read them, analyze their weaknesses, and then write their own version. The course assignments leading up to the final project were designed to help students prepare materials for the wiki entry.   

The main assignments for the course were:   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	a researched, written biography of the author;   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	an oral presentation with a visual element about the author and her works;   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	an annotated bibliography in preparation for the wiki;   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	an online wiki entry for the author; and   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	a reflection paper about the wiki activity.   

The biography of the author was due half way through the course, and it functioned to prepare the students for the biographical section of the wiki and the oral presentation to the class on their author. The wiki entry was the final project for the course, and it incorporated research and literary analysis elements.   

**Why a Wiki?**   

I chose a wiki entry for the final course project because I felt that such a piece of writing would further engage the students in the precepts of feminist recovery and make clear to them how writers are made popular or marginalized in different time periods. Similarly, I felt that since many of the students were not English majors and even those who were had little to no experience with eighteenth-century literature, they would benefit from understanding how literature of the past can speak to contemporary readers through a digital medium.   

The idea of multimodal writing and of encouraging students to be "writer/designers" has met with great success in the composition classroom and is currently growing through a variety of digital humanities projects in literary studies. Digital Humanities projects in the eighteenth century currently include the [18th-Century Common](http://www.18thcenturycommon.org/), the digital exhibit "[What Jane Saw](http://www.whatjanesaw.org/rooms.php?location=NRNE)" as well as the recently-launched website [ABOPublic](http://www.aphrabehn.org/ABO/). While it was not within the scope of a six-week course to put together an entire website, I opted to use the course wiki section of our class Blackboard page to create our student-researched wiki.   

By engaging students with multimodal ways of writing, I hoped to encourage them to see literary study as a dynamic, ever-changing process of research and discovery. According to [Cheryl Ball and Ryan Moeller](http://ten.fibreculturejournal.org/wp-content/dynmed/ball_moeller/index.html), "this new [multimodal] version of the university...should value different models of learning and nontraditional academic literacies....The focus of communication would have to shift away from writing to include new media designing as a critical literacy composition practice." The wiki project embraced such a philosophy by combining traditional literary research with the online encyclopedia platform. Similarly, as Pamela Takayoshi and Cynthia L. Selfe point out, when students write in "internationally networked digital environments, texts must be able to carry meaning across geo-political, linguistic, and cultural borders, and so texts must take advantage of multiple semiotic channels" (2). One of the goals of the wiki project was to make students aware of the inadequacy of the existing wiki pages for these authors and the difficulty of making digital texts informative and unbiased.   

Similarly, it has been made clear through the work of various scholars that the medium of the wiki itself holds many possibilities for making students more aware of digital communication, the possibilities of modern scholarship, and the continuing gender bias in publicly-available knowledge. [Adeline Koh](http://www.hybridpedagogy.com/journal/introducing-digital-humanities-work-undergraduates-overview/) suggests that "Wikipedia editing trains students to think about what constitutes reliable information and what does not, which translates into their academic work." Similarly, using Wikipedia in gender-related projects can serve to make students more aware of "the gender gap in its [Wikipedia's] editors--the typical Wikipedia editor is a thirty-year-old, middle-class, English-speaking college-educated male" (Koh). Thus, our wiki project expanded student understanding of feminist approaches to literary studies and the vital need for such approaches.   

The course wiki functioned to make students more comfortable with new forms of literacy while engaging them personally in the project of feminist recovery and critical thinking. According to Elizabeth Dolly Weber, when students work on a course wiki, "[it] guides students to recast and reshape information rather than simply reading it, facilitates individualized research and critical thinking, and encourages students to think creatively and to work cooperatively and collaboratively in ways that are otherwise difficult to achieve in the classroom" (125). The class wiki for our summer course achieved all of these goals in a relatively short amount of time.   

**Author Wiki Entries**   

In many cases, the existing Wikipedia entries for the authors we studied in the course were extremely short and lacking in detail (such as the one for [Elizabeth Tollet](http://en.wikipedia.org/wiki/Elizabeth_Tollet)). If an author already had a relatively well-developed Wikipedia page (such as those of [Anna Laetitia Barbauld](http://en.wikipedia.org/wiki/Anna_Laetitia_Barbauld) or [Mary Robinson](http://en.wikipedia.org/wiki/Mary_Robinson_(poet))), then the students were charged with reading the existing post thoroughly and deciding, through their own research, what was missing, biased or under-developed. Finally, students wrote their own wiki entry on the authors using the Course Wiki tool in Blackboard. As a class, we studied the Wikipedia entries of well-known poets like [Alexander Pope](http://en.wikipedia.org/wiki/Alexander_Pope) and [William Wordsworth](http://en.wikipedia.org/wiki/William_Wordsworth) to get an idea of the general sections of a wiki entry. Together we narrowed the scope of the wiki to the following sections:   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	Short Introduction;   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	Short Biography;   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	Role in the Literary Eighteenth Century and Influence on Future Writers;   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	Discussion, with Examples, of Major Themes in Her Work;   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	Optional: Other Literary or Non-Literary Contributions to the Historical Record; and   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	References List (a minimum of 10 references, including both articles and books).   

Students were encouraged to include images and hyperlinks in their wikis to make the entries more interactive. I required the students who were working in pairs to split the sections evenly and to make the authorship of each section clear using their initials.  Toward the end of the course, students had time in the computer lab to upload all of their information to the Blackboard site. They then presented their wikis to the class on the last day of the course.   

The projects were graded on how thoroughly the students covered each section, the amount and quality of primary and secondary sources used, the depth of literary analysis, and the creativity shown in formatting the entry and in using images.   

The final part of the assignment was to write a short reflective paper in which students analyzed what they had learned about the process of researching their poet and what they had learned about the process of canon-formation. The idea for the reflection paper came from my experiences teaching composition classes, where the production of reflection papers are a fairly common practice. In the composition classroom, students use the reflection paper as a way of self-assessing and of verbalizing what they have learned in order to gain insight into where they improved as writers and what still lies ahead.   

Similarly, the reflection paper on eighteenth-century women poets was meant as a tool for students to describe their research process and what they had learned from it, as well as to reflect on the project of feminist recovery in a digital medium. The reflection paper encouraged students to consider the choices they had made for the content of their wiki and how it compared to the original Wikipedia page online. Additionally, it allowed me, as the instructor, to learn what methods the students had found the most useful in the classroom and what aspects of the final project were the most stimulating intellectually for the students.

             **Next Section: [Sample Student Wiki](http://digitaldefoe.org/?p=262)** . . .