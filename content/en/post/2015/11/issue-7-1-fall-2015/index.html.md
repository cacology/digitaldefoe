---
layout: post
title: "Issue 7.1, Fall 2015"
date: 2015-11-19
---

![](issue15draft1.png)
<div class="issue_title_block">
<div class="issue_title"></div>
<div class="issue_title"></div>
<div class="issue_title">Speed & Sensation</div>
<div class="issue_title">in the Early Eighteenth Century</div>
<div class="issue_title"></div>
<div class="issue_title"></div>
<div class="issue_subtitle"></div>
</div>
<div class="issue_home_headers">Features</div>
[catlist id=13 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Reviews</div>
[catlist id=15 customfield_display="article_author" customfield_display_name=no]