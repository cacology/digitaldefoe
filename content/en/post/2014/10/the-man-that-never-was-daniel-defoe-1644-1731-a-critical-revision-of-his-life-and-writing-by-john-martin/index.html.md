---
layout: post
title: "<em>The Man That Never Was: Daniel Defoe 1644-1731 -- A Critical Revision of His Life and Writing</em>, by John Martin"
date: 2014-10-29
tags: ["Issues","Issue 6.1 - Fall 2014","Reviews"]
---

      
_The Man That Never Was_ is John Martin's second biography of Daniel Defoe. His first, _Beyond Belief: The Real Life of Daniel Defoe_, received mixed reviews for its provocative and often unorthodox view of the life and works of Defoe. The point that Martin returns to in this second biography is that Defoe's year of birth was unknown until he discovered the writer's birth record, which indicates that the writer was born in 1644, some fifteen or sixteen years before his supposed birth year of 1659/1660.  Martin also pursued this claim in another book on Defoe: _Alien Come Home: The Story of Daniel Defoe's Missing Years, 1644-1680_.   

	In _The Man That Never Was_, Martin claims that Defoe was the son of Daniel Foe and that James Foe was his uncle, not his father, as has been posited by Defoe scholars (xi).  Martin explains that James Foe's brother Daniel took over the family farm in Northamptonshire after the death of their father, who was also named Daniel Foe. The biographer Frank Bastian, in _Defoe's Early Life_, notes that James Foe's brother Daniel married and mentions a family, though he claims his children were the writer's older first cousins.  Martin disagrees, arguing that since Daniel Foe was the oldest male in the family, tradition dictated that the first born male would be named after the father: hence Daniel Defoe the writer was his eldest son rather than his nephew.    

When we delve into this and other fascinating claims made by Martin, they soon begin to unravel, as J. A. Downie recently pointed out in "Defoe's Birth," a note in _The Scriblerian and the Kit-Cats_. First, Daniel was a very common name among the (De) Foe families of Northamptonshire, where Martin claims Daniel Defoe was born. It is, therefore, difficult to associate the writer with one of the several Daniel Foes listed in the records of that region. Second, although Martin suggests that all historical facts are open to "misunderstanding" and are "a complex matter" (xv), he does not adequately address or explain away critical documents associated with the writer.  This is evident time and time again, and I will offer four representative examples here.   

First, in a marriage license dated December 28, 1683, Defoe's age is given as about 24 years. For a man to state that he is fifteen or sixteen years younger than he is to his twenty-year-old wife and her wealthy father seems fanciful in the extreme.  Further, in 1726, Defoe writes the following as "Andrew Moreton, Esq" in the preface to _The Protestant Monastry_:   "[I] am _in my_ 67th _Year, almost worn out with Age and Sickness_" (vi).  If one deducts 67 years from 1726, we arrive at the year 1659 as Defoe's birth year.[1](#1)   Second, Martin tries to persuade us that Defoe did not attend Morton's Academy in or around 1676 because he was working in Brazil on his sugar plantations from 1663 to 1680, but he fails along the way to tackle the evidence in _The Present State of Parties_ (1712), in which Defoe mentions three "Western Martyres" from that academy, not by name but nickname: Kitt. Battersby, Young Jenkins, and Hewlin.  These are certainly the sorts of schoolboy nicknames that would have been used at Morton's Academy.  Third, according to Martin, Defoe visited North America from September 1699 to August 1700; however, during that time, Defoe was in London arranging a lease for a house at 87 Woolstaple Round, Westminster, which was on Christ's Hospital land (Peterson 325).  Fourth, Martin has Defoe visiting Brazil and Madagascar in 1705/1706 as does Crusoe in _The Father Adventures_ (1719).  However, it appears from Defoe's letters that he had returned to London from a long trip around England for Robert Harley on November 6, 1705, and was hard at work establishing a distribution network for his Review until the spring of 1706.  According to John Robert Moore, Defoe wrote _The Apparition of Mrs Veal_ (1706) on his return from this trip (168-69).   

Martin's claims about Defoe's origins and life also result in self-contradiction. For example, Martin argues that the first part of _Memoirs of a Cavalier_ (1720) should be read as allegorical autobiography. If this is the case, the dreams of the protagonist's mother, which she records in her prayer book, must be those of Defoe's own mother (xix).  In the invocation, the Cavalier mentions "several strange Dreams" his mother "had while she was with Child of her _second_ Son, which was myself" (1).  However, according to Martin's own chronology, Defoe was the _first_ son of Daniel and Ellene Foe (xxxi).  Martin does try to establish that Defoe's father was married before and had a son with his first wife, Defoe's half-brother Thomas King (53), but he later claims that Defoe had another older brother, Roger Fenwick, who served as Lieutenant-Colonel in Colonel Lockhart's Regiment of Foot; Fenwick is identified by Martin as the older brother reported in the opening pages of _Robinson Crusoe_ (1719), although he never provides any biographical evidence in support of this supposition (453).  While he states that he has made room for this brother in the family chart on page 417 of _Beyond Belief_, there is no such chart on that page. Martin goes to some lengths to connect Defoe with Fenwick, but lacking evidence, he settles for trying to convince the reader that the connection _could_ be possible.  Martin seems to miss the point that the allusion to Lockhart in _Robinson Crusoe_ serves a literary rather than a biographical purpose, with Defoe referring to Lockhart to connect Crusoe with the Commonwealth.   

Martin also pursues at length the possible connection of Defoe to William Penn, querying Defoe's connection with the Jacobites.  However, Martin's biography is so tainted by fiction, inaccuracies, and an unreliable chain-forging of evidence that any claims he makes on this topic are unconvincing. However, there is perhaps some measure of truth in Martin's assertion that the lives of Defoe's relatives, who clearly settled in Essex, inspired his works of fiction just as Henry Foe's journal may have led, in part, to the production of _A Journal of the Plague Year_ (1722).   

Martin has clearly carried out a lot of research in Northamptonshire and the United States, and he has made some effort to link those records to the life and writings of Daniel Defoe.  However, what he has not done is satisfactorily tackled the body of evidence that runs counter to his biographical conclusions.    

Sheldon Rogers  

    University of Portsmouth  

    **NOTES**  

    <a name="1" id="1"></a>1. I do not doubt, however, that Martin is correct in his assumption that Defoe could not have been born in 1659 as the _legitimate_ son of James Foe, as one of James's two daughters was born on June 19, 1659. 

**  

      WORKS CITED**

Bastian, Frank.  _Defoe's Early Life_.  London: Macmillan, 1981.  Print.   

Defoe, Daniel.  _The Letters of Daniel Defoe_.  Ed. George Harris Healey.  Oxford: Clarendon Press, 1955.  Print.   

---.  _Memoirs of a Cavalier_.  London: A. Bell and others, 1720.  Print.   

---.  _The Protestant Monastery_.  London: W. Meadows and others, 1727 [for 1726].  Print.   

Downie, J. A.  "Defoe's Birth."  _The Scriblerian and the Kit-Cats_ 45.2 (2013): 225-30.  Print.   

Martin, John.  _Alien Come Home: The Story of Daniel Defoe's Missing Years, 1644-1680_.  Sandy: Anglia Publishing, 2009.  Print.   

---.  _Beyond Belief: The Real Life of Daniel Defoe_.  Pembrokeshire: Accent Press, 2006.  Print.   

Moore, John Robert.  _Daniel Defoe: Citizen of the Modern World_.  Chicago: U of Chicago P, 1958.  Print.   

Mundy, P. D.  "The Ancestry of Daniel Defoe."  _Notes and Queries_ 197 (1952): 382-83. Print.   

Peterson, Spiro.  "Defoe and Westminster, 1696-1706."  _Eighteenth-Century Studies_ 12.3 (1979): 306-38.  Print.

    