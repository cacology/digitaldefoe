---
layout: post
title: "Issue 12.1, Fall 2020"
date: 2021-05-31
---

![Old Ouse Bridge, York ' Art UK](old-ouse-bridge-york-10361)
<div class="issue_home_headers">Distinguished Scholar Invited Essay</div>
[catlist id=46 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Select Proceedings of the Defoe Society Biennial Conference, York, UK, 2019</div>
[catlist id=48 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Features</div>
[catlist id=44 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Scholar's Comment</div>
[catlist id=47 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Reviews</div>
[catlist id=45 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Consider a Submission to _The Burney Journal_</div>
[![](BJ-Combined-Pages-July-20_cfp-300x196.jpg)](http://digitaldefoe.org/wp-content/uploads/2021/06/BJ-Combined-Pages-July-20_cfp.jpg)