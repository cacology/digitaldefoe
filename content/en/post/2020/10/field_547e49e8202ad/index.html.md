---
layout: post
title: "Custom Post Header Override"
date: 2020-10-21
---

a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:172:"Use this to completely override the header portion of any post (post title and author). Put custom HTML for banner-style post headers, like image-mapped navigation banners.";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}