---
layout: post
title: "Gaming the Golden Age of Piracy  |  Last Words"
date: 2015-11-04
tags: ["Issues","Features","Issue 7.1 - Fall 2015"]
---

**
Last Words**

The writers, perhaps unsurprisingly, make no explicit connections between the questions raised in the exchange and their eighteenth-century parallels. Players therefore most likely confront the linked issues of autonomy, interpretation, media, and culture as present rather than historical or historicized phenomena - just as readers of novels would have 250 years ago. The game's remediation of eighteenth-century phenomena, then, extends beyond history and subject matter; there is more to it than piracy, but as has long been the case, much depends on the user. Just as readers could read for sex and scandal, players can play for parkour and plunder. They can also, though, undertake the more diligent labor of following the guidance (or guides) that the texts themselves provide in order to sit in more thoughtful judgment of their content and the circumstances of their production. The elevation of the game requires the elevation of the player. Ironically, then, the most authentically eighteenth-century experiences of _Assassin's Creed IV_ may be those that take place outside rather than within the Animus.

Rhodes College

**
WORKS CITED**

Aarseth, Espen. _Cybertext: Perspectives on Ergodic Literature_. Baltimore, MD: Johns Hopkins UP, 1997. Print.

_Assassin's Creed IV: Black Flag_. Montreal: Ubisoft, 2013. Xbox 360.

Aubrey, John. "An Essay Towards the Description of the North Division of Wiltshire." _Wiltshire: The Topographical Collections of John Aubrey_. Ed. John Edward Jackson. Devizes: The Wiltshire Archaeological and Natural History Society, 1862. Print.

Barnes, Annette, and Jonathan Barnes. "Time Out of Joint: Some Reflections on Anachronism." _Journal of Aesthetics and Art Criticism_ 47.3 (1989): 253-61. Print.

Barry, Boubacar. _Senegambia and the Atlantic Slave Trade_. Cambridge: Cambridge UP, 1998. Print.

Beasley, Berrin, and Tracy Collins Standley. "Shirts vs. Skins: Clothing as an Indicator of Gender Role Stereotyping in Video Games." _Communication and Society _5 (2002): 279-93. Print.

Bialuschewski, Arne. "Black People under the Black Flag: Piracy and the Slave Trade on the West Coast of Africa, 1718-1723." _Slavery & Abolition: A Journal of Slave and Post-Slave Studies_ 29.4 (2008): 461-75. Print.

---. "Daniel Defoe, Nathaniel Mist, and the _General History of the Pyrates_." _Papers of the Bibliographical Society of America_ 98 (2004): 21-38. Print.

Bogost, Ian. _Unit Operations: An Approach to Videogame Criticism_. Cambridge, MA: MIT Press, 2006. Print.

Bolster, W. Jeffrey. _Black Jacks: African American Seamen in the Age of Sail_. Cambridge, MA: Harvard UP, 1997. Print.

Bond, Richard. E. "Piratical Americans: Representations of Piracy and Authority in Mid-Twentieth-Century Swashbucklers." _The Journal of American Culture_ 33.4 (2010): 309-21. Print.

Cassell, Justine, and Henry Jenkins, _From Barbie to Mortal Kombat: Gender and Computer Games_. Cambridge, MA: The MIT Press, 2000. Print.

Daston, Lorraine J. "The Factual Sensibility." _Isis_ 79.3 (1988): 452-67. Print.

"Did anybody else find James Kidd to be attractive? [SPOILER, SEQUENCE 5]." _Reddit_. Web. 15 Jul. 2015.

Dow, Douglas N. "Historical Veneers: Anachronism, Simulation, and Art History in _Assassin's Creed II_." _Playing With the Past: Digital Games and the Simulation of History_. Ed. Matthew Wilhelm Kapell and Andrew B. R. Elliot. London: Bloomsbury, 2013. 215-32. Print.

Dugaw, Diane. _Warrior Women and Popular Balladry, 1650-1850._ Chicago: U of Chicago P, 1996. Print.

Eastman, Carolyn. "Shivering Timbers: Sexing Up the Pirates in Early Modern Print Culture." _Common-Place _10.1 (2009). Web. 15 Jul. 2015.

Elliot, Andrew B.R., and Matthew Wilhelm Kapell. "Introduction: To Build a Past that Will 'Stand the Test of Time': Discovering Historical Facts, Assembling Historical Narratives." _Playing With the Past: Digital Games and the Simulation of History_. Ed. Matthew Wilhelm Kapell and Andrew Elliot. London: Bloomsbury Academic, 2013. 1-30. Print.

Entertainment Software Association (ESA). _Essential Facts about the Computer and Video Game Industry_. Washington, DC: ESA, April 2014. Web. 5 Jan. 2014.

Ferreira, Roquinaldo. "Slaving and Resistance to Slaving in West Central Africa." _The Cambridge World History of Slavery_. Ed. David Eltis and Stanley L. Engerman. Vol. 3. New York: Cambridge UP, 2011. 111-31. Print.

Furbank, P.N., and W. R. Owens. _The Canonisation of Daniel Defoe_. New Haven: Yale UP, 1988. Print.

Gaver, W. W., et al., "The Drift Table: Designing for Ludic Engagement." Proc. CHI EA '04. ACM Press (2004): 885-900. Print.

Huizinga, Johan. _Homo ludens_. New York: Beacon, 1971. Print.

Hunter, J. Paul. _Before Novels: The Cultural Contexts of Eighteenth-Century English Fiction_. New York: W. W. Norton, 1990. Print.

Jenkins, Henry. "Game Design as Narrative Architecture." _First Person: New Media as Story, Performance, and Game. _Ed. Noah Wardrip-Fruin and Pat Harrigan. Cambridge, MA: MIT Press, 2003. 118-30. Print.

Johnson, Charles. _A General History of the Pyrates_, 2nd ed. London: 1724. _Eighteenth Century Collections Online. _Web. 5 Jan. 2015.

Jones, Steven E. _The Meaning of Video Games: Gaming and Textual Strategies_. New York: Routledge, 2008. Print.

Juul, Jesper. "The Definitive History of Games and Stories, Ludology and Narratology." _The Ludologist, _22 February 2004. Web. 20 Oct. 2015.

Kain, Erik. "Watch the Terrible 4th of July 'Assassin's Creed III' Live-Action Trailer.'" _Forbe_s, 7 Apr. 2012. Web. 6 Feb. 2015.

Konstam, Angus. _Pirates: The Complete History from 1300 BC to the Present Day_. Guilford, CT: Lyons Press, 2011. Print.

Lear, Anna. "What's the point of buying art?" _Arqade_. StackExchange, 30 Oct. 2013. Web. 1 Aug. 2015.

Lichfield, John. "French left loses its head over Robespierre game," _The Independent,_ 17 Nov. 2014. Web. 6 Mar. 2015.

Linebaugh, Peter and Marcus Rediker. _The Many Headed Hydra: Sailors, Slaves, Commoners, and the Hidden History of the Revolutionary Atlantic_. Boston: Beacon Press Books, 2000. Print.

Lupton, Christina, and Peter McDonald. "Reflexivity as Entertainment: Early Novels and Recent Video Games." _Mosaic _43.4 (2010): 157-73. Print.

McKeon, Michael. "Generic Transformation and Social Change: Rethinking the Rise of the Novel." _Theory of the Novel: A Historical Approach_. Ed. Michael McKeon. Baltimore: The Johns Hopkins UP, 2000. 382-99. Print.

McLuhan, Marshall. _Understanding Media: The Extensions of Man_. Cambridge, MA: The MIT Press, 1964. Print.

Miller, Monica K., and Alicia Summers. "Gender Differences in Video Game Characters' Roles, Appearances, and Attire as Portrayed in Video Game Magazines." _Sex Roles_ 57 (2007): 733-42. Print.

Miller, Peter N. _Peiresc's Europe: Learning and Virtue in the Seventeenth Century_. New Haven: Yale UP, 2000. Print.

O'Driscoll, Sally. "The Pirate's Breasts: Criminal Women and the Meanings of the Body." _The Eighteenth Century_ 53.3 (2012): 357-79. Print.

Partridge, Stephanie. "The Incorrigible Social Meaning of Video Game Imagery." _Ethics & Information Technology_ 13.4 (2011): 303-12. Print.

Rediker, Marcus. _The Slave Ship: A Human History_. New York: Penguin Books, 2007. Print.

---. _Villains of All Nations: Atlantic Pirates in the Golden Age_. Boston: Beacon Press, 2004. Print.

Shaw, Adrienne. "What is Video Game Culture? Cultural Studies and Game Studies." _Games and Culture_ 5 (2010): 403-24. Print.

Suellentrop, Chris. "Slavery as New Focus for a Game." _The New York Times_, 27 Jan. 2014. Print.

Swann, Marjorie. _Curiosities and Texts: The Culture of Collecting in Early Modern England, _Philadelphia: U of Pennsylvania P, 2001. Print.

Sweet, Rosemary. _Antiquaries: The Discovery of the Past in Eighteenth-Century Britain_. London: Hambledon, 2004. Print.

Terlecki, Melissa, et al. "Sex Differences and Similarities in Video Game Experience, Preferences, and Self-Efficacy: Implications for the Gaming Industry." _Current Psychology_ 20.1 (2011): 22-33. Print.

Vine_, _Angus._ In Defiance of Time: Antiquarian Writing in Early Modern England_. Oxford: Oxford UP, 2010. Print.

Ubisoft. "Ubisoft FY14 Earnings Presentation," 15 May 2014. Web. 16 Jan 2015.

Warner, William. _Licensing Entertainment: The Elevation of Novel Reading in Britain, 1684-1750_. Los Angeles: U of California P, 1998. Print.

"Where does the art show up?" _Assassin's Creed IV: Black Flag_. GameFAQs, 2013. Web. 1 Aug. 2015.

Woodard, Colin. _The Republic of Pirates_: _Being the True and Surprising Story of the Caribbean Pirates and the Man Who Brought Them Down. _Orlando: Harcourt, 2007. Print.