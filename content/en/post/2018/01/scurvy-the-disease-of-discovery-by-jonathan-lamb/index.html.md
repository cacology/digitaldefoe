---
layout: post
title: "<em>Scurvy: The Disease of Discovery</em>, by Jonathan Lamb"
date: 2018-01-06
tags: ["Issues","Issue 9.1 - Fall 2017","Reviews"]
---

Stories about diseases are always popular. The details of bodily trauma can horrify yet rivet an audience while the methods of cure may amuse. Thus, a book such as Jonathan Lamb's _Scurvy: The Disease of Discovery_ attracts readers with its promise of the weird, horrible, and wonderful. Lamb's work does not disappoint.

As Lamb explains with scientific detail, scurvy is a deficiency disease, meaning it is caused by the absence of ascorbic acid, or vitamin C, in one's diet. Those without access to fresh fruits and vegetables were susceptible to scurvy, and historically those with the most documented cases of scurvy included sailors and others on extended sea travels. Indicators of scurvy include extreme weakness and fatigue, loss of teeth and foul breath, difficulty breathing, hemorrhages under the skin that can turn limbs black and blue, and pain in the joints as the collagen is lost. In fact, some eye-witness accounts of cited by Lamb describe the clacking of bones in scurvy patients. Death from scurvy was painful and frightening to observe in oneself and others as the body and even personality of the sufferer altered significantly.

In choosing scurvy as subject matter, Lamb inherited an archive of marvelous stories of exploration, illness, and science. His text is rife with accounts of mariners, poets, and medical men that highlight the unique language of scurvy. As a literary historian, Lamb brings an artful analytical eye to these texts, reading language and images (both textual and pictorial) as artifacts of the disease.

Lamb has the added challenge that anyone working with a wealth of archival materials has: finding the right spot for each source. Lamb steps and sometimes leaps from one rich account to another, creating what can at times be a dizzying reading experience, particularly when he asks the reader to bound from one historical period to a non-adjacent one within one paragraph. Of course, he makes no promise of following a historical trajectory, but such shifts without signals can be jarring for readers.

Undergirding Lamb's assemblage of scurvy textual curios is his argument that extended oceanic travels to explore and discover the world were also fundamentally endeavors to explore and discover the nature of and possible cures for scurvy. Thus, the effects of exploration-transoceanic trade, scientific knowledge, colonization, and slavery-have traces in scurvy, not just as an effect, but as a cause.

Sailors who were stricken with scurvy experienced a particular form of discovery as the signs of disease in their body were revealed to them. Likewise, in reading accounts of travel, readers can discover the physical and linguistic effects of scurvy. Lamb writes, "There is...always a possibility that a narrative of scurvy might itself exhibit signs of the disease, either being dulled by the bleakness and the pain, or brightened by its hallucinations" (61). The first-hand narratives, therefore, are extensions of the bodily experiences with the disease because of the way it heightens the senses, affects perception, and alters personality-all factors that can influence the content of an account. Therefore, Lamb explores the relationship these pathological intensities have in fiction and coins the term "scorbutic fiction" in his fourth chapter (223).

Drawing upon medical texts, medical philosophy, poetry, novels, accounts of journeys, and literary interpretations of them, Lamb discusses the aesthetics and what he calls the "genius" of scurvy-the word "genius," referring to the disease's inscrutable logic (9). Coleridge's _The Rime of the Ancient Mariner_ figures most prominently as a literary source in _Scurvy_. Other key literary texts and authors include Homer's _Odyssey_, Luis Vaz de Cameons's _The Lusiads, _Shakespeare's _Othello_, Milton's _Paradise Lost_, Francis Bacon's _New Atlantis_, accounts of expeditions with Captain Cook by Joseph Banks, Johann Reinhold Forster, and Robert Falcon Scott, as well as various works by Margaret Cavendish, Daniel Defoe, and Herman Melville-and that's the short list.

Rather than an extended analysis of these and other texts, Lamb's _Scruvy_ offers new insights on these works by relating them to the discourse of scurvy both medical and popular. For instance, one of the comments on Coleridge begins with a quotation from _Rime_: "heat and stench arising from diseased bodies rot the very planks." About this line, Lamb writes, "In the poem which owes so much to Coleridge's interest in the effects of scurvy, the decayed state of Death and Life-in-Death is answerable to the skeletal and disarticulated state of the spectre-bark on which they sail...In scorbutic voyages, this diseased equivalence between the ship and its company is often noticed" (50). The subsequent discussion includes other examples of ships thought to be "corrupted" by the presence of people sick with scurvy. It is not, therefore, an exhaustive analysis of the Coleridge.

In _Scurvy _Lamb also seeks to trouble the notion that sailors, doctors, and other scientists readily accepted the use of citrus fruits as a cure for scurvy. While the claim that citrus cured the disease circulated in the eighteenth century, it was still a disputed point (as innovative as medicine is and has been, new ideas can be challenged by tradition). Many believed the disease could be cured by malt wort, and others thought it was caused by corrupted or foul foods.

In fact, Lamb sees the citrus debate as a synecdoche of the intellectual battle in the eighteenth century between empiricists who based medical claims on practice and observation of actual cases and theorists who bandied with the latest concepts about bodily systems such as circulation and consumption. As a result, Lamb writes, "The history of scurvy is especially tormenting...being strewn with red herrings, false starts, and mistaken conjectures that mock all teleological symmetries" (34). While the same could be said for many other disease histories, Lamb is right that most readers will have a preformed notion of the history that hinges on the use of limes or lemons to cure scurvy. However, these same readers will approach the history of another disease, such as leprosy or yellow fever, with no such notions since that knowledge is more specialized.

Despite its historical significance in the age of exploration, scurvy has been the subject of only a few book-length works with varying target audiences. Stephen Bown's _Scurvy: How a Surgeon, a Mariner and a Gentleman Solved the Greatest Medical Mystery of the Age of Sail_ (referred to by Lamb) is less a cultural and literary history of the disease, as Lamb describes his own project, and more a popular history of scurvy in oceanic voyages in the eighteenth century. Kevin Brown's _Poxed and Scurvied_ focuses more generally on maritime health and medicine. Those interested in looking at archival works on scurvy may want to consult _Scurvy: Webster's Timeline History 1534-2007_, edited by Philip M. Parker, a bibliography of works on scurvy as early as the sixteenth century. Many of those referenced in this text and Lamb's bibliography can be accessed digitally through Early English Books Online, the Wellcome Library, and the United States National Library of Medicine.

Scurvy has not received nearly the same kind of critical attention that diseases such as smallpox and yellow fever have. Because of its more conceptual approach (again, it is not a historical narrative of scurvy), Lamb's book is different from anything else on scurvy and is, in fact, more akin to scholarship on disease in literature, for example Cristobal Silva's _Miraculous Plagues_, Priscilla Wald's _Contagious_, and David Shuttleton's _Smallpox and the Literary Imagination_. At the same time, it is unclear to what extent Lamb engages with other scholarly disease discourses, for his citations of contemporary scholarship focus more on the history of science (especially Stephen Shapin and Joyce Chapin) than medical or disease history.

Often the observations Lamb makes about scurvy also apply to the histories of other diseases. For instance, in the archives of smallpox, yellow fever, and tuberculosis, one will find similar disputes among medical scientists, the clergy, and laypeople concerning the definition, treatment, and containment of the disease in question. Lamb notes the impossibility of documenting a lived experience with scurvy that is legible and meaningful to readers because of the altered senses and perceptions of a scurvied individual. While this is beautifully argued and detailed in _Scurvy_, the same disorientation and sensitivities can also be seen with other diseases, such as smallpox and tuberculosis. Therefore, one has to wonder how his analysis might shift with consideration of scholarship on disease more broadly. But _Scurvy _is already a book crowded with references.

A truly exciting feature of this book is the notion of the aesthetics of scurvy or disease in general. Nonetheless, readers hoping to find a series of narratives about scurvy that trace a historical trajectory and provide an overview of scurvy in literature and culture might find this book challenging. Even specialists in historical diseases and their narratives may struggle with some of Lamb's analyses, organization, and sometimes competing central claims. Overall, _Scurvy _is an inventive, thoughtful, complex, robustly researched book-and generous, as it invites other scholars to delve into the primary sources and further the literary history of scurvy.

&nbsp;

Sarah Schuetze
University of Wisconsin-Green Bay

&nbsp;

WORKS CITED

Bown, Stephen. _Scurvy: How a Surgeon, a Mariner and a Gentleman Solved the Greatest Medical Mystery of the Age of Sail_. New York: St. Martin's Griffin, 2005.

Brown, Kevin. _Poxed and Scurvied: The Story of Sickness and Health at Sea_. Annapolis: Naval Institute P, 2011.

Chapin, Joyce E. _Subject Matter: Technology, the Body, and Science on the Anglo-American Frontier, 1500-1676_. Cambridge: Harvard UP, 2001.

Parker, Philip M., ed. _Scurvy: Webster's Timeline History 1534-2007_. Las Vegas: ICON, 2009.

Shapin, Stephen. _A Social History of Truth_. Chicago: U of Chicago P, 1994.

---. _Never Pure: Historical Studies of Science as if It Was Produced by People with Bodies, Situated in Time, Space, Culture, and Society, and Struggling for Credibility and Authority_. Baltimore: Johns Hopkins UP, 2010.

Shuttleton, David. _Smallpox and the Literary Imagination 1660-1820_. New York: Cambridge UP, 2007.

Silva, Cristobal. _Miraculous Plagues: An Epidemiology of Early New England Narrative._ New York: Oxford UP, 2011.

Wald, Priscilla. _Contagious: Cultures, Carriers, and the Outbreak Narrative_. Durham: Duke UP, 2008.