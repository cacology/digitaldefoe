---
layout: post
title: "Issue 13.1, Fall 2021"
date: 2022-01-03
---

[![Drawing of fever, allegorically represented as a beast, and ague, depicted as a blue wormlike creature. Ague is wrapped around the torso of a visibly ill figure seated by a fire on the left of the image. Fever stands in the center of the image holding its arms in the air. In the background on the right, a physician writes a prescription.](http://digitaldefoe.org/wp-content/uploads/2021/12/AgueFever-scaled.jpg)](AgueFever-scaled.jpg)
<div class="issue_home_headers">Reflections</div>
[catlist id=50 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Defoe in Performance</div>
[catlist id=54 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Notes</div>
<div>[catlist id=51 customfield_display="article_author" customfield_display_name=no]</div>
<div class="issue_home_headers">Reviews</div>
[![](http://digitaldefoe.org/wp-content/uploads/2022/01/Amulet.Image_.jpg)](Amulet.Image_.jpg)[catlist id=53 customfield_display="article_author" customfield_display_name=no]

&nbsp;

&nbsp;

* * *

<span style="font-size: small;">Image Credits:
Top: "Ague and Fever," Designed by James Dunthorne; Etched by Thomas Rowlandson, 1788. Image courtesy [Wellcome Collection](https://www.rct.uk/collection/810271/ague-and-fever).
Reviews Section: Detail of amulet and charm to protect against plague. Bavaria, German, 1690-1710. Image courtesy the [Science Museum Group Collection](https://coimages.sciencemuseumgroup.org.uk/images/2/880/large_A666092__0003_.jpg); used under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 Licence](https://creativecommons.org/licenses/by-nc-sa/4.0/).</span>
<div class="issue_home_headers">Consider a Submission to _The Burney Journal_</div>
[![](Burney-Journal-January-22-Ad-Color-2_1-230x300.jpg)](http://digitaldefoe.org/wp-content/uploads/2022/01/Burney-Journal-January-22-Ad-Color-2_1.jpg)