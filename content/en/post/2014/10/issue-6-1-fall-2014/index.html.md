---
layout: post
title: "Issue 6.1, Fall 2014"
date: 2014-10-29
---

![hogarth-v2](hogarth-v2.png)
<div class="issue_title_block">
<div class="issue_title">English Poetry, 1690 - 1720</div>
<div class="issue_subtitle">Andreas K. E. Mueller, Guest Editor</div>
</div>
<div class="issue_home_headers">Features</div>
[catlist id=6 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Pedagogies</div>
[catlist id=7 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Notes</div>
[catlist id=8 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Reviews</div>
[catlist id=9 customfield_display="article_author" customfield_display_name=no]