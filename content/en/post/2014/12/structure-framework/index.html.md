---
layout: post
title: "Structure & Framework - Klein"
date: 2014-12-02
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

**Course Format **
In my past courses, students at Stony Brook had expressed a wish to have greater individual choice of materials in the course. I decided to experiment with this possibility in my course on eighteenth-century women poets. I hoped that in allowing students to have some say in the texts we discussed, they might become more invested in the course and the [final assignment](http://digitaldefoe.org/?p=260). Therefore, I chose the authors for the first half of the course and the students chose the authors for the second half (though they chose from a list compiled by me ahead of time). Thus, in the first half of the semester, I picked out the poems, wrote the reading questions, presented the biographical information on these poets ([see below](#poets)) and led the class discussions.

In the second half of the course, after selecting a poet from the pre-selected list students then researched the author, presented on her biography, chose poems for their classmates to read and discuss, and prepared reading questions that the other students had to answer before coming to class. The poems and reading questions were vetted by me before being sent out to the rest of the class, but by and large students were successful in choosing poems that they found interesting and significant for class discussion. In this way, students were active in "editing" our anthology and class list. My usual input as instructor was to limit the number of poems, as students often chose too many for us to cover in one class period.
By giving the students choice over the syllabus content, students were actively encouraged to question the course syllabus and engage in a form of textual criticism, as they often looked for poems outside the anthology to include in the course reading. According to Erick Keleman, "the reasons to bring textual criticism into any classroom are to demystify textual media and thereby to increase students' ability to negotiate and interpret textual mediations" (122). Thus, by having students choose the poems themselves, they engaged in a kind of critical thinking that led them to question the traditional literature classroom and engage actively in feminist recovery of unanthologized poems.
Students also became emotionally and intellectually invested in the poets they chose as the poet became "theirs" through researching her. This sentiment was especially prominent in the case of the two groups that chose Hannah More and Ann Yearsley and ended up presenting the two sides of those women's relationship. In this case, again, the course goals of engaging students in a nuanced kind of feminist recovery project were attained. Jean Marsden warns that often female writers with "views we find distasteful" are neglected by feminist scholars (661). In the case of More and Yearsley, both were included in the syllabus and discussed by the students, and students could decide for themselves whether More's treatment of the impoverished and dependent Yearsley were warranted. The students were offered the chance to study women writers "from a wide range of educational, class, religious, and political backgrounds," therefore encouraging students to look at the "issues...that separate women rather than unite them" (Marsden 661).
Additionally, in the first half of the syllabus, I paired female writers with male contemporaries in order to make it clearer to students how the women of the time were in conversation with their male peers. This was an important addition to the course as it helped us avoid the problem of women's writing in anthologies, as described by Jeanne Moskal. Moskal argues that bringing women's voices into anthologies has resulted in "two versions of women writers' liminality...the women-only and the mixed-sex anthologies...[F]or the teacher the theoretical choice between integrationism and separatism takes the practical form of which textbooks to order and which poems and novels can be fitted into the syllabus" (2). Although I ended up choosing a [women's-only anthology](http://digitaldefoe.org/?p=258) for the course, I supplemented it with the works by male authors to avoid such "separatism" and to encourage classroom discussions that showed how women poets were central to the larger literary world of the eighteenth century.

**Course Poets** <a id="poets" name="poets"></a>

_Poets Chosen By Instructor:_
The poets I chose to teach in the first half of the syllabus were fairly canonical, as I wanted to insure that we would cover the most well-known female poets of the century. At the same time, however, I also made room to include Phillis Wheatley and Joanna Baillie to make sure that the course would feature authors from outside England as well as at least one author of color.
• Aphra Behn
• Anne Finch, Countess of Winchilsea
• Lady Mary Wortley Montagu
• Charlotte Smith
• Phillis Wheatley
• Joanna Baillie
• Georgiana, Duchess of Devonshire
_List of Poets the Students Could Choose From:_
I compiled a list of poets that the students could choose from so that they would not be overwhelmed by the choices in the Lonsdale anthology and to make sure that students ended up with a poet about whom they could find sources. In this class, the students ended up covering almost the entire list (the only poets not covered from this list were Elizabeth Thomas, Elizabeth Hands and Susanna Blamire).
• Sarah Egerton (née Fyge, later Field; 1670-1723)
• Elizabeth Thomas (1675-1731)
• Elizabeth Tollet (1694-1754)
• Mary Leapor (1722-1746)
• Anna Seward (1742-1809)
• Anna Laetitia Barbauld (1743-1825)
• Hannah More (1745 - 1833)
• Elizabeth Hands (1746-1815)
• Susanna Blamire (1747-94)
• Ann Yearsley (1752-1806)
• Mary Robinson (1758-1800)

**Next Section: [Course Materials](http://digitaldefoe.org/?p=258)** . . .