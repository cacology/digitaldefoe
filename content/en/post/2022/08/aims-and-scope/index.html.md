---
layout: post
title: "Aims & Scope"
date: 2022-08-01
---

**Aims and Scope**

_Digital Defoe _is a mutually-anonymous peer-reviewed online journal with new volumes released annually. We welcome submissions exploring any area relating to Defoe and/or his contemporaries-broadly conceived. In addition to traditional scholarly papers, we welcome submissions of notes, pedagogical resources and reflections, as well as innovative multimedia projects relating to the long eighteenth century.

Our journal was founded with the following goals in mind:
-to navigate the potential and the limitations of digital culture for eighteenth-century studies
-to be honest and transparent about why we and our authors make particular communicational and technological choices when sharing our projects
-to mediate the conversation between our readers and the narratives they see unfold in each volume.

_Digital Defoe_ is indexed in the [Directory of Open Access Journals](https://doaj.org/) (DOAJ), [Google Scholar](https://scholar.google.com/schhp?hl=en), the [European Reference Index for the Humanities and Social Sciences](https://kanalregister.hkdir.no/publiseringskanaler/erihplus/periodical/info?id=504140) (ERIH Plus), and the [MLA International Bibliography](https://www.mla.org/Publications/MLA-International-Bibliography). We archive the web page and PDFs once a year in the [Internet Archive](https://scholar.archive.org/).

Content published on this website is under an Attribution-NonCommercial-NoDerivatives 4.0 International Creative Commons License ([CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)).

Authors retain copyright of their work without restriction. For more information, please visit our [Copyright page](https://digitaldefoe.org/copyright/).

All authors are permitted to deposit all versions of their work(s) in an institutional or subject repository.

Beginning with Volume 15, content published on this website will fall under an Attribution-NonCommercial-ShareAlike 4.0 International Creative Commons License ([CC BY-NC-SA 4.0](http://CC BY-NC-SA 4.0)).

**Corrections**

We seek to ensure that our published content remains as accurate and unchanged as possible, however, mistakes may happen. We follow the best practices outlined by [CELJ](http://www.celj.org/) and [COPE](https://publicationethics.org/).

Corrections to a published article will contain a post-publication notice within the downloadable PDF and HTML page. Readers will be fully informed of any corrections, expressions of concern, or retractions made from first date of publication. Correction notices will explain what was corrected and why.