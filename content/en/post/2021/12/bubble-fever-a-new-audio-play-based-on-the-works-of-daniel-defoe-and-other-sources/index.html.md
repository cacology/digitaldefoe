---
layout: post
title: "Bubble Fever: A New Audio Play, Based on the Works of Daniel Defoe and Other Sources"
date: 2021-12-19
tags: ["Defoe.in.Performance","Issue 13.1-Fall 2021","Issues"]
---

[![Photo of staged version of the play. One younger woman in a pink dress and an older man sit at a table strewn with papers and documents.](IMG_2873-300x225.jpeg)](http://digitaldefoe.org/wp-content/uploads/2021/12/IMG_2873.jpeg)

_Pictured: Laura Hayward (as Hannah Defoe) and Robert Lowe (as Daniel Defoe)._
_Photo Credit: David Fletcher_

[audio mp3="http://digitaldefoe.org/wp-content/uploads/2021/12/Bubble-Fever-Cast-Recording-mp3.mp3"][/audio]

Written and directed by David Fletcher

Performed by the Loft Theatre Company, in association with the History Department of the University of Warwick

Recording produced and music composed by [Jonathan Fletcher](http://jonathanfletcher.org/)