---
layout: post
title: "The Philosophy of Progress - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<span class="style1 style12">**Readings:   

          ➢	John Locke, _Essay Concerning Human Understanding_, selections  

          ➢	John Locke, _Two Treatises of Government_, selections**</span>

<span class="style1 style12">

  In his _Essay Concerning Human Understanding_, Locke contests traditional notions of knowledge; in the _Two Treatises_, he contests traditional notions of government. Our discussion of excerpts from these texts gives depth to the historical transformations introduced in the opening lecture.   

Our conversation about the epistemology touches on Locke's rejection of prior models of innate knowledge. We note his special use of such terms as sensation and reflection, and explore various images of human understanding at work turning experience into ideas, including that of the "closet wholly shut from light, with only some little opening left, to let in external visible resemblances, or ideas of things without." We then approach the political theory as a comparable rejection of top-down authority. Students become familiar with such key concepts as patriarchy/patriarchalism, the state of nature, property, social contract, civil society, and paternal power.   

Finally we find links between these two foundational texts of liberal democratic thought. I ask students to think with me about how the empirical mind is served by civil society and vice versa. We also discuss contradictions and gaps within and between Locke's epistemology and his political theory, particularly relating to the status of women.  On the one hand, Locke's (largely) universal models of learning and political engagement cut against traditional views of female cognitive and political inferiority. On the other hand, though Locke refutes the traditional equivalence of political and familial authority, he ultimately rationalizes male superiority within the family and more or less takes it as a given within the state. 
  </span>