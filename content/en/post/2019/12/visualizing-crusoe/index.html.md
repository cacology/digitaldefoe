---
layout: post
title: "Visualizing Crusoe"
date: 2019-12-04
tags: ["Issues","Issue 11.1 - Fall 2019","Features"]
---

"<span style="font-family: Adobe Caslon Pro, serif;">[Visualizing Crusoe](https://scalar.usc.edu/works/visualizing-crusoe-/index)" is a DH project that examines the centrality of visualization in Daniel Defoe's _Robinson Crusoe_. The project aims to capture two dual threads that emerge from the novel and an examination of Defoe's text in the digital age: the first, "visualizing Crusoe"-that is, using DH tools to visualize, model, and reveal aspects of the novel proper; and the second, "visualizing Crusoe"-a fitting description of a protagonist who makes meaning of his world by producing visual tools and artifacts, including lists, tables, journals, and tallies during his time spent upon the island. Three-hundred years ago-centuries before the digital humanities boom-Crusoe thinks visually. Visualizing Crusoe curates these examples of data visualization in the novel and also features new visualizations based on the text, including an interactive timeline of events in the novel and a map of Crusoe's travels, to reveal how many of our current methods of data visualization are indebted to older, traditional forms of data visualization and how our current digital moment can provide new insights into Defoe's canonical text.</span>