---
layout: post
title: "Closets Without Walls - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

[DOWNLOAD SPREADSHEET FOR COMPLETE INFORMATION](http://digitaldefoe.org/wp-content/files/teaching/bobker/closetswithoutwalls.htm)

## «[A](#A) - [B](#B) - [C](#C) - [D](#D) - [E](#E) - [F](#F) - [G](#G) - [H](#H) - [I](#I) - [J](#J) - [L](#L) - [M](#M) -[P](#P) - [R](#R) - [S](#S) - [T](#T) - [V](#V) - [W](#W)»

**A**<a id="A" name="A"></a>
A Book of Rarities: Or, Cabinet of Curiosities Unlock'd (1743)
A brief history of the Restauration (1729)
A cabinet of choice jewels (1701)
A cabinet of choice jewels (1762)
A Cabinet of Fancy (1799)
A Cabinet of Jewels opened to the Curious, by a key of Real Knowledge (1757)
A Cabinet of Miscellanies (1794)
A call to the unconverted (1746)
A catalogue of a pleasing assemblage of prints... (1792)
A catalogue of a well-chosen and select collection of Pictures (1791)
A Catalogue of that Superb and Well Known Cabinet of Drawings of John Barnard, Esq. (1787)
A catalogue of the cabinet of birds, and other curiosities (1769)
A catalogue of the collection of pictures, etc. (1758)
A catalogue of the elegant cabinet of natural and artificial rarities of the late ingenious Henry Baker, Esq. (1775)
A Catalogue of the genuine, curious, and valuable (1779)
A catalogue of the valuable museum (1794)
A closet for ladies and gentlewome (1608)
A Closet Piece: The Experimental Knowledge of the Ever-Blessed God (1721)
A collection of curious prints and drawing by the best masters in Europe (1718)
A Companion to Bullock's Museum (1799)
A coppy of verses writt in a Common Prayer Book (1710)
A general history of the proceedings and cruelties of the court of inquisition in Spain, Portugal (1731)
A key to natural history (1798)
A key to the cabinet of the Parliament, by their remembrancer (1648)
A key to the Kings cabinet (1645)
A key to the Six Per Cent Cabinet (1798)
A letter from the Man in the Moon to Mr. Anodyne Necklace (1725)
A manual history of Repentance and Impenitence (1724)
A rich cabinet of modern curiosities containing many natural and artificial conclusions... (1704)
A satyr, occasioned by the author's survey of a scandalous pamphlet intituled, the King's cabanet opened (1645)
A Thousand Notable Things on Various Subjects (1776)
A true narration of the surprizall of sundry cavaliers (1642)
A vindication of King Charles (1648)
An Account of a Useful Discovery (1756)
Apollo's Cabinet or the Muses Delight (1757)
Aristotle's Last Legacy (1711)
Art's Master-Piece (1768)

**B**<a id="B" name="B"></a>

Beautiful Cabinet Pictures (1798)

**C**<a id="C" name="C"></a>

Cabinet Litteraire (1796)
Cabinet of Curiosities, No. 1 (1795)
Catalogue of the geniune (1798)
Catalogue of the intire Cabinet of Capital Drawings, collected by the late Greffier Francois Fagel (1799)
Christ's famous titles (1728)
Coins and medals, in the cabinets of the Earl of Fife (1796)
Copys of several conferences and meetings (1790)
Cupid's Cabinet Open'd (1750)
Cupids cabinet unlock't (1641)
Curiosities: or, the cabinet of nature (1637)
Curtius's Grand Cabinet of Curiosities (1800)

**D<a id="D" name="D"></a>**

Delights for young Men and Maids (1725)
Duties of the Closet (1732)

**E<a id="E" name="E"></a>**

Elegant and Copious History of France. Number 1. (1791)
Elegant Drawing and Cabinet Pictures (1785)
England's choice cabinet of rarities; or The famous Mr. Wadham's last golden legacy (1700)
England's Mournful Monument (1714)
Every Lady her own Physician or the Closet Companion (1788)

**F<a id="F" name="F"></a>**

Flower-Garden Display'd (1732)
For the Inspection of the CuriousFor the inspection of the curious...a cabinet of royal figures (1785)
Fragment of the chronicles of Zimri the Refiner (1753)

**G<a id="G" name="G"></a>**

Gale's Cabinet of Knowledge (1796)
Gloria Britannorum or, The British Worthies (1733)

**H<a id="H" name="H"></a>**

History of Mother Bunch of the West (1797)
History of Mother Bunch of the West, Part the Second (1797)
Hocus Pocus (1715)

**I<a id="I" name="I"></a>**

Incomparable varieties (1740)
Instructions for a prince (1779)

**J<a id="J" name="J"></a>**

Jocabella, or a cabinet of conceit (1640)

**L<a id="L" name="L"></a>**

Ladies Cabinet broke open, Part 1 (1718)
Letters, poems, and tales (1718)
Lineal Arithmetic (1798)

**M<a id="M" name="M"></a>**

M----C L----N's cabinet broke open (1750)
Miss C--Y's cabinet of curiosities (1765)
Mist's Closet Broke Open (1728)
Monthly Beauties (1793)
Mother Bunch's Closet broke open...Part the Second (1800)
Mrs. Pilkington's Jests (1764)

**P<a id="P" name="P"></a>**

Particulars of and conditions of sale for a large and valuable estate called Goldings (1770)
Phylaxa Medinae. The cabinet of physick (1799)
Proposals for publishing by subscription from the curious and elaborate works of Thomas Simon (1753)
Psalmes of confession found in the cabinet of the most excellent King of Portinga (1596)

**R<a id="R" name="R"></a>**

Religion the most delightful employment (1739)
Ruperts sumpter, and private cabinet rifled (1644)

**S<a id="S" name="S"></a>**

Seven conferences held in the King of France's Cabinet of Paintings (1740)
Specimens of British Minerals selected from the Cabinet of Philip Rasleigh (1797)
Sunday Thoughts .4 (1781)

**T<a id="T" name="T"></a>**

Thane's second Catalogue (1773)
The accomplish'd Lady's Delight (1706)
The Believer's Golden Chain (1763)
The Book of Psalms Made Fit for the Closet with Collects and Prayers (1719)
The British Phoenix (1762)
The Cabinet (1797)
The Cabinet (1754)
The Cabinet of Beasts (1800)

The Cabinet of Genius (1787)
The Cabinet of Love (1792)
The Cabinet of Momus and Caledonian Humorist (1786)
The cabinet of the arts (1799)
The cabinet of True Attic Wit (1783)
The cabinet of wit (1797)
The Christian mans closet (1591)
The Christian's Closet-Piece: Being An Exhortation to all People To forsake their Sins, Which too much Reign in the present Age: As Pride, Envy, ... (1770)
The Christian's duty from the sacred scriptures (1730)
The Christian's New Year's Gift: containing a companion (1764)
The Christian's Plea for His God and Saviour Jesus Christ (1719)
The Christian's Preparation for the worthy receiving of the Holy Sacrament of the Lord's Supper (1772)
The chyrugians closet (1630)
The Closet Companion (1791)
The Closet of Counsells conteining the advice of divers philosophers (1569)
The Compleat English and French Vermin-Killer (1710)
The Copper Plate Magazine (1792)
The Country Physician (1703)
The Cyprian Cabinet (1783)
The Female Pilgrim1 (1762)
The French Momus (1718)
The General State of Education in the Universities (1759)
The Gentleman and Lady's Palladium (1752)
The Genuine Letters of Mary Queen of Scots to James Earl of Bothwell (1726)
The Golden Cabinet (1790)
The Golden Cabinet (1793)
The Golden Cabinet (1765)
The housekeeper's valuable present (1790)
The Irish Cabinet (1746)
The Irish cabinet: or His Majesties secret papers (1646)
The Key to the kings cabinet-counsell (1644)
The King of Scotlands negotiations at Rome (1650)
The Kings cabinet opened (1645)
The Ladies Cabinet (1743)
The ladies cabinet opened (1639)
The Lady's companion (1743)
The Laird of Cool's Ghost (1786)
The Last Night (1790)
The Lord George Digby's cabinet and Dr Goff's negotiations (1646)
The Lovers Cabinet (1755)
The Modern Family Physician (1783)
The Muses Cabinet (1771)
The Naturalist's Pocket 1 (1799)
The Naturalist's Pocket 2 (1799)
The Naturalist's Pocket 3 (1799)
The Naturalist's Pocket 4 (1799)
The Naturalist's Pocket 5 (1799)
The Naturalist's Pocket 6 (1799)
The New Week's Preparation for a Worthy Receiving of the Lords Supper (1770)
The Oxford Cabinet (1797)
The Parallel (1762)
The Parents Pious Gift (1704)
The Phenix Volume One (1707)
The Pleasing Instructor (1756)
The Poor Man's Help, and Young Man's Guide (1709)
The private tutor to the british youth (1764)
The rich cabinet furnished with varietie of excellent discriptions (1616)
The riches and extent of free grace displayed (1772)
The Royal Jester (1792)
The second part of Mother Bunch of the West (1750)
The Second Volume of the Phenix (1707)
The Spirit of Liberty (1770)
The state of France (1760)
The treasurie of commodious conceits (1573)
The world's doom: or the cabinet of fate unlocked. Vol. 2 (1795)
The world's doom: or the cabinet of fate unlocked. Vol.1 (1795)
To a vertuous and judicious lady who (for the exercise of her devotion) built a closet (1646)
To be seen in Curtius's Cabinet of Curiosities (1792)
Two spare keyes to the Jesuites cabinet (1632)

**V<a id="V" name="V"></a>**

Vox Populi (1774)

**W<a id="W" name="W"></a>**

Who Runs next (1715)
Wit's Cabinet (1715)