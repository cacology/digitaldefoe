---
layout: post
title: "Epistolary Spaces - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<span class="style1 style12">**Readings:   

          ➢	Eliza Haywood, _Love in Excess _  

➢	Samuel Richardson, _Pamela; or, Virtue Rewarded _**</span>

<span class="style1 style12">Closet discourses and practices provide concrete tools for exploring the rise of the novel in the final weeks of the semester. We read four influential and entertaining novels in chronological sequence. Many critics have argued that the modern novel shaped and reflected the growth of bourgeois domestic ideology in eighteenth-century England. Focusing on the novel's links to the secret history, our exploration emphasizes the gradual, uneven process of this development. Cynthia Wall has pointed out that most of the settings in eighteenth-century novels are only vaguely sketched if at all. Yet there is nevertheless a preponderance of closets and cabinets (and antechambers, keyholes, closed gardens, backdoors, backstairs, and underground passages) in them. Other clear, concrete marks of the influence of secret history on eighteenth-century novels include the elevated/public status of key characters, the elliptical rendering of certain names (such as Mr B---), and the centrality to their plots of private correspondence and sexual scandal. [Joseph Highmore's _Mr. B--- Finds Pamela Writing_](http://www.tate.org.uk/art/artworks/highmore-i-mr-b-finds-pamela-writing-n03573) encapsulates a number of these themes. We consider how novels finally challenge the secret history's traditional economy of value in which the importance of private affairs lies in the way they impinge upon or allegorize larger-national and/or political-concerns. In the eighteenth century, novelists were asking if and how the personal, the domestic, and ordinary people might be valued in and of themselves. McKeon's discussion of the secret history is very helpful here (469-505) in relation to his rereading of _Pamela_ (639-59): McKeon shows that it is the carefully crafted political aura in Richardson's novel that invests Mr B--- and Pamela's amatory entanglement with "socio-ethical weight" (642).   

                 Our discussions of _Love in Excess_ and _Pamela_ also look at how female privacy helped to lay the groundwork for the radical questioning of traditional gender roles and social hierarchies. Haywood uses the privileged, highly literate and reflexive solitude of her elite female characters to work out a new ideal of rational sexual agency for all women, dramatically revising the longstanding association of female virtue with chastity. In Richardson's novel, Pamela's surprising sophistication and self-awareness reflect her earlier dressing-room intimacy with her mistress, Lady B---, and the countless hours she later spends reading and writing letters in one closet or another: in other words, her exceptional access to privacy equips Pamela, morally and intellectually, to play the heroine. Ultimately, for both novelists, some substantial degree of female autonomy is the basic precondition of a good-that is, a companionate-marriage. Some students feel frustrated by the hypocrisies and contradictions in this formulation, which seems to assess female agency in terms of its advantages to men and heterosexuality. It can help to recall the older patriarchal values and practices-arranged marriages or marriages of alliance, for example-to which Haywood and Richardson were reacting.   

Our study of the novel as a modern genre in the making also focuses on key scenes of private reading of _Pamela_ and _Love in Excess_. Haywood is especially interested in how reading helps her curious but virtuous heroine, Melliora, to cultivate and ultimately to discipline her passion. In _Pamela_, Mr B--- learns to love Pamela respectfully only after reading all of her letters and coming to sympathize with her suffering. We discuss how these metatextual subplots model the virtual and internal experiences of intimacy that were increasingly understood to be characteristic of novels and at the core of their moral power.

**  

Suggested Presentation Topics:**   

Ros Ballaster on amatory fiction and the female reader   

Eighteenth-century reading practices   

Literacy in the eighteenth century   

_Desire and Domestic Fiction_   

The novel and masturbation</span>