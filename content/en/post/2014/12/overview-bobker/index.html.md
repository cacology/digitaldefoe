---
layout: post
title: "Overview - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td rowspan="2" valign="top">

<span class="style1 style12">The English closet took on many new shapes and functions as it proliferated in the long eighteenth century. It had origins in sixteenth-century palace apartments designed in _enfilade_: the lockable room at the end of a series of adjoining chambers had been crucial to the performance and consolidation of absolute power in the Tudor and early Stuart courts. In these secluded places, kings and queens could store valuables and special documents, read, write, or pray alone, and exchange confidences with their most trusted courtiers. Yet closets proved remarkably resilient over the next two centuries, even as power drifted away from the court. In the houses of people of quality and, increasingly, those of the middling sort, private rooms served as prayer closets, cabinets of curiosity, dressing rooms, libraries, art galleries, and impromptu bedrooms; and merging with the bath or privy, closets were transformed into bathing closets, closets of ease, outdoor privies known as earth closets and, eventually, water closets.</span>

While multiplying and morphing in material culture, these intimate spaces also made significant appearances in all kinds of writing. The closet was, for example, a metaphor for the space of the mind in empirical philosophy, a symbol of female vanity in satirical poetry, and a setting for introspection, sexual intrigue, and letter writing in fiction. Along with its close cousin, the cabinet, the closet also gave a name and an implicit structure to hundreds of miscellanies or anthologies in eighteenth-century England, from how-to books like _The Golden Cabinet of Useful Knowledge_ to recipe and remedy books like _The Queen-Like Closet_.</td>
<td valign="top"></td>
</tr>
<tr>
<td valign="top" width="370px">

[
](http://digitaldefoe.org/wp-content/files/teaching/bobker/1GreenCloset.png)[![Green Closet, Frogmore](1GreenClosetthumb.png)](http://digitaldefoe.org/wp-content/files/teaching/bobker/1GreenCloset.png)<span class="style1 style 12">Fig. 1. _Green Closet, Frogmore_ © British Library Board, 747.f.3, volume 2, plate opposite page 19. </span>

<div align="right"></div></td>
</tr>
</tbody>
</table>
<span class="style1 style12">_Eighteenth-Century Literature and the Culture of the Closet_ is a course that I developed to explore the functional, narrative, and symbolic roles closets played in eighteenth-century life and literature. Focusing on discourses and practices of the closet especially helps to illuminate the changing parameters of privacy in the period and the centrality of this category to concurrent developments in politics, religion, science, architecture, gender, and sexuality. First defined as a kind of withdrawal available only to the elite, privacy became in the eighteenth century a positive category of experience, as desirable as it was variable. The course takes a special interest in how privacy shapes and reflects literary styles and genres of the period, including the secret history, the prayer manual, the anthology, the country house poem, and the novel.</span>

&nbsp;

I have taught this semester-long course three times-once as a multilevel, interdisciplinary undergraduate seminar at Emory University and twice as a graduate English seminar at Concordia University in Montreal. I have also incorporated aspects of this course in introductory surveys of eighteenth-century literature. When I first designed it, my research agenda was at the forefront of my mind: the course was an opportunity for me to test, refine, and expand my ideas on the proliferation of closets in eighteenth-century architecture and writing, and to work on communicating them as clearly as possible. I have returned to the course and its themes again and again because they are clearly engaging for students as well. Advanced students enjoy the many open-ended explorations. At the same time, because the question of privacy was so central in eighteenth-century Britain, and a major preoccupation for canonical figures on the syllabus such as Locke, Pepys, Haywood, Pope, and Richardson, the course works well as a general introduction to the period.

There are intellectual challenges for everyone. Our objects of study are three moving targets: (1) the closet as a flexible architectural construct, (2) privacy's evolution in relation to other historical developments of the period (especially new practices and ideas of publicness), and (3) the reciprocal relationship between changing literary forms and writers' inventive use of closets as settings and symbols. Each of these themes invites a distinctive disciplinary orientation-those of material culture, social theory, and literary history respectively-while meta-thematic analysis of the processes of transformation-historicism-connects them all. Both depth and breadth of analysis are required, and maintaining the balance between them has been important to me each time I teach the course. On the one hand, there are a great many opportunities for creative and critical leaps. On the other hand, the specificity-the materiality-of our objects demands a special rigor and precision.

Below, I explain the key historical, cultural, and theoretical ideas I have emphasized during each of the course's eleven separate sections and I outline some of the most fruitful topics of conversation. I have found it useful initially to approach each theme on its own. After several weeks of overview (Sections 1 through 5), the course moves roughly chronologically through a range of interrelated texts (Sections 6 through 11). Early on we spend a good deal of time deciphering the closet's range of functions and uncovering our ideas about the meaning and value of private and public-detective work that is above all about careful close reading of primary texts. Later, we enter more abstract territory as we ask how various literary genres celebrate, reinforce, or challenge different kinds of private experience, not least those of readers. Near the end of the semester, many students have pieced together a basic narrative of privacy's emergence in and around literary form and will be ready to make their own intuitive leaps.
Teachers interested in the course as a whole will find it productive to peruse the sections in order. However, the lists of section headings and readings allow for more selective encounters.

Starting with Section 6, I have suggested presentation topics designed to familiarize the class with a range of complementary materials. Writing projects for the course have generally been a series of short response papers, in which students are asked to document their initial impressions of the readings, then a long final research paper, preceded by an annotated bibliography and prospectus, in which inquiries emerging during response papers and class discussions are extended. My students' final essays have covered such topics as Pepys at the coffee house, _Castle of Otranto_ as a secret history, the feminism of Swift's scatology, _Rape of the Lock_ as cabinet of curiosities, the feminization of privacy in _Pamela_ and _Memoirs of a Woman of Pleasure_, status implications of the word alone in the seventeenth century, among many others: the pleasure they have taken in defining and pursuing their projects for this course has in turn been one of the greatest pleasures of the course for me as well. Please use the seminar outlines below in your classroom however you wish. I welcome your questions and comments at [danielle.bobker@concordia.ca](mailto:danielle.bobker@concordia.ca)</td>
</tr>
</tbody>
</table>
&nbsp;