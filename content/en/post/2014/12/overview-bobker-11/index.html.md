---
layout: post
title: "Privy Pastoral - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<span class="style1 style12">**Readings:   

          ➢	Ben Jonson, "To Penshurst"   

➢	Jonathan Swift, "Panegyric on the Dean," "The Lady's Dressing Room,"
 "A Beautiful Young Nymph Going to Bed,"   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"Strephon and Chloe," and "Cassinus and Peter"   

➢	Mary Wortley Montagu, "Reasons that induced D--- S--- to Write a Poem Called 'The Lady's Dressing Room'"   

➢	Samuel Rolleston, _Philosophical Dialogue Concerning Decency_ 

**</span>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
              <td height="561" valign="top" width="27%" align="right">

<span class="style43 style44">&nbsp;![](18.jpg)  

              </span><span class="style1 style46">Fig. 18. A plaine pot of a privie in perfection.>   

    John Harington, _Metamorphosis of Ajax_, 196.</span>

&nbsp;
</td>
              <td valign="top" width="73%">

<span class="style1 style12">

                Is the desire for excretory privacy innate? Our discussion of some eighteenth-century responses to this question is informed by the material history of the water closet and the literary history of country-house poetry. A mechanized privy pot, capable of instantly flushing away waste, built into a room reserved exclusively for solitary excretion had been invented in the sixteenth century ([Figure 18](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/18.jpg)), but such a machine did not have wide appeal until the late eighteenth century. Before then, even among those who could have afforded to install special equipment, [simple chamber pots, which could be used anywhere and emptied by servants, were vastly more common](http://www.sciencemuseum.org.uk/broughttolife/objects/display.aspx?id=5185&keywords=chamber+pot). The fundamental value encapsulated by the water closet - the fantasy of perfect excretory autonomy - was, however, already in the air, and already subject to critique, in the first half of the eighteenth century.   

Pastoral, georgic, and country-house poetry focus on relationships among nature (including the body and its impulses), culture (including art, labour, and agriculture), and retreat. The primary texts in this section all draw on the interrelated forms of nature poetry to depict excretory privacy as a fraught gender issue. Though each juggles a unique set of presuppositions about the extent to which culture can or should compensate for apparently natural sexual differences, all toy with the common (and enduring) belief that women are particularly shamed by the exposure of primal bodily functions. Mary Wortley Montagu's retort to Swift's "Lady's Dressing Room" is an engaging way into these issues: Is there evidence in the poem that Montagu or any of her characters share Strephon's fear of Celia's shit? We then consider Rolleston's _Dialogue Concerning Decency_ as a countertext to Swift's longest, earliest, and most explicit scatological poem. "Panegyric on the Dean" commemorates the pair of his-and-hers outdoor privies Swift had just built on the country estate of his patroness, Lady Anne Acheson, and is written for her (and in her voice). As they explore the modern ideal of complete excretory autonomy, both texts ask not only (1) whether the ideal is aligned with or contrary to nature and (2) whether it is or should be equally shared by both sexes, but also (3) whether it reinforces social or selfish impulses.  These questions guide our conversation.   

**Suggested Presentation Topics:**  

The history of the water closet  

Swift's "excremental vision"   

Pollution issues: Mary Douglas's _Purity and Danger_  

Michael Edson, "'A Closet or a Secret Field': Horace, Protestant Devotion and British Retirement Poetry"

                </span><span class="style1 style12">  

                </span>

                </td>
            </tr>
          </tbody></table>
  