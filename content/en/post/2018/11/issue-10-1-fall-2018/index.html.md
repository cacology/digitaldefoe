---
layout: post
title: "Issue 10.1, Fall 2018"
date: 2018-11-01
---

<div class="issue_home_headers">Distinguished Scholar Invited Essay</div>
[catlist id=36 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Features</div>
[![FeaturedImage](http://digitaldefoe.org/wp-content/uploads/2018/11/1181px-1732_Herman_Moll_Map_of_the_West_Indies_and_Caribbean_-_Geographicus_-_WestIndies-moll-1732.jpg)](1181px-1732_Herman_Moll_Map_of_the_West_Indies_and_Caribbean_-_Geographicus_-_WestIndies-moll-1732.jpg)

[catlist id=37 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Reviews</div>
[catlist id=38 customfield_display="article_author" customfield_display_name=no]