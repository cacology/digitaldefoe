---
layout: post
title: "Daniel Defoe’s <em>Some Thoughts of an Honest Tory in the Country</em> (1716): A Critical Edition"
date: 2015-10-19
tags: ["Issues","Issue 7.1 - Fall 2015"]
---

# <span style="color: #386117;">[Introduction to the Edition](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  '  [Note on the Text](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition Copy")  '  [Front Matter](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy-2/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  '  [Maintext of _Some Thoughts_](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy-3/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  ' [Bibliography](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-2/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")</span>

# **
Bibliography**

Anon. _A Cry for Justice against all the Impeach'd and Attainted Rebels and Traitors, shewing, That Mercy to K. George's Enemies, is Cruelty to all true Friends of our King and Country, and that to save the one, will be the Destruction of the other_. London: S. Popping, 1716. Print.

Anon. _A New and General Biographical Dictionary ...Greatly Enlarged and Improved_. 12 vols. London: W. Strahan et al., 1784. Print.

Anon. _Observations upon the State of the Nation, in January 1712/3_. London: J. Morphew, 1713. Print.

Atterbury, Francis. _English Advice to the Freeholders of England_. London: n.p., 1714 [1715]. Print.

Backscheider, Paula R. _Daniel Defoe: His Life_. Baltimore: Johns Hopkins UP, 1989. Print.

Bennett, G. V. _The Tory Crisis in Church and State 1688-1730: The Career of Francis Atterbury, Bishop of Rochester_. Oxford: Clarendon Press, 1975. Print.

Burnet, Gilbert. _History of His Own Time_. 2 vols. London: Thomas Ward, 1724-34. Print.

Colley, Linda. _In Defiance of Oligarchy: The Tory Party, 1714-1760_. Cambridge: Cambridge UP, 1982. Print.

Defoe, Daniel. _An Argument Proving that the Design of Employing and Ennobling Foreigners, Is a Treasonable Conspiracy against the Constitution_. London: Printed for the booksellers of London and Westminster, 1717. Print.

- - -. _The Complete English Tradesman, in Familiar Letters_. London: C. Rivington, 1726 [1725]. Print.

- - -. _Conjugal Lewdness: or, Matrimonial Whoredom_. London: T. Warner, 1727. Print.

- - -. _A Continuation of Letters Written by a Turkish Spy at Paris_. London: W. Taylor, 1718. Print.

- - -. _An Enquiry into the Case of Mr. Asgill's General Translation_. London: J. Nutt, 1704. Print.

- - -. _An Essay on the History of Parties, and Persecution in Britain_. London: J. Baker, 1711. Print.

- - -. _An Essay on the South-Sea Trade_. London: J. Baker, 1711. Print.

- - -. _Jure Divino_. London: n.p., 1706. Print.

- - -. _A New Family Instructor_. London: C. Rivington and T. Warner, 1727. Print.

- - -. _Reasons against Fighting_. London: n.p., 1712. Print.

- - -. _Review_ (1704-13). 9 vols. Ed. John McVeagh. London: Pickering and Chatto, 2004-11. Print.

- - -. _Secret Memoirs of a Treasonable Conference at S. . . . . House, For Deposing the Present Ministry_. 2nd ed. London: J. More, 1717. Print.

- - -. _The Works of Daniel Defoe_. 44 vols. Ed. P. N. Furbank and W. R. Owens. London: Pickering and Chatto, 2000-8. Print.

Fielding, Henry. _The True Patriot and Related Writings_. Ed. W. B. Coley. The Wesleyan Edition of the Works of Henry Fielding. Middletown: Wesleyan UP, 1987. Print.

Furbank, P. N., and W. R. Owens. _The Canonisation of Daniel Defoe_. New Haven: Yale UP, 1988. Print.

- - -. _A Critical Bibliography of Daniel Defoe_. London: Pickering and Chatto, 1998. Print.

- - -. _Defoe De-attributions: A Critique of J. R. Moore's "Checklist."_ London: Hambledon, 1994. Print.

- - -. _A Political Biography of Daniel Defoe_. London: Pickering and Chatto, 2006. Print.

Gibson, William. _Enlightenment Prelate: Benjamin Hoadly, 1676-1761_. Cambridge: James Clarke, 2004. Print.

Gregg, Edward. _Queen Anne_. New Haven: Yale UP, 1980. Print.

Hatton, Ragnhild. _George I: Elector and King_. London: Thames and Hudson, 1978. Print.

Hill, Brian W. _Robert Harley: Speaker, Secretary of State and Premier Minister_. New Haven and London: Yale UP, 1988. Print.

Hoadly, Benjamin. _The Thoughts of an Honest Tory, upon the Present Proceedings of that Party_. London: A. Baldwin, 1710. Print.

Holmes, Geoffrey. _British Politics in the Age of Anne_. 2nd ed. London: Hambledon, 1987. Print.

Horwitz, Henry. _Revolution Politics: The Career of Daniel Finch Second Earl of Nottingham, 1647-1730_. Cambridge: Cambridge UP, 1968. Print.

Jones, Clyve. "Debates in the House of Lords on 'The Church in Danger', 1705, and on Dr. Sacheverell's Impeachment, 1710." _The Historical Journal_ 19.3 (1976): 759-71. Print.

Macaree, David. "Daniel Defoe and the '15." _International Review of Scottish Studies_ 14 (1987): 11-18. Print.

Monod, Paul. _Jacobitism and the English People, 1688-1788_. Cambridge: Cambridge UP, 1993. Print.

Moore, John Robert. _A Checklist of the Writings of Daniel Defoe_. 2nd ed. Hamden: Archon, 1971. Print.

- - -. "Defoe Acquisitions at the Huntington Library." _Huntington Library Quarterly_ 28.1 (1964): 45-57. Print.

Novak, Maximillian E. "Defoe." _The New Cambridge Bibliography of English Literature_. Ed. George Watson. Cambridge: Cambridge UP, 1971. Print.

Rogers, Nicholas. "Popular Protest in Early Hanoverian London." _Past and Present_ 79.1 (1978): 70-100. Print.

Schonhorn, Manuel. "Defoe and the Limits of Jacobite Rhetoric." _English Literary History_ 64.4 (1997): 871-86. Print.

Spaeve, Donald A. _The Church in an Age of Danger: Parsons and Parishioners, 1660-1740_. Cambridge: Cambridge UP, 2000. Print.

Treadwell, Michael. "London Trade Publishers 1675-1750." _The Library_, 6th series, 4.2 (1982): 99-134. Print.

Trent, William Peterfield. "Bibliographical Notes on Defoe - III." _The Nation_ 29 Aug. 1907: 180-83. Print.

- - -. "Bibliography of Daniel Defoe." Beinecke Rare Book and Manuscripts Library, Yale University, GEN MSS 859.

Wotton, William. _A Vindication of the Earl of Nottingham from the Vile Imputations, and Malicious Slanders, which have been cast upon HIM in some late PAMPHLETS_. London: J. Roberts, 1714. Print.

# <span style="color: #386117;">[Introduction to the Edition](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  '  [Note on the Text](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition Copy")  '  [Front Matter](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy-2/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  '  [Maintext of _Some Thoughts_](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy-3/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  ' [Bibliography](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-2/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")</span>