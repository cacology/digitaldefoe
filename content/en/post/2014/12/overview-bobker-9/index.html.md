---
layout: post
title: "Closets Without Walls - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<a name="spiritual" id="spiritual"></a>[DOWNLOAD  SPREADSHEET FOR COMPLETE INFORMATION](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/closetswithoutwalls.htm)

## [Spirtual Advice](#spiritual) ' [Educational Miscellany](#educational) ' [Religious Miscellany](#religious) ' [Miscellany of Art & Music  ](#art)'  [Miscellany](#miscellany)  

            [Catalogue](#catalogue)  '  [Fiction](#fiction)  '  [Recipe Book](#recipe)  ' [Miscellany of Biographical Material](#bio)  '  [Literary Miscellany](#literary)  '  [Historical Miscellany](#historical)

<p>

**  

Spiritual Advice**  

A cabinet of choice jewels (1762)   

A Closet Piece: The Experimental Knowledge of the Ever-Blessed God (1721)<a name="educational" id="educational"></a>           

The Christian's Plea for His God and Saviour Jesus Christ (1719)                                          

**Educational Miscellany**  

A Book of Rarities: Or, Cabinet of Curiosities (1743)   

A brief history of the Restauration (1729)   

A Cabinet of Jewels opened to the Curious, by a key of Real Knowledge (1757)   

A Cabinet of Miscellanies (1794)   

A catalogue of the cabinet of birds, and other curiosities (1769)   

A closet for ladies and gentlewomen (1608)   

A general history of the proceedings and cruelties of the court of inquisition in Spain, Portugal (1731)   

A key to natural history (1798)   

A key to the cabinet of the Parliament, by their remembrance (1648)   

A key to the Six Per Cent Cabinet (1798)   

A rich cabinet of modern curiosities containing many natural and artificial conclusions... (1704)   

A satyr, occasioned by the author's survey of a scandalous pamphlet intituled, the King's cabanet opened (1645)   

A Thousand Notable Things on Various Subjects (1776)   

A true narration of the surprizall of sundry cavaliers (1642)   

An Account of a Useful Discovery (1756)   

Aristotle's Last Legacy (1711)   

Art's Master-Piece (1768)   

Cabinet of Curiosities, No. 1 (1795)   

Cupids cabinet unlock't (1641)   

Curiosities: or, the cabinet of nature (1637)   

Delights for young Men and Maids (1725)   

Elegant and Copious History of France. Number 1. (1791)   

Every Lady her own Physician or the Closet Companion (1788)   

Gale's Cabinet of Knowledge (1796)   

Gloria Britannorum or, The British Worthies (1733)   

History of Mother Bunch of the West (1797)   

History of Mother Bunch of the West, Part the Second (1797)   

Hocus Pocus (1715)   

Instructions for a prince (1779)   

Jocabella, or a cabinet of conceit (1640)   

Ladies Cabinet broke open, Part 1 (1718)   

Lineal Arithmetic (1798)   

Monthly Beauties (1793)   

Mother Bunch's Closet broke open...Part the Second (1800)   

Mrs. Pilkington's Jests (1764)   

Phylaxa Medinae. The cabinet of physic (1799)   

Ruperts sumpter, and private cabinet rifled (1644)   

Seven conferences held in the King of France's Cabinet of Paintings (1740)   

Specimens of British Minerals selected from the Cabinet of Philip Rasleigh (1797)   

The accomplish'd Lady's Delight (1706)   

The British Phoenix (1762)   

The Cabinet (1754)   

The Cabinet (1797)              

The Cabinet of Genius (1787)   

The Cabinet of Momus and Caledonian Humorist (1786)   

The cabinet of True Attic Wit (1783)   

The chyrugians closet (1630)   

The Closet of Counsells conteining the advice of divers philosophers (1569)   

The Complete English and French Vermin-Killer (1710)   

The Country Physician (1703)   

The Country-Man's Vade-Mecum (1709)   

The Female Pilgrim (1762)   

The General State of Education in the Universities (1759)   

The Gentleman and Lady's Palladium (1752)   

The Golden Cabinet (1790)   

The housekeeper's valuable present (1790)   

The Irish Cabinet (1746)   

The Ladies Cabinet (1743)   

The ladies cabinet opened (1639)   

The Lady's companion (1743)   

The Lovers Cabinet (1755)   

The Modern Family Physician (1783)   

The Muses Cabinet (1771)   

The Naturalist's Pocket 1 (1799)   

The Naturalist's Pocket 2 (1799)   

The Naturalist's Pocket 3 (1799)   

The Naturalist's Pocket 4 (1799)   

The Naturalist's Pocket 5 (1799)   

The Naturalist's Pocket 6 (1799)   

The Parallel (1762)   

The Phenix Volume One (1707)   

The Pleasing Instructor (1756)   

The Poor Man's Help, and Young Man's Guide (1709)   

The private tutor to the british youth (1763)   

The rich cabinet furnished with varietie of excellent discriptions (1616)   

The Royal Jester (1792)   

The second part of Mother Bunch of the West (1750)   

The Second Volume of the Phenix (1707)   

The Spirit of Liberty (1770)   

The state of France (1760)   

The treasurie of commodious conceits (1573)   

To be seen in Curtius's Cabinet of Curiosities (1792)   

Two spare keyes to the Jesuites cabinet (1632) <a name="religious" id="religious"></a>  

Vox Populi (1774)   

Wit's Cabinet (1715)   

**Religious Miscellany**  

A call to the unconverted (1746)   

A copy of verses writt in a Common Prayer Book (1710)   

A manual history of Repentance and Impenitence (1724)   

Christ's famous titles (1728)   

Duties of the Closet (1732)   

Religion the most delightful employment (1739)   

Sunday Thoughts (1781)   

The Book of Psalms Made Fit for the Closet with Collects and Prayers (1719)   

The Christian mans closet (1591)   

The Christian's Closet-Piece: Being An Exhortation to all People To forsake their Sins, Which too much Reign in the present Age: As Pride, Envy, ...  (1770)   

The Christian's duty from the sacred scriptures (1730)   

The Christian's New Year's Gift: containing a companion (1764)   

The Christian's Preparation for the worthy receiving of the Holy Sacrament of the Lord's Supper (1772)   

The Closet Companion (1791)   

The New Week's Preparation for a Worthy Receiving of the Lords Supper (1770)   

The Parents Pious Gift (1704)   

The world's doom: or the cabinet of fate unlocked. Vol.1 (1795)   

The world's doom: or the cabinet of fate unlocked. Vol.2 (1795) <a name="art" id="art"></a>  

To a vertuous and judicious lady who (for the exercise of her devotion) built a closet (1646)   

**Miscellany of Art & Music**  

A cabinet of choice jewels (1701)   

A catalogue of a well-chosen and select collection of Pictures (1791)   

Apollo's Cabinet or the Muses Delight (1757)   

Cupid's Cabinet Open'd (1750)   

Flower-Garden Display'd (1732)   

For the Inspection of the Curious (1785)   

Proposals for publishing by subscription from the curious and elaborate works of Thomas Simon (1753)   

The Cabinet of Beasts (1800)   

The cabinet of the arts (1799) <a name="miscellany" id="miscellany"></a>  

The Copper Plate Magazine (1792)   

The Oxford Cabinet (1797)   

**Miscellany**  

England's choice cabinet of rarities; or The famous Mr. Wadham's last golden legacy (1700) <a name="catalogue" id="catalogue"></a>  

**Catalogue**  

A catalogue of pleasing assemblage of prints (1792)                                             

A Catalogue of that Superb and Well Known Cabinet of Drawings of John Barnard, Esq.  (1787)   

A catalogue of the collection of pictures, etc. (1758)   

A catalogue of the elegant cabinet of natural and artificial rarities of the late ingenious Henry Baker, Esq. (1775)     

A Catalogue of the genuine, curious, and valuable (1779)   

A catalogue of the valuable museum (1794)   

A collection of curious prints and drawing by the best masters in Europe (1718)   

A Companion to Bullock's Museum (1799)   

Beautiful Cabinet Pictures (1798)   

Cabinet Litteraire (1796)   

Catalogue of the genuine (1798)   

Catalogue of the intire Cabinet of Capital Drawings, collected by the late Greffier Francois Fagel (1799)   

Coins and medals, in the cabinets of the Earl of Fife (1796)   

Curtius's Grand Cabinet of Curiosities (1800)   

Elegant Drawing and Cabinet Pictures (1785)   

For the inspection of the curious (1785)   

M----C L----N's cabinet broke open (1750)   

Particulars of and conditions of sale for a large and valuable estate called Goldings (1770) <a name="fiction" id="fiction"></a>  

Thane's second Catalogue (1773)   

The Last Night (1790)   

**Fiction**  

A Cabinet of Fancy (1799)   

A letter from the Man in the Moon to Mr. Anodyne Necklace (1725)   

Copys of several conferences and meetings (1790)   

Fragment of the chronicles of Zimri the Refiner (1753)   

Miss C--Y's cabinet of curiosities (1765)   

The Cabinet of Love (1792)   

The cabinet of wit (1797)   

The Cyprian Cabinet (1783) <a name="recipe" id="recipe"></a>  

The Female Pilgrim (1762)   

The Laird of Cool's Ghost (1786)   

**Recipe Book**  

Incomparable varieties (1740) <a name="bio" id="bio"></a>  

**Miscellany of Biographical Materials**  

A key to the Kings cabinet (1645)   

A vindication of King Charles (1648)   

England's Mournful Monument (1714)   

Letters, poems, and tales (1718)   

Mist's Closet Broke Open (1728)   

Psalmes of confession found in the cabinet of the most excellent King of Portinga (1596)   

The French Momus (1718)   

The Genuine Letters of Mary Queen of Scots to James Earl of Bothwell (1728)   

The Irish cabinet: or His Majesties secret papers (1646)   

The King of Scotlands negotiations at Rome (1650)   

The Kings cabinet opened (1645)   

The Lord George Digby's cabinet and Dr Goff's negotiations (1646)   

The Queen's Closet Opened (undated) <a name="literary" id="literary"></a>  

The riches and extent of free grace displayed (1772)   

Who Runs next (1715)   

**Literary Miscellany**  

The Golden Cabinet (1765) <a name="historical" id="historical"></a>  

**Historical Miscellany**  

The Key to the kings cabinet-counsell (1765)   