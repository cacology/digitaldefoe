---
layout: post
title: "Feminist Recovery Practices & Digital Pedagogies: Teaching 18th-Century Women Poets"
date: 2014-10-29
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

IN THE SUMMER of 2012, I was given the opportunity to teach a summer poetry course, and I chose to teach a course on female poets of the eighteenth century. My [rationale](http://digitaldefoe.org/?p=252) for the course was that it would allow students to come into contact with poetry that, even as English majors, they would not likely read elsewhere. As I began planning the course, it became clear to me that given certain limitations on our time and [prior student knowledge](http://digitaldefoe.org/?p=254), it would neither be possible nor useful to conduct the course in the manner of a "typical" upper-level literature course during a regular semester. Instead, after some consideration, I decided that the course would combine my desire to trouble the "typical" eighteenth-century canon, as it is conceived in survey courses, with my growing interest in digital humanities and multimodal composition.

As the title of this website indicates, the course was devoted to exploring how to use feminist recovery practices and digital pedagogies in the classroom. The course objectives ([below](#courseobjectives)) also included more "traditional" objectives of a literature or poetry course. The objectives for this web presentation (also [below](#webobjectives)) are also multifold. First, I present this information in order to suggest ways in which to create literature courses that are more interactive and digitally-oriented as well as more attuned to feminist recovery practices. Second, I hope that this presentation will make clear the possible "payoffs" for including lesser-taught texts in the classroom.

**Course Objectives**<a id="courseobjectives" name="courseobjectives"></a>

The main goals of the course were to:
» introduce students to eighteenth-century British culture and eighteenth-century British women's poetry;
» explore the interactions between the poetry of women and men in eighteenth-century Britain;
» understand the position and oppression of women in eighteenth-century Britain;
» gain an appreciation of eighteenth-century poetic forms and styles; and
» contribute to the popularization of understudied women's literature.

This website explores the various pedagogical and scholarly tools that went into designing the course, sets out the operation of the course, and describes the final projects prepared by the students. In addition to sections on [students' previous knowledge](http://digitaldefoe.org/?p=254), my [expectations for the course](http://digitaldefoe.org/?p=252), the [course framework](http://digitaldefoe.org/?p=256), and [information on our materials](http://digitaldefoe.org/?p=258), there is also a section on the [final assignment](http://digitaldefoe.org/?p=260) and an example of one of the [student projects](http://digitaldefoe.org/?p=262) submitted for this assignment.

**
Web Presentation Objectives**<a id="webobjectives" name="webobjectives"></a>

The main goals of this website are to:
» introduce scholars of the eighteenth century to digital projects for the literature classroom;
» demonstrate that digital projects actively generate student interest in research and broaden their abilities to
write in a digital medium;

» examine the limitations of students and literature courses at the university undergraduate level, especially with
regard to eighteenth-century literature broadly and women's literature specifically;
» suggest methods for making students more aware of canon-formation and feminist practices; and
» open up a discussion among scholars on the relation of digital pedagogy and feminist recovery practices in the classroom.

I explain the rationale for using digital pedagogies and feminist recovery practices and their interconnectedness in the sections on [rationale](http://digitaldefoe.org/?p=252), [prior student knowledge](http://digitaldefoe.org/?p=254), and the [final student wiki assignment](http://digitaldefoe.org/?p=260), while the section on the [course materials](http://digitaldefoe.org/?p=258) suggests how to make a course of this nature inclusive and diverse.

**Ula Klein**
Texas A&M International University

**Next Section: [Rationale](http://digitaldefoe.org/?p=252)** . . .