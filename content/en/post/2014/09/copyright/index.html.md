---
layout: post
title: "Copyright Information"
date: 2014-09-25
---

Authors featured in _Digital Defoe_: _Studies in Defoe & His Contemporaries_ retain copyright of their work without restrictions and may republish their text in electronic or print form if they acknowledge _Digital Defoe_: _Studies in Defoe & His Contemporaries_ as the site of original publication. Authors of accepted text or multimedia submissions agree to allow _Digital Defoe_: _Studies in Defoe & His Contemporaries_ the right to publish and distribute their work electronically on the journal site, in the online archives, and on CD-ROM. Authors also grant _Digital Defoe_: _Studies in Defoe & His Contemporaries_ the right to reference and feature their work in promotional materials. _Digital Defoe_: _Studies in Defoe & His Contemporaries_, including its design, interface, content, and features, is copyrighted and not under the ownership or restrictions of any individual or institution. _Digital Defoe_: _Studies in Defoe & His Contemporaries_ is the collective vision of the current editorial staff, members of _The Defoe Society_, and the international community of Defoe and interested scholars. _Digital Defoe_: _Studies in Defoe & His Contemporaries_ is an open access journal which means that all content is freely available without charge to the user or his/her institution. Users are allowed to read, download, copy, distribute, print, search, or link to the full texts of the articles, or use them for any other lawful purpose, without asking prior permission from the publisher or the author. This is in accordance with the BOAI definition of open access. _Digital Defoe_: _Studies in Defoe & His Contemporaries_ holds a CC BY-NC-ND license for its content, which states that users can use and reuse the material published in the journal but only for non-commercial purposes. Derivatives of the journal content are not allowed.

***

All authors are permitted to deposit all versions of their work(s) in an institutional or subject repository.

***

Beginning with Volume 15, content published via _Digital Defoe_ falls under a[ CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.

This means you are free to:

**Share** - copy and redistribute the material in any medium or format

**Adapt** - remix, transform, and build upon the material

The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

**Attribution** - You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

**NonCommercial** - You may not use the material for commercial purposes.

**ShareAlike** - If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

**No additional restrictions** - You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

Notices:

You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.

No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.