---
layout: post
title: "Reflections on the Course - Klein"
date: 2014-12-02
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

**The goals of the course (revisited) were to:**
• introduce students to eighteenth-century British culture and eighteenth-century British women's poetry;
• explore the interaction between the poetry of women and men in eighteenth-century Britain;
• understand the position and oppression of women in eighteenth-century Britain;
• gain an appreciation of eighteenth-century poetic forms and styles; and
• contribute to the popularization of understudied women's literature

At the end of the course, students had acquired an appreciation of poetic forms (like sonnets, odes, and heroic couplets), read a variety of poems from the eighteenth century by English, Scottish, and American women, and had first-hand experience with literary research using both primary and secondary sources. Through in-class presentations and supplementary readings, students were also introduced to eighteenth-century culture and life in Britain and America, with particular attention paid to the position of women at the time. The course included poetry by women, both rural and London-based, well-known in their own time and obscure, rich and poor, black and white. The course focused on issues of inclusivity, diversity, feminist recovery, canonicity, and community. Students demonstrated their mastery of literary terms and analysis through their final project, and, through the wikis and class discussions, they also showed their new-found interest in the female authors we studied.

Where I feel the course could be improved was with regard to the formal elements of poetry, such as the uses of meter, rhyme, line breaks, etc. While some of these elements were covered in the course, there was not enough time to explore them in-depth. Similarly, in a full-length, semester-long course, there might have been time for students to give a second oral presentation on an element of eighteenth-century life, especially pertaining to women. Instead, the burden of introducing students to the historical period fell to me, the instructor, and was limited by time.

Additionally, it bears mentioning that although my initial hope was that the students would have time to revise the wiki entries and then use them to edit the existing author pages on Wikipedia, the students did not seem overly eager to share their work publicly. Some of them actively expressed their fear of publishing in a public forum. Pooneh Lari notes that this is a fairly common fear of students: "another concern noted about the use of wikis is the idea of 'hidden audience'....Wheeler and Wheeler (2009)...noted that students were aware of a hidden audience of visitors that would visit the wiki that could be tracked by the hit counter....A simple solution to this problem is to create a password for accessing the wiki, thereby excluding outside visitors" (123).

Another solution to this issue might have been to include time for revision, further class/instructor collaboration, and then edit the Wikipedia pages together, collaboratively. Lari notes, however, that "a community of practice provides an environment for social interaction between learners in which they can have a dialogue and discuss their learning and perspectives" (124). Even though the students ultimately did not publish their wiki entries, they still gained important experiences through the course discussions and dialogues that grew out of the wiki project.

**The main goals of this website (revisited) are to: **

• introduce scholars of the eighteenth century to digital projects for the literature classroom;
• demonstrate that digital projects actively generate student interest in research and broaden their abilities to write in a digital medium;
• examine the limitations of students and literature courses at the university undergraduate level, especially with regard to eighteenth-century
literature broadly and women's literature specifically;
• suggest methods for making students more aware of canon-formation and feminist practices; and
• open up a discussion among scholars on the relation of digital pedagogy and feminist recovery practices in the classroom.

While it may not be practical or possible to run such specific poetry courses at other universities, it may be possible to adapt these ideas and materials to a single unit of a course on poetry, eighteenth-century literature, or even a British survey course. One of the major ideas that guided my choices during this course and the ensuing online project about it was that even the most general courses can accommodate lesser-known writers and works, works by women, lower-class writers and writers of color, as well as digital, multimodal projects and writing.

I hope that this project will inspire other instructors to incorporate course wikis and other interactive, multimodal projects into their classes. Additionally, it is my hope that this web project will also spark further conversation about how such projects can enhance literary studies while working in tandem with projects that focus on issues of equality. Research projects that engage actively in understanding, sifting through, altering and analyzing public knowledge, especially those that focus on issues relating to sex, race, class, sexuality, nationality and empire, can, in a digital forum, leave the classroom and contribute to changing public paradigms of thought on these topics. Digital projects have the power to engage students in textual studies while also helping them become twenty-first-century thinkers and digital writers.

**Final Questions for Discussion**

What is the future of the "traditional" literary research paper in the undergraduate classroom?

How might instructors make use of digital forms of writing to make literature courses, from the introductory course to the survey to the upper-level seminar, more effective and student-centric?

What kinds of digital projects are best suited to the literature classroom?

How can digital technologies change/augment classroom syllabuses, "traditional" anthologies, and classroom instruction?

What kinds of projects help students gain first-hand understanding of literary theories and practices, including, but not limited to feminism, queer theory, critical race theory, postcolonialism and transnational conceptions of literature and literary canons?
**Next Section: [Works Cited](http://digitaldefoe.org/?p=266)** . . .