---
layout: post
title: "Announcements"
date: 2014-09-25
---

We hope to see you in the fall of 2021 in San Juan, Puerto Rico, the homeland of the Taino people, for the [Seventh Biennial Meeting of the Defoe Society](https://www.defoesociety.org/conference/)!

<!-- wp:paragraph -->

<!-- /wp:paragraph -->