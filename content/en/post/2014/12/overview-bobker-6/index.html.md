---
layout: post
title: "Privacy and Modernity II: The Public Sphere - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<span class="style1 style12">**Readings:
➢ Jürgen Habermas, _The Structural Transformation of the Public Sphere_, selections
➢ Michael Warner, "Public and Private" in _Public and Counterpublics_
➢ Joseph Addison and Richard Steele, _The Spectator_, Numbers 1, 10, 217
**</span>

<span class="style1 style12">An introduction to public sphere theory extends students' understanding of changing ideas and practices of privacy as corollaries or complements (and not necessarily in opposition) to changing ideas and practices of publicness. This section turns on Jürgen Habermas's influential account of how new modes of political action and interpersonal connection, independent of the state, were made possible by the growth of capitalism, personal wealth, and print culture in eighteenth-century England. We note that here, not only is the family the major site in the development of privacy "in the modern sense of a saturated and free interiority" (28), but it is also the subjective condition of possibility of the modern public sphere (43). </span>

&nbsp;
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="right" valign="top" width="27%">

[![Warner Public and Private](9warner.png)](http://digitaldefoe.org/wp-content/files/teaching/bobker/9MichaelWarnercopy.pdf)<span class="style43 style44"> </span>

<span class="style1 style46">Fig. 9. Public and private
From Michael Warner, _Publics and Counterpublics_, 29-30</span>

[![](http://digitaldefoe.org/wp-content/files/teaching/bobker/10.jpg)](10.jpg)<span class="style1 style46">

Fig. 10. Private and public. Jürgen Habermas, _The Structural Transformation of the Public Sphere_, 30.</span>

&nbsp;

</td>
<td valign="top" width="73%">

<span class="style1 style12">With reference to three essays from Joseph Addison and Richard Steele's highly successful, daily London periodical, _The Spectator_ (one of Habermas's exemplary texts), we observe how print's quick turnaround and low costs facilitated a more reciprocal relationship between authors and readers. This is most obviously manifested in the many letters from readers that Mr Spectator solicits, publishes, and engages with in print. In Number 10, when Mr Spectator declares, "I shall be ambitious to have it said of me, that I have brought Philosophy out of Closets...," he makes the private room symbolize the antiquated, impenetrable form of intellectual authority that he explicitly rejects in favor of a more interactive mode of engagement. (As we will see in Section 7, in the eighteenth century, the closet or cabinet "opened" in fact became a very common figure for the unprecedented accessibility of commercial print.) The issue of women's access to the public sphere is especially charged in the _Spectator_. Mr Spectator represents female readers as important beneficiaries of the daily guidance provided by his publication because they are naturally susceptible to frivolity and other passionate excesses, but he also seems eager to discipline female embodiment and women's collective agency beyond the home. In Number 217, for example, Mr Spectator responds with bemused reproach to "Kitty Termagant"'s description of a "Club of She-Romps," a wild all-female midnight gathering.

Convinced by Habermas's narrative in outline, Michael Warner emphasizes the democratic potential of modern media publics while criticizing the ways their putative universality in fact privileges heterosexual white men. Warner especially champions the idea and manifestations of counterpublics, that is virtual collectives in which the embodied conditions of gender and sexuality are not denied and repressed as in conventional publics but rather treated as "the occasion for forming publics, elaborating common worlds, making the transposition from shame to honor, from hiddenness to the exchange of viewpoints with generalized others" (61). For instance, Warner finds in the "Club of She-Romps" in _Spectator_ Number 217 a striking illustration of an early counterpublic. This part of Warner's argument causes some debate among students, some of whom are skeptical that this obviously satirical essay can be read so much against the grain. Warner's discussion of a famous anecdote about Diogenes masturbating in the marketplace succinctly illustrates "the visceral force behind the moral ideas of private and public" (21). Another very helpful point of reference is his comprehensive chart of definitions ([Figure 9](http://digitaldefoe.org/wp-content/files/teaching/bobker/9MichaelWarnercopy.pdf)), which elaborates the wide range of meanings of private and public, some but not all of which are opposing. We use it to review Habermas's specific uses of the terms private and public ([Figure 10](http://digitaldefoe.org/wp-content/files/teaching/bobker/10.jpg)) (which may seem contradictory but in fact are not) and we return to this chart often throughout the semester to make sense of our own and other current investments in these categories.

</span></td>
</tr>
</tbody>
</table>
  