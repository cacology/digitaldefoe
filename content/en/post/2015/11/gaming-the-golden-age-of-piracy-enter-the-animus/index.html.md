---
layout: post
title: "Gaming the Golden Age of Piracy   |   Enter the Animus"
date: 2015-11-04
tags: ["Issues","Features","Issue 7.1 - Fall 2015"]
---

**Enter the Animus**

The _Assassin's Creed_ series makes the movement between worlds an integral part of its game mechanics. In none of the games do players play the past directly. Instead, each installment operates as a frame narrative in which players enter history through the Animus - a virtual reality machine that allows users to relive and record past lives through the genetically encoded memories of those that lived them. An overarching story situates each adventure within a timeline extending from the near future to 75010 BC, when Adam and Eve rebelled against an older but far more advanced "First Civilization" that originally engineered human beings on Earth as a workforce. Humanity triumphed in the war that followed, but two groups emerged from victory with different visions of the future: the Assassin Order, which champions free will and individuality, and the Order of the Knights Templar, whose members seek to "perfect" civilization by bringing it under their control. As the games reveal, their conflict has been the unseen driving force of human history for more than two thousand years. The Animus remains at its center, for it is the key to recovering the lost First Civilization artifacts needed by the Templars to impose their New World Order.

Beneath the byzantine storyline lies a simple but fantastic conceit: the near-lossless remediation of historical experience. To achieve the complete set of goals for a given memory sequence is to have attained what the Animus and game call "full synchronization" - the reliving of events exactly as those events were lived in the times of the playable avatars. Violating the rules of the game - whether by directing those avatars into areas designated out-of-bounds, failing to achieve specific mission goals, or in any way causing them to die before their time - results in what the series refers to as "desynchronization." The Animus treats such violations as the player having strayed too far from the avatar's "actual" experiences; in essence, the historical simulation crashes and the player must restart the memory.

Such crashes, as well as all the information and interfacing options normally provided to enhance the experience of play (including maps, health meters, inventories, tallies of points or money earned) seamlessly integrate game design and mechanics into the fictional framework of the Animus technology. At the same time, though, they quietly call attention to a gap between the promise and the possibility of an absolutely authentic historical experience. The times and places to which the Animus gives access, no matter how richly detailed or historically accurate they appear, are not actually the past; they are marked by the fact of the machine as simulations of the past within simulations of a present accessed via yet another machine - the player's actual game console. The Animus thus serves as a reminder of an ineradicable distance and difference between past and present even as it creates a sense of approximate overlap.

[video width="1280" height="720" mp4="http://digitaldefoe.org/wp-content/uploads/2015/11/Fig-1_Third-Person-Nassau.mp4" poster="http://digitaldefoe.org/wp-content/uploads/2015/11/fig1still.png"][/video]

In the first three games, players enter the Animus via a specific avatar: series protagonist and Assassin descendant Desmond Miles. These installments maintain a third-person perspective of Miles and the Assassins whose memories he relives; players control the main characters' actions but for the most part view the worlds they occupy from behind and above them (fig. 1). _ACIV_, however, removes the Animus technology from the darker corners of the ancient conspiracy and reinstalls it in the new entertainment division of "Abstergo Industries," a Templar front-company. For the first time in the series, the third-person perspective of a specific twenty-first century avatar vanishes in favor of the first-person perspective of a nameless and faceless Abstergo employee (fig. 2). Players now enter the Animus directly from a workstation in Abstergo Entertainment headquarters; charged with gathering scenes from the past for an upcoming "virtual experience" set during the Golden Age of Piracy, they use what amounts to an in-game game console to look over the shoulder and play out the life of privateer cum-pirate Edward Kenway.

[video width="1280" height="720" m4v="http://digitaldefoe.org/wp-content/uploads/2015/11/Fig-2_Abstergo-Offices-Animus-Workstation.m4v" poster="http://digitaldefoe.org/wp-content/uploads/2015/11/fig2still.png"][/video]

&nbsp;

&nbsp;

&nbsp;

&nbsp;