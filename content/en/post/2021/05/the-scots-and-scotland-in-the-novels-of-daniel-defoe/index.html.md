---
layout: post
title: "The Scots and Scotland in the Novels of Daniel Defoe"
date: 2021-05-29
tags: ["Issue 12.1-Fall 2020","Features2020","Issues"]
---

I

N.H. KEEBLE writes that "Scotland figured more largely in the literary career of Daniel Defoe than in that of any other early modern English writer, at least until Samuel Johnson published his _Journey to the Western Isles of Scotland_ in 1775" (1). The centrality of Scotland in Defoe's non-fiction, extending well beyond his role as a spy and propagandist in the years surrounding the Anglo-Scottish Union, is well known. This emphasis may reflect the deep connections he developed with Scotland over a number of years. Paula R. Backscheider points out that Defoe had established friendships with Scots in London prior to visiting Scotland, and he made many contacts and friends from various walks of life during his repeated visits there (205). Defoe also invested financially in Scotland, in such products as wine, horses, and linen (Backscheider 234-35). And, of course, his son, Benjamin, attended the University of Edinburgh for a time.[[1]](#_edn1)

Defoe engaged with Scottish affairs in his non-fiction well into the 1720s, writing in the _Tour_, for example:

<span style="font-family: Adobe Caslon Pro, serif;">The North Part of _Great Britain_, I mean _Scotland_, is a Country which will afford a great Variety to the Observation ... a Kingdom so famous in the World for Great and Gallant Men, as well States-Men as Soldiers, but especially the last, can never leave us barren of Subject, or empty of somewhat to say of her. (3: 4)</span>

In terms of his creative writing, Defoe wrote a few poems in praise of Scotland-"Caledonia: A Poem in Honour of Scotland, and the Scots Nation" (1706) and "A Scots Poem" (1707). Yet, despite his fascination with North Britain in his non-fiction and an occasional foray into Scottish subjects in his poetry, most of the novels convincingly attributed to him do not address the Scots or Scotland at any great length, as Juliet Shields has noted.[[2]](#_edn2) Oddly, some of the novels that have been de-attributed-such as _The_ _Memoirs of Major Alexander Ramkins, a Highland Officer_ and works on the Scottish seer Duncan Campbell-do focus on the Scots or Scotland, and one wonders if their earlier attribution to Defoe stemmed from a belief that Defoe's profound interest in Scotland in his non-fiction must extend to his novels. If we judge only by the number of references to Scots or Scotland alone in the works we are fairly certain are his, we are forced to conclude that this is not the case. However, despite the side-lining of Scottish matters in the majority of his novels, where Scotland or Scottish characters do appear, Defoe does a lot with a little, foregrounding the centrality of the north to a strong, moral, and hardy Britain, and perhaps paving the way for Tobias Smollett's _Humphry Clinker_, in which Scotland seems to be the intellectual and spiritual cure for a corrupt England.[[3]](#_edn3)

II

Before delving into the treatment of the Scots and Scotland in Defoe's works of prose fiction, it is helpful to revisit, if only briefly, the dominant English attitude toward their neighbors to the north during or around the time his novels were published. Despite Scottish involvement in the 1715 Jacobite rebellion, Defoe's fiction was not written in a time of rampant cultural Scotophobia, especially of an anti-Highlander nature. It is far easier to find unpleasant accounts of Scots and Scotland before the Union, when, for example, one writer quipped in 1705,

<span style="font-family: Adobe Caslon Pro, serif;">A Modern _Scot_ is so averse to good</span>

<span style="font-family: Adobe Caslon Pro, serif;">His daily Study is Ingratitude.</span>

<span style="font-family: Adobe Caslon Pro, serif;">............................................</span>

<span style="font-family: Adobe Caslon Pro, serif;">Their Countrey is that barren Wilderness</span>

<span style="font-family: Adobe Caslon Pro, serif;">Which _Cain_ did First in banishment possess;</span>

<span style="font-family: Adobe Caslon Pro, serif;">.............................................</span>

<span style="font-family: Adobe Caslon Pro, serif;">Pimps, Bullies, Traitors, Robbers, (tis all one,</span>

<span style="font-family: Adobe Caslon Pro, serif;">_Scotland_ like wide-jaw'd Hell, refuses none. ("The Character of a Scot" 8, 12)</span>

There was also, of course, a particularly virulent anti-Scottish period well after Defoe's death, between 1745 and the 1760s, during Lord Bute's brief and heady moments as first minister.[[4]](#_edn4) However, even then, the Scots as a people were not necessarily blamed. Paul Langford notes that in England "there seems to have been no attempt to victimise resident Scots" during either rebellion (152). And a review of works published in Britain between 1715 and 1725-shortly before and during the publication of Defoe's novels-reveal that only a few harshly stereotype the Scots, indicating that Scotophobic sentiment likely fluctuated in the early eighteenth century.

This is not to say that such stereotypes did not circulate in private and/or public after the Union. In 1715, for instance, the diarist Dudley Ryder, while training for the law at the Middle Temple, summarizes a conversation he had in which one Mr. Bowes articulates a rather harsh anti-Scottish perspective:

<span style="font-family: Adobe Caslon Pro, serif;">_Thursday, April_ 26. Went to Westminster Hall. Came home with Mr. Bowes and Leeds. Mr. Bowes upon the mentioning of a Scotchman took occasion to tell us that he hated the name and sight of a Scotchman, for it was the genius and nature of that nation to be tricking cheating rogues that have always a design to deceive and defraud you. I thought he was too general in his invectives, though I think that they have more generally a disposition to play the knave than the English. They have especially the art of dissembling and carry it with the greatest respect and outward deference. They have the art of address and flattery to a great degree of impudence, that they are generally never ashamed or afraid to intrude into company, but push themselves forward wherever they are.[[5]](#_edn5) (226-27)</span>

In a more jovial vein, John Couper published a broadside, _Bag-Pipes no Musick: A Satyre on Scots Poetry_ in 1720 that relies on contemporary stereotypes of the boorish Scot to mock Scottish pretensions to literary and musical culture in an English context, as some lines readily demonstrate:

<span style="font-family: Adobe Caslon Pro, serif;">Scotch _Moggy_ may go down at _Aberdeen_</span>

<span style="font-family: Adobe Caslon Pro, serif;">Where Bonnets, Bag-pipers, and Plaids are seen;</span>

<span style="font-family: Adobe Caslon Pro, serif;">But such poor Gear no Harmony can sute,</span>

<span style="font-family: Adobe Caslon Pro, serif;">Much fitter for a _Jew's_ Trump than a Lute:</span>

<span style="font-family: Adobe Caslon Pro, serif;">Low Bells, not Lyres, the highland Cliffs adorn,</span>

<span style="font-family: Adobe Caslon Pro, serif;">Macklean's loud halloo, or Mackgreigor's Horn.</span>

<span style="font-family: Adobe Caslon Pro, serif;">Sooner shall _China_ yield to Earthen Ware,</span>

<span style="font-family: Adobe Caslon Pro, serif;">Sooner shall Abel teach a singing Bear,</span>

<span style="font-family: Adobe Caslon Pro, serif;">Than _English_ Bards let _Scots_ torment their Ear;</span>

<span style="font-family: Adobe Caslon Pro, serif;">Who think their rustic Jargon to explain;</span>

<span style="font-family: Adobe Caslon Pro, serif;">For anes is once, lang long, and two is twain.
<p style="padding-left: 60px;" align="justify"><span style="font-family: Adobe Caslon Pro, serif;">Let them to _Edinbrough_ foot it back,</span>

<span style="font-family: Adobe Caslon Pro, serif;">And add their Poetry to fill their pack[.] (1)</span>

Couper insists that no "mumbl[ing]" and jarring Scot (including Allan Ramsay and his disciples) could ever produce the refined "Poetick Sound" of a John Dryden (1).

Nevertheless, despite such residual stereotypes, in the 1710s and 1720s, a counter tradition was slowly emerging. Literary attempts were being made by English as well as Scottish writers to challenge stereotypes of the Scots as immoral, criminal, uneducated, impoverished, and brutish beings by creating morally complex Scottish characters with a proper place in the British nation. A notable example is Susanne Centlivre's 1714 play _The Wonder_, which features two Scottish characters, the main character Colonel Britton, "a Scottish rover on his way back to Britain following the ... War of the Spanish Succession" (O'Brien 14), and his footman Gibby, who is dressed in Highland clothing because, as Colonel Britton says, "This is our Country Dress you must know, which, for the Honour of _Scotland_, I make all my Servants wear" (Centlivre 50). While Gibby speaks in some odd Scottish dialect and is the fool of the play, Colonel Britton is well integrated into a newly configured Britain as he speaks in perfect English dialect and his name suggests he represents the recently unified nation. While Centlivre plays with Scottish stereotypes, she also celebrates the anglicised, military Scot. A second example is Eliza Haywood's 1728 _The __Agreeable Caledonian_, a typical Haywoodian romance, but one that foregrounds a noble Scot: the young, attractive, virile, and enterprising Glencairn.[[6]](#_edn6) Therefore, English cultural conceptions of Scotland were starting to change and a more sympathetic relationship with North Britains was beginning to develop, one ingrained in a  sense of the "_Scots_" as "so bold and brave a people," to borrow a phrase used by Joseph Addison in _The Spectator_ on May 21, 1711 (53).[[7]](#_edn7)

III

Though Defoe did describe the Scots in negative terms when he was frustrated with them, especially during the Union negotiations, he was one of the formative figures in the emergence of this new "sympathetic Britishness" that Evan Gottleib describes as evolving in the eighteenth century (21).[[8]](#_edn8) Though the references to the Scots and Scotland in his novels are relatively few, they are carefully placed to foreground the importance of Scots in domestic and foreign affairs, specifically during a time of change. With regard to the actual number of such references to Scotland or the Scots, there appear to be none in _Moll Flanders_; only one each in _Roxana_, _A_ _Journal of the Plague Year_, and _Captain Singleton_; and two in each of _Robinson Crusoe _and _Serious Reflections_. However, as we will note later, several of these have considerable significance. The Scot takes on the most substantial role in _The __Farther Adventures of Robinson Crusoe _(1719), _Memoirs of a Cavalier _(1720), and _Colonel Jack _(1722), in which we observe Defoe fashion the embryo of the recuperative narrative of the Scots/Scotland that Tobias Smollett later embraced.[[9]](#_edn9) In particular, in these works, Defoe shows Scots contributing to moral growth and class mobility in domestic space and to forging a strong military presence and emergent trading empire abroad. He does so by accentuating the martial prowess and business acumen of the Scots as well as the advantages of "law, religion, and education" in Scotland, these three spheres remaining in Scottish hands after the Union (Guibernau 598).

In _Serious Reflections_, the final work of the Crusoe trilogy, Crusoe, a Yorkshire man by birth, reflects on honesty and regional character, taking into account the Scot in his speech: "[T]hey say, There is a Sort of Honesty in my Country, _Yorkshire Honesty_, which differs very much from that which is found in these southern Parts about _London_: Then there is a Sort of _Scots_ Honesty, which they say is a meaner Sort than that of _Yorkshire_" (33). Despite the naysayers, "_Scots _Honesty," Crusoe assures us, is "of a very good Kind" because honesty is always best when it is "sow'd with a Sort of Grain call'd _Religion_" (34).[[10]](#_edn10) In having Crusoe favourably compare the honest Scot to the morally upright people in his own shire, Defoe gestures toward Scotland as a place of Protestant spirituality or morality.

_Colonel Jack_ provides the most direct example of Defovian Scottish morality. Juliet Shields has discussed the brief Scottish segment in this novel in terms of Jacobitism and disaffection, adding that Scotland is most significantly understood as a place of impoverishment in the novel, "a region of dearth" in which the Scots often struggle for daily survival (39). Much to Captain Jack's chagrin, he cannot even practice his pickpocketing skills in Scotland as a result of poverty and, amusingly, impenetrable Scottish plaid:

<span style="font-family: Adobe Caslon Pro, serif;">for as to the Men, they did not seem to have much Money about them; and for the Women, their Dress was such, that had they any Money, or indeed any Pockets, it was impossible to come at them; for wearing large Plads about them, and down to their Knees, they were wrap'd up so close, that there was no coming to make the least attempt of that kind. (147)</span>

However, Scotland is also shown to offer a corrective to the criminal London underground that both Colonel and Captain Jack inhabit. Initially, Scotland is viewed simply by both men as a safe haven, a way to escape English problems. On this subject, Colonel Jack informs the reader, "for we had ... been assur'd, that when we came out of _England_, we should be both Safe, and no Body could Hurt us, tho' they had known us" (132-33). Justice in Scotland, however, is severe as the young English rogues discover when they have an encounter with two half-naked pickpocketers being whipped through the streets of Edinburgh (148-50). Colonel Jack remarks that the Scots are "the severest People upon Criminals of" his "kind in the World"-suggesting that the brutal arm of the law ensures justice is taken seriously (147).

Defoe suggests in _Colonel Jack_ that the Scottish commitment to punishing crime in the interests of justice may be deeply rooted in its strong religious identity.[[11]](#_edn11) The country may be economically impoverished, but it is not morally penurious. When in Kelso, Jack pays attention to the Presbyterian Church, which is "very large and throng'd with People," including the nobility, implying that Caledonian communities are committed to higher moral principles, a commitment that appears to rub off on Jack (147). After all, it is in Edinburgh that Colonel Jack resolves to return the horse that he stole to its rightful English owner, attempting thereby to make amends for his crime: "I had a secret Resolution, if I had gone back to _England_, to have restor'd him [the stolen horse] to the Owner, at _Puckeridge_, by _Ware_; and so I should have wrong'd him of nothing, but of the use of him for so long time" (149).

However, it is more important to the trajectory of the novel that Scotland also offers alternatives to crime, a means to better oneself through education. It is in Scotland that Colonel Jack gains an education that allows him to become far more cultured. In Scotland, a Stabler takes the illiterate Colonel Jack to "an honest, but a poor young Man" who taught him "both to Write and Read" "in a little time ... and for a small Expence" (151). While a series of unfortunate events and the influence of Captain Jack prevent him from immediately taking advantage of this newly acquired knowledge, Defoe presents his instruction in Scotland as vital to Colonel Jack's ultimate success as an honest businessman. When Colonel Jack eventually becomes part of the emergent empire, he can engage with well-respected historical, classical, military, and geographic works, writing,

<span style="font-family: Adobe Caslon Pro, serif;">as I had learn'd to Read, and Write when I was in _Scotland_; so I began now to love Books, and particularly I had an Opportunity of Reading some very considerable Ones; such as _Livy's _Roman History, the History of the _Turks_, the _English _History of _Speed_, and others; the History of the _Low Country_ Wars, the History of _Gustavus Adolphus_, King of _Sweden_, and the History of the _Spaniard's_ Conquest of _Mexico_. (200-01)</span>

If his readers fail to pick up on the importance of Scotland to Jack's intellectual and moral prowess by the middle of the book, Defoe ensures that it is reiterated at its end, where Colonel Jack reaffirms that it is in Scotland that he begins his journey toward a principled and cultured life, gaining a foundation upon which his Bristol Tutor builds. Jack recalls,

<span style="font-family: Adobe Caslon Pro, serif;">I had been bred indeed to nothing of either religious, or moral Knowledge; what I gain'd of either, was first by the little time of civil Life, which I liv'd in _Scotland_, where my abhorrence of the wickedness of my Captain and Comrade, and some sober religious Company I fell into, first gave me some Knowledge of Good and Evil, and shew'd me the Beauty of a sober religious Life[.] (339)</span>

Therefore, while any apparent association of Scotland with Jacobitism might, as Juliet Shields suggests, make the nation somewhat suspect to Defoe's readers (39), the greater emphasis on its facilitation of Jack's education and spiritual health deflects attention toward the moral and cultural potential Scotland offers to its often-combative southern neighbour.

Most references to Scots or Scotland in Defoe's fiction appear when he writes of Continental military adventures or an emergent empire rather than a domestic space. _Memoirs of a Cavalier _proves pertinent here as much of the Cavalier's narrated life is spent in foreign parts rather than in England. In this novel, Defoe carefully crafts beneficial homosocial encounters with Scots abroad in order to offset in the minds of his readers many of the accusations directed at Scots at home. The first half of _Memoirs_ sets out the Cavalier's role in fighting Continental wars, initially with France, where he has to pass as a Scot in order to avoid French prejudice against the English, and then (more successfully) with the great Swedish leader Gustavus Adolphus, considered the "father of modern war" (Dupuy). Here the Cavalier comes into contact with actual Scots, and he is impressed by their valour. He finds that his father has been acquainted with one of the Scots, Sir John Hepburn, who is irreplaceable to the Swedish monarch, and in whose regiment the English Cavalier asks to serve.

The respect and admiration the Cavalier forms for the Scots abroad makes his later experience of the civil wars at home deeply problematic, allowing Defoe to highlight that even in this depiction of pre-union Britain, the mutual interests the English and Scots have abroad make dissension at home unnatural. Although the Cavalier answers in the affirmative when Charles I asks him whether he is "willing to serve him against the _Scots_," throughout his account of the civil wars, the Cavalier highlights the striking military prowess of Scottish soldiers (122). The English regiment in which he finds himself is often "disorderly and shameful" in its encounters with the Scots (124). The Scottish army is described as "bold and ready, commanded by brave Officers," and the Cavalier later notes that his regiment was "not a Match" for it (126, 130). Elsewhere he writes of a Scots military gentleman who sought to engage in single combat with an English counterpart, and only an "old Lieutenant" was courageous enough to come out to meet him when "no Body would stir" (131). The Scots soldier, we are told, "used" the "stout old [English] Soldier" "very generously" after capturing him: he "treated him in the [Scottish] Camp very courteously, gave him another Horse, and set him at Liberty, _gratis_" (131).

Defoe's admiration is somewhat tempered when he turns to Highlanders, who are more curious beings for the Cavalier, less civilized in some respects. Of them he writes, "I confess, the Soldiers made a very uncouth Figure, especially the _Highlanders_: The Oddness and Barbarity of their Garb and Arms seemed to have something in it remarkable" (133). Yet despite this visible difference, Defoe ultimately describes them as skilled and courageous. He writes that these "generally tall swinging Fellows" are vigorous enough "to endure Hunger, Cold, and Hardships" and that they are "wonderfully swift of Foot," capable of "keep[ing] Pace with the Horse" (133, 130).

When Defoe does engage with accusations directed at the Scots in relation to the civil wars, he again deflects attention from them in the same moment he acknowledges them. For example, while the Cavalier describes "the _Scots_" as "headstrong" and "zealous for their own Way of Worship," he claims in the same sentence that "[a]ll Men blamed _Laud_ for prompting the King to provoke" them into civil war (136). The Cavalier's father also suggests that unnamed members of Charles I's inner circle must also be blamed for the civil wars, since "he feared there was some about the King who exasperated him too much against the _Scots_, and drove things too high" (121). Even in terms of the Scots handing over Charles I to the English in exchange for money, the Cavalier remains relatively uncritical of them. He blames every party in some respect at the end of the novel, so the Scot is presented as neither more nor less errant or guilty than the English. Interestingly, Defoe places some emphasis on the Scottish collusion with the English Parliament to undermine the king's authority, so the Scots and English are pictured working together against the king. The punishment received by the Scots is attributed to the fact that they "unjustly assisted the [English] Parliament to conquer their lawful Sovereign, contrary to their Oath of Allegiance" (276-77). Charles I is also blamed for not working with the Scots, "grant[ing] ... their own Conditions," which would have allowed him to enter Scotland and remain safe (278). In the _Memoirs_, therefore, Defoe creates a kind of British union between the Scots and the English Cavalier abroad, so the conflict between the Scots and English armies at home seems dreadful. When he laments that during battles, he is "moved ... to Compassion" by hearing someone "cry for Quarter in _English_," this likely included some of his Scottish countrymen, and when he notes that "[h]ere I saw my self at the cutting of the Throats of my Friends; and indeed some of my near Relations. My old Comerades and Fellow-soldiers in _Germany_, were some _with us_, some _against us_," he may be referring to the Scots who were vital allies abroad (165).

Thus far, we have argued that Defoe's novels present the Scots as a religious and moral people who are devoted to securing justice and facilitating social mobility through education, and who are skilled and courageous soldiers. We have also suggested that his novelistic representations of the Scot create a vision of British solidarity abroad, which points to and encourages the possibility of unity and harmony at home. In so doing, he works to undermine Anglo-Scottish discord. References to travel, however, are not solely directed at recuperating domestic relations, but also at building strong trading relations overseas. And this is particularly important in novels written only decades after the failed Darien venture, in which attempts to build a distinctly Scottish commercial empire had gone sadly awry. Maximillian E. Novak reminds us that Defoe's "A Scots Poem"-written in 1707 in the voice of a Scot-specifically encourages Scots to join the imperial project:

<span style="font-family: Adobe Caslon Pro, serif;">I'd fearless venture to the Darien Coast;</span>

<span style="font-family: Adobe Caslon Pro, serif;">Strive to retri[e]ve, the former Bl[i]ss we lost,</span>

<span style="font-family: Adobe Caslon Pro, serif;">Yea, I wou'd view _Terra incognita_.</span>

<span style="font-family: Adobe Caslon Pro, serif;">And climb the Mountains of America. (qtd. in Novak 310)</span>

In Defoe's novels, Scots seem to have heeded this call as they sometimes appear just in the nick of time to save English protagonists from an unpleasant fate. For example, in the brief, but poignant references in the first and third volumes of the Crusoe trilogy, Scots suddenly appear to facilitate the sailor's journey. In _Robinson Crusoe_, Crusoe, having managed to "escape out of slavery from the _Moors_ at _Sallee_" by boat, is unable to persuade sailors in a larger Portuguese vessel to rescue him until "at last a _Scots_ sailor who was on board, call'd to" him and he responded, saying that he "was an _Englishman_" recently freed from captivity (28). At that point, they instruct Crusoe to "come on board, and very kindly" take him and all of his "goods" onto their ship (28). The Scotsman is an interpreter and translator, mediating between Crusoe and the Continental Europeans, and, as a result, Crusoe is saved.

In _The Farther Adventures_, Scots take on a more substantial role.[[12]](#_edn12) The first appearance of Scottish characters occurs when Crusoe is comforted by the fact that the "Company" amongst which he finds himself in China includes five Scotsmen. They are praised in this case for their economic strengths. Crusoe tells his readers that the five "appear'd ... to be Men of great Experience in Business, and Men of very good Substance" (309). In the second reference to these Scots merchants, it is evident that some also have great military skill. One advises the group how to deal with, and leads them against, "forty or fifty" Tartars, during which the Scot shows exceedingly great determination and daring against the enemy:

<span style="font-family: Adobe Caslon Pro, serif;">One of the _Scots_ merchants of _Muscow_ happen'd to be amongst us; and as soon as he heard the Horn, he told us in short, that we had nothing to do, but to charge them immediately without loss of Time; and drawing us up in a Line, he ask'd if we were resolv'd, we told him we were ready to follow him; so he rode directly up to them[.] (315)</span>

In the third reference to these characters, one of the Scottish merchants reveals himself to be gifted at recognizing the 'true' Christian religion, thereby keeping Crusoe on the narrow path of the Christian faith. When the group arrives at Argun, on the "Frontiers of the _Muscovite_ Dominions" (325), that Scotsman warns Crusoe not to confuse false with true forms of Christianity, and his reading of Muscovite Christianity, according to Crusoe, turns out to be all too accurate:

<span style="font-family: Adobe Caslon Pro, serif;">Now we came, where at least, a Face of the Christian Worship appeared ... and it made the very Recesses of my Soul rejoice to see it:  I saluted the brave _Scots _Merchant ... with my first acknowledgment of this; and taking him by the Hand, I said to him, blessed be God, we are once again come among Christians; he smil'd, and answered, do not rejoice too soon Countryman, these _Muscovites_, are but an odd Sort of Christians ... Well, says I, but still 'tis better than Paganism, and worshipping of Devils: Why, I'll tell you, says he, except the _Russian_ Soldiers in Garrisons, and a few of the Inhabitants of the Cities upon the Road, all the rest of this Country, for above a thousand Miles farther, is inhabited by the worst and most ignorant of Pagans; and so indeed we found it. (325-26)</span>

Given the religious authority assigned to this Scots merchant, it is no surprise that Crusoe turns to him as a companion in the attempt to annihilate idolatry, symbolized by the destroying of the "Idol made of Wood, frightful as the Devil" that they come across in a village in Nortziuskoy (329). The Scots merchant speaks rationally in response to Crusoe's plan to destroy the idol, discussing its lack of utility. The Scots merchant, however, does not turn his back on Crusoe, later agreeing to go with him in a sign of religious alliance, bringing a big, burly, zealous Scot with him-religious fantasy to be sure. Interestingly, in this section, there is not one single type of Scot: there is one with great zeal who is prone to violence, from which Crusoe distances himself, and one that is moderate and reasonable, to whom Crusoe attaches himself. The moderate Scot, Defoe's figure of rational religion, works to save the lives of the priests who watch the idol burn, first suggesting tying them up rather than killing them and then waiting until the fire burns out to avoid the priests throwing themselves into it. In the final reference to the Scots in _Farther Adventures_, the Scots merchant introduces Crusoe to his acquaintance, the governor of Adinskoy, who offers to provide him with "a Guard of fifty Men" if he feels there will be "any Danger" travelling "to the next Station" (345). Once again, a Scot mediates as needed to ensure the safety and well-being of an Englishman.

In the Crusoe trilogy, therefore, when the Englishman finds himself in a foreign land or reflects on his journeys in it, the Scot often plays critical economic, military, and religious roles. United with commercially astute, religiously discerning, and/or martially gifted Scots who help to translate and mediate for the foreigner, Englishmen can survive and even flourish. When the eponymous hero of Defoe's _Captain Singleton_ finds himself in a bind, he similarly turns to a Scot. When he needs a hardy and loyal sailor to man William's vessel for him, he declares, "so I put a _Scotsman_, a bold, enterprizing, gallant Fellow, into her [the sloop], named _Gordon_," and they "sailed away for the Cape of _Good Hope_" (215). The Scot is once again portrayed as an invaluable resource for the Englishman and his ventures abroad.

IV

In representing the Scots and Scotland in his novels, Defoe makes a great effort, albeit indirectly, to promote and uphold the Union, just as he does in his works of non-fiction, especially his political prose. In order to encourage such national cooperation in his English readers, Defoe highlights the moral, military, mercantile, and educational strengths of the Scot. But the novels reveal not only the benefits of Anglo-Scottish partnerships and harmonious relations, but also suggest that Scotland has a remediating rather than a detrimental effect on British identity. Many of the Scots that populate his novels facilitate the ability of his English protagonists to achieve intellectual, spiritual, martial, and commercial success. Just as Moll Flanders needs America to attain her life's goal to be a gentlewoman, so Colonel Jack, the Royalist Cavalier, and Robinson Crusoe need Scotland to help them reach the high ideals of a newly formed Britain.[[13]](#_edn13)

NOTES

[1.](#_ednref1)For an extensive background on Defoe and Scotland, see Paula R. Backscheider, _Daniel Defoe: His Life,_ 203-52, and Maximillian E. Novak, _Daniel Defoe, Master of Fictions: His Life and Ideas_, 289-337.

[2.](#_ednref2) In "What's British about _The British Recluse_? The Political Geography of Early Eighteenth-Century Fiction," Juliet Shields notes, "Despite Scotland's prominence in Defoe's writing more generally, it figures explicitly in only one of his novels, _Colonel Jack, _and there only briefly as the antithesis of London" (39). In this article, we seek to complicate this reading of the Scots and Scotland in Defoe's novels.

[3.](#_ednref3) Unlike those of Defoe, the political (Tory) commitments of Smollett motivated him to position Scotland as an alternative to an England overly concerned with commercial matters (drowning in luxury and excess). Although it is beyond the purview of our article, Rivka Swenson, in _Essential Scots and the Idea of Unionism in Anglo-Scottish Literature,1603-1832_, has pointed out that even works by Defoe that do not seem to engage directly with Scotland, such as _Robinson Crusoe_, can be seen as contributing to discussions of the Anglo-Scottish Union. On this subject, she remarks, "the Union, and unionism, is _the_ source for the Crusoe story, formally as well as substantively" (52).

[4.](#_ednref4) We should keep in mind, as Linda Colley notes in _Britons: Forging the Nation 1707__-__1837_, that what appears to be anti-Scot invective may simply be anti-Highlander (and by extension anti-Jacobite) sentiment in the eighteenth century, since the Highlanders were, for good reason at the time, associated with the Jacobites. It was not uncommon, especially after the Jacobite Rebellions, to view Scottish Lowlanders and Highlanders as two distinct groups.

[5.](#_ednref5) Ryder later served as an MP first for St Germans and later for Tiverton. Knighted in 1740, Ryder served in a variety of roles during his career, including solicitor-general, attorney-general, and eventually chief justice of the king's bench (Lemmings).

[6.](#_ednref6) For a brief, but helpful review of this rarely discussed work, see Susan Paterson Glover, _Engendering Legitimacy: Law, Property, and Early Eighteenth-Century Fiction_, 150-51.

[7.](#_ednref7) Richard Steele also wrote very highly of the Scots in a letter dated November 15, 1717 to his wife regarding his reception in Edinburgh: "You cannot imagine the civilities and honours I had done me there, and never lay better, ate or drank better, or conversed with men of better sense than there" (1: 211-12). The Scottish writer John Arbuthnot also renovated inherited stereotypes in the John Bull Pamphlets (1712). John Bull's sister Peg may be impoverished and shrivelled but she is also energetic, intelligent, feisty, and somewhat agreeable. The Scots likewise picked up on negative representations of themselves and sought to transform them.

[8.](#_ednref8) For example, Defoe refers to the Scots as "surly," "haughty" and "recalcitrant" (qtd. in Backscheider 227, 251).

[9.](#_ednref9) Though in Smollett's case, he presented a Tory point of view.

[10.](#_ednref10) Later in _Serious Reflections_, Crusoe warns against the dangers of division, reminding his readers of the violence that has occurred in Scotland and Ireland because of religious division, leading to the "Flame of War" which is "always quench'd with Blood" (253).

[11.](#_ednref11) On the subject of Defoe's appreciation for and advocacy of the Scottish Presbyterian Church or Kirk, see Holly Faith Nelson and Sharon Alker's "Daniel Defoe and the Scottish Church."

[12.](#_ednref12) See Chapter 1 of Swenson's _Essential Scots_ for a more extensive discussion of the _Farther Adventures_, particularly as it relates to Union fantasies.

[13.](#_ednref13) We would like to thank our research assistant Clayton Andres for his excellent work on this project. We are also grateful to the two anonymous peer reviewers who provided helpful comments on an earlier draft of the article.

WORKS CITED

Addison, Joseph. _Selections from "The Spectator_,_"_ edited by J.H. Lobban, Cambridge UP, 1952.

Backscheider, Paula R. _Daniel Defoe: His Life_. Johns Hopkins UP, 1989.

Centlivre, Susanna. _The Wonder: A Woman Keeps a Secret_, edited by John O'Brien, Broadview, 2004.

"The Character of a _Scot_." _A Trip Lately to Scotland. With a True Character of the Country and People_. London, 1705.

Colley, Linda. _Britons: Forging the Nation 1707-1837_. Yale UP, 1992.

Couper, John. _Bag-pipes no Musick: A Satyre on Scots Poetry_. Oxford, 1720.

Defoe, Daniel. _Colonel Jack_, edited by Gabriel Cervantes and Geoffrey Sill, Broadview, 2016.

---. _The Farther Adventures of Robinson Crusoe_. London, 1719.

---. _The Life, Adventures, and Pyracies, of the Famous Captain Singleton_, edited by Manushag N. Powell, Broadview, 2019.

---._ Memoirs of a Cavalier_, edited by James T. Boulton, Oxford UP, 1991.

---. _Robinson Crusoe_, edited by John Richetti, Penguin, 2001.

---. _A Scots Poem_. Edinburgh, 1707.

---. _Serious Reflections during the Life and Surprising Adventures of Robinson Crusoe_. London, 1720.

---. _A Tour Thro' the Whole Island of Great Britain_, vol. 3. London, 1727.

Dupuy, Trevor N. _The Military Life of Gustavus Adolphus: Father of Modern War_. Watts, 1969.

Glover, Susan Paterson. _Engendering Legitimacy: Law, Property, and Early Eighteenth-Century Fiction_. Bucknell UP, 2006.

Gottleib, Evan. _Feeling British: Sympathy and National Identity in Scottish and English Writing 1707-1832_. Bucknell UP, 2007.

Guibernau, Montserrat. "Nationalism without States." _The Oxford Handbook of the History of Nationalism_, edited by John Breuilly, Oxford UP, 2013, pp. 592-614.

Haywood, Eliza. _The Agreeable Caledonian_. London, 1728.

Keeble, N.H. Introduction. _Memoirs of the Church of Scotland_, edited by N.H. Keeble, Pickering & Chatto, 2002.

Langford, Paul. "South Britons' Reception of North Britons, 1707-1820." _Anglo-Scottish Relations from 1603-1690_, edited by T.C. Smout, Oxford UP, 2005, pp. 143-69.

Lemmings, David. "Ryder, Sir Dudley [1691-1756], judge." _Oxford Dictionary of National Biography _(online ed.), Oxford UP, 2004, doi-org.ezproxy.whitman.edu/10.1093/ref:odnb/ 24394.

Nelson, Holly Faith, and Sharon Alker. "Daniel Defoe and the Scottish Church." _Digital Defoe: Studies in Defoe & His Contemporaries_, vol. 5, no. 1, Fall 2013, pp. 1-19.

Novak, Maximillian E. _Daniel Defoe, Master of Fictions: His Life and Ideas_. Oxford UP, 2001.

O'Brien, John. Introduction. _The Wonder: A Woman Keeps a Secret_, by Susanne Centlivre, edited by John O'Brien, Broadview, 2004.

Ryder, Dudley. _The Diary of Dudley Ryder 1715__-__1716_, edited and translated by William Matthews, Methuen & Co., 1939.

Shields, Juliet. "What's British about _The British Recluse_? The Political Geography of Early Eighteenth-Century Fiction." _Representing Place in British Literature and Culture, 1660-1830: From Local to Global_, edited by Evan Gottleib and Juliet Shields, Ashgate, 2013, pp. 31-46.

Steele, Richard. _The Epistolary Correspondence of Sir Richard Steele_. London, 1787. 2 vols.

Swenson, Rivka. _Essential Scots and the Idea of Unionism in Anglo-Scottish L__iterature,1603-1832._ Bucknell UP, 2016.