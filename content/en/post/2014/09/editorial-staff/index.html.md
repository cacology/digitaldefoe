---
layout: post
title: "Editorial Staff"
date: 2014-09-25
---

[Stephanie Hershinow](https://stephaniehershinow.hcommons.org/) is associate professor of English at Baruch College, City University of New York, where she focuses on the history and theory of the novel, eighteenth-century fiction, and law and literature. Her book _[Born Yesterday: Inexperience and the Early Realist Novel](https://jhupbooks.press.jhu.edu/title/born-yesterday)_ (JHUP 2019) argues that eighteenth-century novels experimented with the limits of realism through their depictions of adolescence. Her essays have appeared in _Novel_, _Eighteenth Century: Theory and Interpretation_, _Studies in Eighteenth-Century Culture_, _Romantic Circles_, and _Eighteenth-Century Fiction_. She is currently editing Jane Austen's _Emma_ for [W.W. Norton](https://wwnorton.com/norton-library), and her next book is a literary history of legal personhood.

[Christopher Loar](https://chss.wwu.edu/english/english-faculty-christopher-loar) is an Associate Professor at Western Washington University, where he teaches literatures of the long eighteenth century from both Britain and the Americas. He has published essays on imperial military memoirs, James Boswell's melancholy, and Robinson Crusoe's firearms. His first book, [_Political Magic: British Fictions of Savagery and Sovereignty, 1650-1750_](http://www.amazon.com/Political-Magic-Fictions-Sovereignty-1650-1750/dp/082325691X) (Fordham 2014) examines the role that the concept of savagery played in this period's fictional reimagining of political categories. His current research interests include environmental writing in the long eighteenth century, narratives of the British empire, and early modern political theory.

[Denys Van Renen](https://www.unk.edu/academics/english/facultystaff/denys_vanrenen.php) is an Associate Professor at University of Nebraska at Kearney, where he teaches Science Studies, long-eighteenth-century texts, and environmental humanities.  He has published in _Comparative Drama_, _Eighteenth-Century Fiction_, _SEL Studies in English Literature 1500-1900_, _College Literature_, _Journal of Narrative Theory_, _Restoration: Studies in English Literary Culture, 1660-1700_, among other journals.  His first book, _[The Other Exchange: Women, Servants, and the Urban Underclass in Early Modern English Literature](https://www.nebraskapress.unl.edu/nebraska/9780803280991/) _(University of Nebraska Press, 2017), foregrounds the economies of women and laborers; his second, [_Nature and the New Science in England, 1665-1726_](https://www.liverpooluniversitypress.co.uk/books/id/54600/) (Oxford University Studies in The Enlightenment, 2018), showcases the interdependences among natural systems, aesthetic practices, and national identity.  He has also co-edited the volume, _[Beyond 1776 Globalizing the Cultures of the American Revolution](https://www.upress.virginia.edu/title/5162) _(University of Virginia 2018).

**Book Review Editor**

[Rivka Swenson](https://english.vcu.edu/people/faculty/swenson.html) is Associate Professor of English at Virginia Commonwealth University, where she especially enjoys teaching transhistorical courses on forms, modes, and genres with strong eighteenth-century roots (the Robinsonade, the Gothic). Her first book, _Essential Scots and the Idea of Unionism in Anglo-Scottish Literature, 1603-1832_ argued that the aesthetic politics of Unionism shaped the form of prose narratives during this period, in conversation with the cross-media cultural conceptualization of Britishness and subnational protest as aesthetic systems. Essays have appeared in journals such as _The Eighteenth-Century: Theory and Interpretation_ and _Studies in Eighteenth-Century Culture _and collections such as _The Oxford Handbook of British Poetry, 1660-1830 _and _The Cambridge Companion to "Robinson Crusoe_."

**Technical Editor**

Michelle Lyons-McFarland is a lecturer at Case Western Reserve University, currently teaching Professional and Technical Communication for Engineers. Her current book project, _Literary Objects in Eighteenth-Century Literature_, focuses on the evolving ways authors in the long eighteenth-century interacted with material culture to imbed tacit understandings about character within their texts. Her research interests include descriptive bibliography, book history, material culture, and gothic literature.

**Projects Editor**

[Krista E. Roberts](https://t.co/n1XWZWzUxd) is a lecturer at the University of Illinois at Urbana-Champaign. Before joining _Digital Defoe_, she served on the editorial staff for various academic journals, including _a'b_: _Auto'Biography Studies_. Through her research, she examines the construction of medical authority vis a vis the testimonies and descriptions of women, nonhuman animals, and material actants in medical texts published between 1700 to the contemporary era.

**Social Media Editor**

[Octavia Cox](https://www.conted.ox.ac.uk/tutors/7139)

**Editorial Review Board**

Leith Davis
Simon Fraser University

Stephen H. Gregg
Bath Spa University

Penny Pritchard
University of Hertfordshire

Pat Rogers
University of South Florida

Janet Sorensen
University of California Berkeley

Rivka Swenson
Virginia Commonwealth University

**Founding Editors**

Katherine Ellison
Illinois State University

Holly Faith Nelson
Trinity Western University

**Editorial Assistant**

Jaimie Gleissner

**Site Design **

Katherine Ellison & Keely Siciliano