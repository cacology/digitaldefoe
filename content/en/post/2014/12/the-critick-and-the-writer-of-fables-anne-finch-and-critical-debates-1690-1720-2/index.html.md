---
layout: post
title: "“The Critick and the Writer of Fables”: Anne Finch and Critical Debates, 1690 – 1720"
date: 2014-12-02
tags: ["Issues","Article Notes","Issue 6.1 - Fall 2014"]
---

##   

          <a name="works" id="works"></a>WORKS CITED

    Ades, John I. "'Fit Letters Though Few': Dryden's Correspondence." _Papers on Language and Literature _19.3 (1983): 263−79. _Academic Search Complete_. Web. 12 May 2014.   

Audra, E. and Williams, Aubrey. "Introduction to _An Essay on Criticism_." _Pastoral Poetry and An Essay on Criticism_. Ed. E. Audra and Aubrey Williams. London: Methuen, 1961. 197−235. Print.   

Backscheider, Paula R. _Eighteenth-Century Women Poets and Their Poetry: Inventing Agency, Inventing Genre_. Baltimore: Johns Hopkins UP, 2008. Print.   

---. and Catherine E. Ingrassia, eds. _British Women Poets of the Long Eighteenth Century_. Baltimore: Johns Hopkins UP, 2009. Print.   

Barash, Carol. _English Women's Poetry, 1649−1714: Politics, Community, and Linguistic Authority_. Oxford: Oxford UP, 1996. Print.   

---. "The Political Origins of Anne Finch's Poetry." _Huntingdon Library Quarterly_ 54 (1991): 327−51. Print.   

Benedict, Barbara M. "Publishing and Reading Poetry." _The Cambridge Companion to Eighteenth-Century Poetry_. Ed. John Sitter. Cambridge: Cambridge UP, 2001. 63−82. Print.   

Dryden, John. "To My Lord Chancellor. Presented on New-years-day." _Poems and Fables_. Ed. James Kinsley. London: Oxford UP, 1961. 28−32. Print.   

---. _Sylvae, or, the second part of the Poetical Miscellanies_. London: Jacob Tonson, 1685. _Early English Books Online_. Web. 12 May 2014.   

Ezell, Margaret J.M. _Writing Women's Literary History_. Baltimore: Johns Hopkins UP, 1993. Print.  

Fairer, David. _English Poetry of the Eighteenth Century, 1700−1789_. London: Pearson, 2003. Print.   

Finch, Anne. _The Anne Finch Wellesley Manuscript Poems: A Critical Edition_. Ed. Barbara McGovern and Charles Hinnant. Athens, GA: The U of Georgia P, 1998. Print.   

---. _The Poems of Anne Countess of Winchilsea. From the original edition of 1713 and from unpublished manuscripts_. Ed. Myra Reynolds. Chicago: U of Chicago P, 1903. Print.   

Gavin, Michael. "Critics and Criticism in the Poetry of Anne Finch." _English Literary History_ 78.3 (2011): 633−55. Print.   

Gerrard, Christine. Introduction. _A Companion to Eighteenth-Century Poetry_. Ed. Christine Gerrard. Malden, MA: Blackwell, 2006. 1−4. Print.   

Griffin, Robert J. "The Age of 'The Age of' is Over: Johnson and the New Versions of the Late Eighteenth-Century." _Modern Language Quarterly_ 62.4 (2001): 377−92. _Academic Search Complete_. Web. 12 May 2014.   

Hammond, Paul. _The Making of Restoration Poetry_. Cambridge: D.S. Brewer, 2006. Print.   

Hunter, J. Paul. "Canon of Generations, Generation of Canons." _Modern Language Studies_ 18.1 (1988): 38-46. _Academic Search Complete_. Web. 12 May 2014.   

--- "Missing Years: On Casualties in English Literary History, Prior to Pope." _Common Knowledge_ 14.3 (2008): 434−44. Print.   

Lonsdale, Roger, ed. Introduction. _The New Oxford Book of Eighteenth-Century Poetry_. Oxford: Oxford UP, 1984: xxxiii−xli. Print.   

McGovern, Barbara. _Anne Finch and her Poetry: A Critical Biography_. Athens, GA: U of Georgia P, 1992. Print.   

McGovern, Barbara, and Charles Hinnant. Introduction. _The Anne Finch Wellesley Manuscript Poems: A Critical Edition_. Ed. Barbara McGovern and Charles Hinnant. Athens GA: The U of Georgia P, 1998. xv−xli. Print.   

Myers, William. _Dryden_. London: Hutchinson, 1973. Print.   

Nokes, David. "Pope's Friends and Enemies: Fighting with Shadows." _The Cambridge Companion to Alexander Pope_. Ed. Pat Rogers. Cambridge: Cambridge UP, 2007. 25−36. Print.   

Parker, Blandford. _The Triumph of Augustan Poetics: English Literary Culture from Butler to Johnson_. Cambridge: Cambridge UP, 1998. Print.   

Pivetti, Kyle. "Coupling Past and Future: Dryden's Rhymes as History." _Modern Philology_ 109.1 (2011): 85−107. _Academic Search Complete_. Web. 12 May 2014.   

Pope, Alexander. "An Essay on Criticism." _Selected Poetry_ Ed. Pat Rogers. Oxford: Oxford UP, 1994.1-19. Print.   

Rudy, Seth. "Pope, Swift, and the Poetics of Posterity." _Eighteenth-Century Life_ 35.3 (2011): 1−28. _Academic Search Complete_. Web. 12 May 2014.   

Tomlinson, Charles. "Why Dryden's Translations Matter." _Translation and Literature_ 10.1 (2001): 2−20. _Academic Search Complete_. Web. 12 May 2014.   

Trolander, Paul, and Zeynep Tenger. "Abandoning Theory: Towards a Social History of Critical Practices." _Critical Pasts: Writing Criticism, Writing History_. Ed Philip Smallwood. Lewisburg, PA: Bucknell UP, 2004: 37−50. Print.   

Williams, Abigail. _Poetry and the Creation of a Whig Literary Culture, 1681-1714_. Oxford: Oxford UP, 2005. Print.   

Stephen Zwicker. _Lines of Authority: Politics and Literary Culture, 1644-1689_. Ithaca, NY: Cornell UP, 1993. Print.   

    </blockquote>