---
layout: post
title: "The Cabinet of Curiosity and the Dressing Room - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<span class="style1 style12">**Readings:  

          ➢	Selections from _The Ladies Cabinet broke Open, Modern Curiosities of Art and Nature, Cabinet of Momus_,   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          and _Cabinet of Choice Jewels_  

➢	Alexander Pope, "Rape of the Lock" and "The Key to the Lock"
**</span>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
              <td height="1770" valign="top" width="27%" align="right">

<span class="style43 style44">[![Le Cabinet de la Bibliotheque de Sainte Genevieve](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/14genevieve0675.jpg)](14genevieve0675.jpg)&nbsp;</span>

Fig. 14. Franz Ertinger, _Le Cabinet de la   

                Bibliothèque de Sainte Geneviève_  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;© The Warburg Institute - University of London.

[![The Cabinet Maker](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/15cabinetmaker.jpg)](15cabinetmaker.jpg)  

                  <span class="style1 style46">Fig. 15. _The Cabinet Maker._   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;© British Library Board, RB.23.a.18153 plate opposite 73.</span>

[![John White](18richcabinetthumb.jpg)](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/18richcabinet.pdf)

Fig. 17: _A Rich Cabinet_. Frontispiece of _A Rich Cabinet_.
</td>
              <td valign="top" width="73%">

<span class="style1 style12">

                When the British elite and a growing group of merchants developed a taste for collecting in the middle of the seventeenth century, they brought into their closets freestanding wooden repositories, and the word _cabinet_-- from the French for "closet"--was increasingly attached to this latter smaller enclosure ([Figure 14](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/14genevieve0675.jpg)). In the eighteenth century, cabinet-makers had a booming trade ([Figure 15](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/15cabinetmaker.jpg)). Multi-sectioned, lockable cabinets permitted not only the safe storage and organization of books, art works, antiquities, natural specimens, and other curios, but also their elegant display. I briefly introduce this practice with reference to a subsection of Michael McKeon's "Subdividing Spaces" (218-19) and Patrick Mauriès's beautifully illustrated _Cabinets of Curiosity_ (see especially III "The Collector: _senex puerilis_," and IV "The Phantom Cabinet: 18th-19th Centuries"), emphasizing the triumph of systematic methods of organization over the collector's subjective experience of awe or wonder. In the eighteenth century, as Mauriès explains, "The concept of the cabinet of curiosities began to change when differences became more important than correspondences. This would lead to the breaking up of the great collections and their re-allocation to specialized institutions, the naturalia to natural history museums and the _artificialia_ to art galleries" (193). The [Ashmolean Museum](http://www.ashmolean.org/about/historyandfuture/) at Oxford, opened in 1683, housed the collection that John Tradescant had originally displayed in his private home; the British Museum, the first national public museum in the world, was founded in 1753 to [exhibit the contents of the private cabinets of naturalist and collector, Sir Hans Sloane](http://www.britishmuseum.org/about_us/the_museums_story/general_history.aspx).   

_[Closets Without Walls](http://108.167.133.27/~keellis/?p=377)_ ([Figure 16](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/closetswithoutwalls.htm)) is my bibliography of 170 publications, most from eighteenth-century England, called "closets" and/or "cabinets," many of which were also qualified as "unlocked" or "broken open."  Its title alludes to the phrase "libraries without walls," which was coined by book and media historian Roger Chartier to refer to the textual _bibliothèques_- book catalogues-popular in eighteenth-century France. Whereas in the French "libraries without walls," publishers confronted the longstanding fantasy that all the books in existence (or at least their titles) might be gathered in one place, the books in the _Closets Without Walls_ archive highlight the important metaphorical role played by private spaces for publishers, and others in the book trade, coming to terms with the growing popularity of print in eighteenth-century England. I introduce the figurative appeal of the closet or cabinet opened with reference to the frontispiece of John White's _Rich Cabinet_ ([Figure 17](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/18richcabinet.pdf)), whose array of boxes is suggestive not only of the residual chaos of natural philosophical knowledge in the seventeenth century but also of the novelty and excitement associated with their public exposure in print. To further investigate this appeal, I ask students to analyze the front matter of _The Ladies Cabinet broke Open, Modern Curiosities of Art and Nature, Cabinet of Momus_, and _Cabinet of Choice Jewels_ as well as three other texts of their own choosing, which they select on the _Closet Without Walls_ bibliography then locate on _Early English Books Online_ or _Eighteenth Century Collections Online_. As the Notes column (G) on the bibliography indicates, in textual closets and cabinets, the figure of private space serves as a very flexible conceptual bridge between an elite, exclusive, manuscript-centered culture of knowledge production and exchange and a growing print culture in which accessibility was increasingly valued.   

The discussion of "Rape of the Lock" focuses on the new light that histories of the closet can shed on it. [The dressing room](http://pudl.princeton.edu/sheetreader.php?obj=j38607033) was the fashionable version of the closet reserved for storing and putting on clothes, accessories, and cosmetics. Following a brief introduction to this space by way of Tita Chico's _Designing Women: The Dressing Room in Eighteenth-Century Literature and Culture_, we explore the impact of a burgeoning consumer culture in eighteenth-century rituals of privacy, especially as depicted in the famous toilet scene at the end of Canto 1 (lines 121-48). Pope clearly both scorns and delights in his characters' love of surfaces. We discuss if and how the quality of this ambivalence differs where the different sexes are concerned. Next we approach the poem as a sort of collector's cabinet: a container for arranging things in relation to one another. In particular, we consider how the poem's many odd groupings-like the "Counsel" and the "Tea" that Queen Anne "sometimes takes" (3.8) or the "twelve vast French Romances, neatly gilt," "three Garters," and "half a Pair of Gloves" (2.38-39) on the Baron's altar to love-comment on the difficulties of Pope's contemporaries in distinguishing between style and substance. Finally, with reference to the satirical paratext "The Key to the Lock," which Pope wrote himself, we consider if and how the poem parodies the genre of secret history.   

**  

Suggested Presentation Topics:**  

Cabinets of curiosities  

The dressing room  

The history of the encyclopedia, the dictionary, the miscellany, and/or the anthology  

Roger Chartier, "Libraries Without Walls"   

Pope's grotto  

Eighteenth-century cosmetics

                </span>

&nbsp;

<span class="style1 style12">  

                </span>

              </td>
            </tr>
          </tbody></table>
  