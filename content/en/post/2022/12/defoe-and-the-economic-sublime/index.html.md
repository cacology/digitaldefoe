---
layout: post
title: "Defoe and the Economic Sublime"
date: 2022-12-29
tags: ["Distinguished Scholar's Comment.2022","Issue 14.1-Fall 2022"]
---

FEW CRITICS have had difficulty connecting Defoe as a writer of realist fiction and his interest in the economic problems of his time, but any association between his writings on Britain's trade or labor or stock-jobbing and the sublime might at first thought seem to be a contradiction in terms. In his attack upon what he considered to be the excesses of the French Revolution, Edmund Burke, the great commentator on the sublime in the eighteenth century, followed his sublime image of the besieged queen of France, whom he had once seen, "decorating and cheering the elevated sphere she just began to move in-glittering like the morning-star, full of life and splendour and joy," with a disparaging picture of a new age from which chivalry, beauty, and the sublime had fled to be replaced by the lifeless products of "sophisters, economists and calculators" (66). According to Burke, these were the kind of thinkers who, in the name of social planning, had divided France into exact geometric squares without any consideration of tradition or the moral nature of human beings. He opined that the "cold hearts" of such thinkers would never be a satisfactory model for Britons (68). Burke's contempt for "economists and calculators"-for their lack of sensibility and emotion-had some basis in reality. John Grant and Sir William Petty, who introduced political arithmetic to England in the seventeenth century showed a singular lack of feeling for human nature. Grant pondered on whether it would not be more profitable to introduce polygamy into England to increase the nation's wealth, using animal reproduction in a manner similar to Swift's future projector in _A Modest Proposal_. And Sir William Petty defied any notion of chivalry when, in responding to a challenge to a duel, suggested as his choice of weapons, axes in a dark cellar. There was evidence enough then for the image of the cold-hearted and very unsublime economist.

If as I intend to argue in this essay Defoe found sublimity in matters of trade, he was at least somewhat unusual. Yet it is also clear that economics is not unsusceptible to images of the sublime.[<sup>1</sup>](#_edn1) Slavoj Zizek argues that the very concept of money contains within it what he calls "_sublime_ material" when conceived of, in Marx's terms, as that "'indestructible and immutable' body which persists beyond the corruption of the body physical" (18-19). And Derrida has shown that the sublime ghost of Hamlet's father wanders through much of Marx's writing. Of course, there are few more sublime images than Adam Smith's "Invisible Hand" which somehow guides individual self-interest to the goal of the national good. It is true that Smith deliberately muted this image in _The Wealth of Nations_, but he was more expansive in his _Theory of Moral Sentiments_, where he also used the image of the "insatiable desires" of the wealthy, who are led by an "invisible hand" to employ the poor to satisfy their egotism, thereby spreading the wealth of the world. Smith followed this with an expansive image of "the great system of government, and the wheels of the political machine which "seem to move with more harmony and ease" because of individual self-interest (184-185). In his economic treatise, despite all the attention paid to it, the image appears with relatively little fanfare.

Daniel Defoe was numbered by his contemporaries among the important economic writers employing political arithmetic, and not surprisingly, it was he who most obviously provided a sublime image of economic activity. Like Adam Smith, but far more dramatically, he saw an "Invisible Hand," guiding human economic activity. It is perhaps no accident that Smith was reported to have "quoted some passages in Defoe which breathed, as he thought, the true spirit of English verse" (Amicus 230). Admirers of Defoe's poetry were rare enough in the late eighteenth century, but it is interesting to know that Smith was among them. For it was in what might be called the poetry of economics that Defoe must have stood out from his contemporaries, in his half mocking, half sublime hymn to money in the _Review_, his praise of industry in _Caledonia_ (1706), and his glorification of the teleological force behind economic activity in his _General History of Trade_ (1713). His hymn to money eulogized that very notion of money as a magical and unchangeable thing that Zizek considered a sublime object:

<span style="font-family: Adobe Caslon Pro, serif;">Mighty _Neuter_! Thou great _Jack-a-both sides_ of the World, how hast Thou brought all Things into Bondage to thy Tyranny? How art Thou the might WORD of this War, the great Wheel in the vast Machine of Politick Motion, the Vehicle of Providence, the great _Medium_ of Conveyance, in which all the Physic of the secret Dispensation in human Affairs is administered, and by the Quality of which it operates to Blessing or Cursing? Well art thou call'd _the God of this World; _for in thy Presence and Absence consists all the heaven or hell of human Affairs; for Thee, what will not Mankind do, what Hazzards will they run, what Villanies perform? For thee, Kings Tyrannize, Subjects are oppress'd, Nations ruin'd, Father murther'd, Children abandon'd, Friends betray'd. Thou art the Charm that unlocks the Cabinet, unscrews Nature. (4:422-423)</span>

When Crusoe was to carry off the gold he discovered on the wreck, despite his knowledge that it was useless on the island, he did not provide an explanation for his actions beyond some additional consideration. The reader is allowed to provide some explanations. Two come to mind along with some combination of both. Perhaps he was being practical in thinking that he might be able to use this seemingly useless commodity if he were rescued some day. Or perhaps, for all his understanding that an object such as a cooking pan would be more useful on the island, he could not rid himself of the symbolic and sublime value which inhered in it.

Although Defoe would hardly have conceived of writing a treatise on the sublime, he gave us enough examples of it throughout his writings to allow us to draw a number of general conclusions about how he used it and for what purposes.[<sup>2</sup>](#_edn2) The most significant passage of course is the discovery of the footprint. One critic has pointed to this event as "an event of immense force," a violent encounter with otherness that brings with it a sense of terror and dislocation (Brown 158). I raise this moment in Defoe's writing because although he is often associated with the start of realist fiction, as well as fiction with economic concerns, he was also always capable of charging scenes with powerful emotions.[<sup>3</sup>](#_edn3)

I want to begin with a wonderful section of the _Tour thro' the Whole Island of Great Britain. _The narrator along with his companions have struggled through a mountainous region beset by an amazing snowstorm and terrible claps of thunder. At first they suspect it is a frightening explosion in a coal mine, but then confirm the possibility that thunder may accompany the snow. Although the narrow pass seems "horrible" to them, they keep on descending. But amid this "frightful country" the narrator finds a combination of running water and coals on the highest hills." He ascribes this to

<span style="font-family: Adobe Caslon Pro, serif;">the wise Hand of Providence for the very purpose which is now served by it, namely, the Manufactures, which otherwise could not be carried on .... After we had mounted the third Hill, we found the Country, in short, one continued Village, tho' mountainous every way, as before; hardly a House standing out of a speaking distance from another, and (which soon told us their Business) the Day clearing up, and the sun shining, we could see that almost at every House there was a _Tenter_, and almost on every Tenter a Piece of _Cloth_, or _Kersie_, or _Shalloon_, for they are the three Articles dof that country's Labour; from which the Sun glancing, and, as I may say, shining (the White reflecting its Rays) to us, I thought it was the most agreeable Sight that I ever saw, for the Hills, as I say rising and falling so thick, and the Vallies opening sometimes one way, sometimes another, so that sometimes we could see two or three Miles this way, sometimes as far another; sometimes like the Streets near _St.Giles_'s, call the _Seven Dials_; we could see through the Glaces almost every Way round us, yet look which Way we would, high to the Tops, and low to the Bottoms, it was all the same; innumerable Houses and Tenters, and a white Piece upon every Tenter. (2:601)</span>

The entire scene has to be regarded as a whole. The narrator moves from the horror of Black Edge, which seems to call up the supernatural type of explosive sound in the midst of terrifying mountains and a terrible snowstorm, to the scene of human activity in the foothills-equally staggering in its infinite number of tenters reflecting the sun in an amazing way. It is only here that the narrator calls upon the invisible hand of God ("the wise Hand of Providence") as the ultimate creator of a nature made exactly right for the industriousness of the inhabitants. He is very much the God of industry, seemingly absent in the hills, but present everywhere in the nature that put together coal and water in such a way as to encourage industry. Where there is no industry, as in the area the narrator encounters on leaving Leeds, around Black Barnsley, the land is "dismal...and frightful," but hardly sublime as he longs once more for a vision of the "Tenters with the Cloths shining upon them" (2:617).

As suggested previously, sublime scenes had almost become Defoe's specialty by the time he came to write the _Tour. _His depiction of the Andes, with its terrifying gorges and frightening volcanoes, made up some of the best pages of _A New Voyage Round the World_ (1725), a work Defoe published in the same year as the first volume of the _Tour_. But while he could depict natural scenery well enough, even scenes of this kind had an aura about them. He was reading Burnet's _Theory of the Earth _in these years, and nothing so reminded him of the geological fall from a perfectly round earth as such terrifying spectacles.

The key to all of this is a teleological view of economics. In his _General History of Trade_, Defoe wrote that God created the world in such a way that the

<span style="font-family: Adobe Caslon Pro, serif;">Originals of Manufacture, the Essentials of Life, or of the Conveniences of Life, such as Physical Plants, Drugs, Spices, Metals, _etc._ were by the Wisdom of the first Disposer, dispers'd thro' his whole Creation, so as to make every part of the World useful, nay, I may say, necessary to some, other part of it; which Diversity is the occasion of the Communications of Necessaries or Conveniences, one to another; from whence is raised this useful thing call'd Trade. (2:4-5.)</span>

What follows this statement is a view of the world which would have warmed the heart of Voltaire's Dr. Pangloss. The world was created the way it was, with various goods placed in countries distant from one another, so that human beings would trade with one another. The ocean was made to bear ships; God made wood and tar to build ships. But the reason all of this was done was "the Employment of the People." (2:26.)

In laboring over the raw materials of the earth, human beings may produce wealth for the nation, but in another sense, they come closest to imitating the transforming power of God himself. Defining what he calls "Manufactures," Defoe argues, "Those Materials which passing thro' the Operation of Men's Hands, lose the Face or Apearance of their first Quality, assuming New Names and Figures, as the several uses they are design'd for require, these are properly called Manufactures." But it is in trade itself that man fulfills the will of God in distributing goods around the world. It is by this that man performs according to God's will:

<span style="font-family: Adobe Caslon Pro, serif;">We Trade with _Turks_, Infidels, Idolaters, Gentiles, Heathens, Savages, it matters not what Gods they serve, so they serve our end, and we can serve our Interest by Trading with them; what if they Worship the Sun or the Moon, this Idol or that; Vistly-pustly, Teckoacomon, Mahomet, or Lucifer? Getting Money is the only Idol that Trade Worships, and it is nothing to the Merchant who he Trades with, if he can make a good Return. (3:46)</span>

How can it be that so ungodly an action as trading with heathens can somehow be in the interest of the God who "design'd the World for Commerce" (1:10)? The answer is approximately the same as that one finds in Adam Smith. God operates through human self-interest to create a world which is generally happy. And this is so because of employment itself. One of the reasons that the mountains over which Defoe's narrator has to cross in the _Tour_ are so terrifying is that they are without human habitation and industry. The same is true of the mountains in _A New Voyage Round the World_. Defoe's God is very much the invisible God that Goldmann described in his _Le Dieu caché_. He is most evident for Defoe (and comfortingly so) among scenes of human economic activity. His seeming absence among the mountains of the Andes and Britain is unpleasantly frightening.

In a Lockean passage, in _A General History of Trade, _Defoe argues that "Man's utmost extended Capacity does not allow him to Conceive of any thing which is not, but by something that is; He can form no Ideas of what he has not seen, but upon the foundation, and by the Form of something which he has seen, or which has been described to him" (1:6). Although Defoe then proceeds to deride anthropomorphic images of an afterlife and the form of angels as being beyond our knowledge, such an argument rather proves the existence of a God behind nature than destroys it:

<span style="font-family: Adobe Caslon Pro, serif;">all the Power of Man's Invention and Understanding, could not have conceiv'd any thing of what we call Manufacturing, had not the Materials been furnished by the Author of Nature, which, as it were, led our Forefathers by the Hand to the Improvements of those Materials. Spinning, Weaving, and Knitting had never been thought of, had not the Wool been first furnished by Nature; which by its very Figure and Substance, dictated to the Invention of Men and the Arts of Manufacturing. Nor could all the Wit of Man have form'd an Idea in his Mind of Wooll, Flax, Silk, or Hair, if there had not been such things form'd in the Creation. (1:7.)</span>

Defoe's God of economic activity does not give humanity manufactured products. He tests humankind to grasp the economic ends of things. In describing this process, Defoe calls upon his powers as a writer of prose to produce images of wonder, vastness, and the miraculous:

<span style="font-family: Adobe Caslon Pro, serif;">The World is now laid open; the Distant and Difficult Parts, are made familiar; the Dangers some have miscarried upon, are thereby avoided; the Dangers others have avoided, are known, and made easy; the Bays, Creeks, Ports, Harbours, Shores, and Seas, formerly Terrible, Unknown, and Unfrequented, are made familiar and easy; compassing the Globe, passing the _Streights_ of _Magellan_, measuring the vast _Southern_ Ocean, Coasting the _Indies_, and the Shores of _China_ and _Japan_, and now no such extraordinary things, every Nation almost have done it as well as we, and we do that now every Year, which was accounted next to miraculous in the first Ages of Trade (1:42).</span>

God's most wonderful trick is in what Defoe calls, after his contemporary, John Cary, the "circulation" of goods. For Defoe, the circulation of goods throughout Britain is a miraculous way of enriching everyone. The more people who handle a product, the more the nation is enriched. The entire _Tour_ is a hymn to the way in which goods circulate to that "great and monstrous thing" called London, and in the _Atlas Maritimus_, he used a combination of enumeration along with the language of the ineffable to attempt to describe what was too great to truly represent:

<span style="font-family: Adobe Caslon Pro, serif;">This Trade is so great, that no single Inland Trade in Europe can compare with it. We find it carry'd on by the help of innumberable Pack-Horses, Draught-Horses, Waggons and Carts: and as it employs a great many Ships, Barges, Lighters, etc. For the conveying of such Goods by Sea and in Rivers, as above; so it employs an infinite Number of Carriers, Waggoners, Pedlars, and travelling Chapmen on Shore, as well on Foot as on Horse backs; and maintains a proportion'd number of Publick Houses as Victuallers, Inns, and Ale-houses on the roads, the Number of which , if I should enter into the Particulars of them, would seem incredible. (108)[<sup>4</sup>](#_edn4)</span>

What I am maintaining is that Defoe was one of the few economists who, while being capable of providing pages of statistics in _An Essay upon Projects_ and _Mercator_, insisted on raising the spectacle of trade to the level of the sublime. In his_ Scots Poem_, he made this into an ecstatic monologue imagining himself flying through earth's geography to revel in the possibilities inherent in trade throughout the world:

<span style="font-family: Adobe Caslon Pro, serif;">I'd gladly breath my Air, on Foreign Shores:</span>

<span style="font-family: Adobe Caslon Pro, serif;">Trade with rude _Indian _and the Sun-burnt _Mores._</span>

<span style="font-family: Adobe Caslon Pro, serif;">I'd speak _Chinese_, I'd prattle _African._</span>

<span style="font-family: Adobe Caslon Pro, serif;">And briskly cross, the first _Meridian._</span>

<span style="font-family: Adobe Caslon Pro, serif;">_ _I'd pass the Line, and turn the _Cap_ about.</span>

<span style="font-family: Adobe Caslon Pro, serif;">I'd rove, and sail th'Earth's greatest _Circle_ out.</span>

<span style="font-family: Adobe Caslon Pro, serif;">I'd fearless, venture to the _Darien_ Coast;</span>

<span style="font-family: Adobe Caslon Pro, serif;">Strive to retrieve, the former Bliss we lost.</span>

<span style="font-family: Adobe Caslon Pro, serif;">Yea, I would view _Terra Incognita_.</span>

<span style="font-family: Adobe Caslon Pro, serif;">And climb the Mountains of _America_.</span>

<span style="font-family: Adobe Caslon Pro, serif;">I'd veer my Course, next, for our _Antipod's_,</span>

<span style="font-family: Adobe Caslon Pro, serif;">I'd hunt for _Monkey_, in the _Indian _Woods.</span>

................................

<span style="font-family: Adobe Caslon Pro, serif;">Towards _New Holland_, I'd my rout advance</span>

<span style="font-family: Adobe Caslon Pro, serif;">And know who harrows this vast _Continent_</span>

<span style="font-family: Adobe Caslon Pro, serif;">`Where scarce e're _European_ has even sent</span>

<span style="font-family: Adobe Caslon Pro, serif;">The greatest Dangers, n'ere shou'd make me faint. (lines 144-166)</span>

Although Defoe occasionally speaks of "we" in speaking of the economic adventures that might be achieved by the Scottish nation if it dedicated itself fully to trade, there is little doubt that this passage is dominated by the image of Defoe's visionary adventures. He is unafraid of experiencing the "greatest Dangers." If Defoe was astonished by the scene of the products of labor flashing in the sun-by evidence of labor in the real world, he was also capable of conjuring up an imagined world of economic exploitation with himself as the center of these adventures-the hero of his own economic romance.

When he wrote in the _Review_ that "Writing upon trade was the "Whore...[he] really doated upon," he was not merely using a metaphor to express his preference for writing on economic subjects (1 [IX], 214).[<sup>5</sup>](#_edn5) He meant that despite the banality of ordinary economic life, trade (which he depicted at times as a form of crime, and indeed a form of prostitution) inspired him as a writer and raised his passions as nothing else. If later economists such as Hume and Smith were to insist on a dispassionate style and Ricardo an entirely unreadable one, Defoe was to remain the one economist who raised the entire spectacle of manufacturing and the circulation of goods to the level of sublimity.[<sup>6</sup>](#_edn6)

_University of California, Los Angeles_

**Notes**

[<sup>1</sup>](#_ednref1) I used the term "economic sublime" in my biography (632) and writers on Defoe have used it occasionally. See for example, Edward 185.

[<sup>2</sup>](#_ednref2) For a discussion of the religious sublime in Defoe's _History and Reality of Apparitions_, see van Leeuwen.

[<sup>3</sup>](#_ednref3) Alexander Welsh used Crusoe's thorough methods for establishing the nature of the evidence for determining who might have made the print as an example of realist technique in fiction (2-6).

[<sup>4</sup>](#_ednref4) Defoe wrote only the section on economic geography for this work.

[<sup>5</sup>](#_ednref5) Defoe's use of the notion of every man having his "Whore" is somewhat similar to Laurence Sterne's later use of the Hobby-Horse, a kind of master passion or obsession.

[<sup>6</sup>](#_ednref6) In his _Brief State of the Inland or Home Trade of England_, Defoe wrote of the "Beautiful Order of Trade" (26). But his appeal was usually to that element of the sublime that belonged to the vast.

**Works Cited**

Amicus. "Anecdotes tending to throw light on the character and opinions of the late Adam Smith, LLD." _The Bee, or Literary Weekly Intelligencer_, May 11, 1791. Rpt. in Adam Smith, _Lectures on Rhetoric and Belles Lettres_, edited by J. C. Bryce, Liberty Fund, 1983, pp. 226-31.

Brown, Tony C. _The Primitive, the Aesthetic, and the Savage: An Enlightenment Problematic_. Minnesota UP, 2012.

Burke, Edmund. _Reflections on the Revolution in France_, edited by J. G. A. Pocock, Hackett Publishing, 1987.

Daniel Defoe. _Defoe's R__eview_, edited by Arthur W. Secord, vol. 4, Columbia UP, 1938.

Defoe, Daniel. _Atlas Maritimus et Commercialis_. London, 1728.

_---._ _Brief State of the Inland or Home Trade of England_. London, 1730.

_---._ _An essay on the history and reality of apparitions. Being an account of what they are, and what they are not; whence they come, and whence they come not._ London, 1727.

_---._ _A general history of trade: and especially consider'd as it respects the British commerce, as well at home, as to all parts of the world_. London, 1713.

_---. A Scots Poem, OR a New Year's Gift, From a Native of the Universe to his Fellow-Animals in Albania_. Edinburgh, 1707. Reprinted in _Poems on Affairs of State_, edited by Frank Ellis, et al., vol. 7. Yale UP, 1954-1975.

_---._ _A Tour thro' the Whole Island of Great Britain_. Vol. 2, Peter Davies, 1927.

Edward, Jesse. "Defoe the Geographer Redefining the Wonderful in _A Tour Thro' The Whole Island Of Great Britain_." _Travel Narratives, the New Science, and Literary Discourse 1569-1750_, edited by Judy Hayden, Ashgate, 2012.

Novak, Maximillian E. _Daniel Defoe: Master of Fictions_. Oxford UP, 2001.

Smith, Adam._ The Theory of Moral Sentiments_, edited by D.D. Raphael and A.L. Macfie, Liberty Fund, 1982.

van Leeuwen, Evert Jan. "The Religious Sublime in _An Essay on the History and Reality of Apparitions_ (1727)." _Positioning Defoe's Fiction_, edited by Aino Mäkakalli and Andreas Mueller, Cambridge Scholars Publishing, 2011, pp. 169-185.

Welsh, Alexander._ Strong Representations: Narrative and Circumstantial Evidence in England_. Johns Hopkins UP, 1992.

Zizek, Slavoj. _The Sublime Object of Ideology_. Verso, 1994.