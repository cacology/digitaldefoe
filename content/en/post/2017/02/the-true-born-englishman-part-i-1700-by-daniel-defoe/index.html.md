---
layout: post
title: "The True Born Englishman, Part I (1700), by Daniel Defoe"
date: 2017-02-15
---

"The True Born Englishman, Part I (1700), by Daniel Defoe" from John Richetti Reading Restoration and 18th Century Verse, Studio 111, October 15, 2009 by John Richetti. Released: 2009. Track 7. Genre: PennSound.