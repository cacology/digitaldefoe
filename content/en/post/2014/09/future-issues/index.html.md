---
layout: post
title: "Future Issues"
date: 2014-09-25
---

<span style="font-weight: 400;">"A Little Recover'd": Digital Defoe CFP for 2021 Volume</span>

<span style="font-weight: 400;">"But I am alive."</span>

<span style="font-weight: 400;">-</span>_<span style="font-weight: 400;">Robinson Crusoe</span>_

_<span style="font-weight: 400;">Digital Defoe</span>_<span style="font-weight: 400;"> is seeking submissions for its next issue (Issue 13.1, Fall/Winter 2021). Articles that explore any area relating to Defoe and/or his contemporaries (broadly conceived) are welcome. In addition to traditional scholarly papers (roughly 4000-7000 words), we welcome essays on fresh pedagogical approaches to the works of Defoe and his contemporaries. These essays may highlight experiences teaching during a time of emergency remote instruction, tentatively returning to campus, or finding resonances between our "unprecedented" times and their eighteenth-century precedents.&nbsp; We also continue to encourage the submission of innovative digital and multimedia projects.</span>

<span style="font-weight: 400;">For this issue, </span>_<span style="font-weight: 400;">Digital Defoe</span>_<span style="font-weight: 400;"> also seeks informal reflections on the theme of pandemic life and recovery. Topics could include returning to society after a period of isolation; survival and its aftermaths; normalcy and the "new normal"; or remembering those we've lost. Reflections may vary in length, but we especially welcome short pieces between 500-1000 words.</span>

<span style="font-weight: 400;">Our aim is to provide a space for creativity and contemplation. If you have an idea for a submission but are unsure about the fit, please send us a short query, and we will respond quickly. We welcome work from graduate students and early career researchers.&nbsp;</span>

<span style="font-weight: 400;">As always, scholarly articles will undergo anonymous peer review; other submissions will be reviewed by the editorial staff. Scholarly essays may be eligible for essay prizes awarded by </span>[<span style="font-weight: 400;">the Defoe Society</span>](https://www.defoesociety.org/awards/)<span style="font-weight: 400;">.&nbsp;</span>

<span style="font-weight: 400;">Please direct queries and submissions to Chris Loar (christopher.loar@wwu.edu), Denys Van Renen (vanrenendw@unk.edu), and Stephanie Hershinow (stephanie.hershinow@baruch.cuny.edu).</span>

<span style="font-weight: 400;">Deadline for submissions is 15 August 2021.</span>