---
layout: post
title: "Dating <em>Warning or Lanthorn to London</em>"
date: 2014-10-28
tags: ["Issues","Issue 6.1 - Fall 2014","Notes"]
---

IT WOULD be helpful, when possible, if digital archives such as Early English Books Online (EEBO), the Bodleian Libraries' Broadside Ballads Online (BBO), and the University of California, Santa Barbara English Broadside Ballad Archive (EBBA) included the estimated date of first printing of ballads in their collections to avoid misreadings of the circumstances of a ballad's production and transmission. Such information would also inform accounts of the adaptation and redeployment of a ballad in particular historical periods. The dating of the ballad _Warning or Lanthorn to London, by the doleful destruction of faire Jerusalem_ in EEBO and BBO is a case in point.

The English Short Title Catalogue (ESTC) dates the ballad _Warning or Lanthorn to London_ "[1655-1658]." The copy mentioned was printed in London for "F. Coles, J. Wright, Tho. Verse and W. Gilbertson." The citation/ reference is given as "Wing (2nd ed.), W925A." In EEBO, the 16-stanza version printed with another ballad, _Of the horrible and woful destruction of Jerusalem_, is also said to have been "printed for F. Coles, J. Wright, Tho. Vere, and W. Gilbertson." The Wing (2nd ed.) catalogue date of "[1658?]" is given along with the date of "1690." The bib name/numbers are listed as "Wing (2nd ed.) / 0144" and "Wing (2nd ed.) / W925A." In BBO, the date of publication of the [16-stanza version of the ballad,](http://ballads.bodleian.ox.ac.uk/search/title/Warning or lanthorn to London by the doleful destruction of faire Jerusalem) again printed alongside the second ballad, is given as "between 1655 and 1658" and the imprint indicates that it was "Printed for F. Coles, J. Wright, Tho. Vere, and W. Gilbertson." The copies listed are "Wood 401(81)" and "Harding B 39(51)."

Evidence suggests that the version of _Warning or Lanthorn to London_ referred to in the ESTC and reproduced in EEBO and BBO is a later edition of an Elizabethan siege ballad. An edition of _Of the horrible and woful destruction of Jerusalem_ appeared in the sixteenth century. EBBA includes a photofacsimile of the Huntington Library`s copy of the ballad _[Of the horyble and woful destruccion of Jerusalem](http://ebba.english.ucsb.edu/ballad/32085/image)_, on which the Elizabethan Thomas Colwell is named as the printer (Huntington Library Britwell 18266). The EBBA gives the approximate date of publication of _Of the horrible and woful destruction of Jerusalem_ as "1569?" That _Warning or Lanthorn to London_ was also first published in the Elizabethan period is supported by the research of Andrew Clark. An 18-stanza version of _Warning or Lanthorn to London_ is included in the first volume of Andrew Clark's _[The Shirburn Ballads, 1585-1616](https://archive.org/stream/shirburnballads100claruoft#page/30/mode/2up)_. Clark provides some prefatory information on the ballad and its sources: "A Black-letter copy in Wood, 401 fol. 81, omits stanzas 5 and 11. The ultimate source of the ballad is, of course, the much-read Josephus. John Stockwood, 'Schoolemaister' of Tunbridge, published at London, 1584, 'A very fruitfull and necessarye sermon of the moste lamentable destruction of Ierusalem,' which contains, and may have suggested, most of the points in the ballad" (31). More recently, Michael Hattaway dates a ballad with the same first line, "When fair Jerusalem did stand," 1586 (42), relying on the details from Appendix A of Tessa Watt's _Cheap Print and Popular Piety, 1550−1640_. Watts writes of the dates of entry of the ballad in the Stationer's Register: "?ent. 15 au. 1586; 8 jn. 1603; 14 de. 1624" (334).

Therefore, while the sixteen-stanza version(s) of _Warning or Lanthorn to London_ reproduced in EEBO and BBO, and listed on the ESTC, may be correctly dated, it is likely that the eighteen-stanza version first appeared in or around 1586.

Trinity Western University

Whitman College

**

WORKS CITED**

[Broadside Ballads Online](http://ballads.bodleian.ox.ac.uk/search/title/Warning or lanthorn to London by the doleful destruction of faire Jerusalem). Bodleian Libraries. Web. 29 Aug. 2014.

Clark, Andrew, ed. [_The Shirburn Ballads, 1586-1616_](https://archive.org/details/shirburnballads100claruoft). Oxford: Clarendon, 1907. _The Internet Archive_. Web. 29 Aug. 2014.

[

Early English Books Online](http://eebo.chadwyck.com/home). ProQuest. Web. 29 Aug. 2014.

[English Broadside Ballad Archive](http://ebba.english.ucsb.edu/ballad/32085/image). U of California, Santa Barbara. Web. 29 Aug. 2014.

[The English Short Title Catalogue](http://estc.bl.uk/F/?func=file&file_name=login-bl-estc). The British Library. Web. 29 Aug. 2014.

Hattaway, Michael. _Renaissance and Reformations: An Introduction to Early Modern English Literature_. Malden, MA: Blackwell, 2005. Print.

Watts, Tessa. _Cheap Print and Popular Piety, 1550-1640_. Cambridge: Cambridge UP, 1991. Print.