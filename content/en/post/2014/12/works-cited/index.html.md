---
layout: post
title: "Works Cited - Klein"
date: 2014-12-02
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

Aull, Laura L. "Students Creating Canons: Rethinking What (and Who) constitutes the Canon." _Pedagogy: Critical Approaches to Teaching Literature, Language, Composition, and Culture_ 12.3 (2012): 497-512. Print.

Ball, Cheryl, and Ryan Moeller. "Reinventing the Possibilities: Academic Literacy and New Media." _The Fibreculture Journal_ 10 (2007) Web. 16 Aug. 2014.

Barst, Julie M. "Pedagogical Approaches to Diversity in the English Classroom: A Case Study of Global Feminist Literature." _Pedagogy: Critical Approaches to Teaching Literature, Language, Composition, and Culture_ 13.1 (2013) 149-57. Print.

Koh, Adeline. "Introducing Digital Humanities Work to Undergraduates: An Overview." _Hybrid Pedagogy_ (14 Aug. 2014). Web. 23 Aug. 2014.

Keleman, Erick. "Critical Editing and Close Reading in the Undergraduate Classroom." _Pedagogy: Critical Approaches to Teaching Literature, Language, Composition, and Culture_ 12.1 (2011): 121-38. Print.

Lari, Pooneh. "The Use of Wikis for Collaboration in Higher Education." _The Professor's Guide to Taming Technology. Leveraging Digital Media, Web 2.0_. Ed. Kathleen P. King and Thomas D. Cox. Charlotte, NC: Information Age, 2011. 121-33. Print.

Marsden, Jean I. "Beyond Recovery: Feminism and the Future of Eighteenth-Century Literary Studies." _Feminist Studies_ 28.3 (2002): 657-62. Print.

Moskal, Jeanne. "Introduction: Teaching British Women Writers, 1750-1900." _Teaching British Women Writers, 1750-1900_. Ed. Jeanne Moskal and Shannon R. Wooden. New York: Peter Lang, 2005. Print. 1-10.

Shesgreen, Sean. "Canonizing the Canonizer: A Short History of _The Norton Anthology of English Literature_." _Critical Inquiry_ 35.2 (2009): 293-318. Print.

Takayoshi, Pamela, and Cynthia L. Selfe. "Thinking about Multimodality." _Multimodal Composition: Resources for Teachers_. Ed. Cynthia L. Selfe. New York: Hampton Press, 2007. 1-12. Print.

Weber, Elizabeth Dolly. "Lighting Their Own Path: Student-Created Wikis in the Commedia Classroom." _Pedagogy: Critical Approaches to Teaching Literature, Language, Composition, and Culture_ 13.1 (2012): 125-32. Print.