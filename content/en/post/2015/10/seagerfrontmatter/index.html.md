---
layout: post
title: "Daniel Defoe’s <em>Some Thoughts of an Honest Tory in the Country</em> (1716): A Critical Edition"
date: 2015-10-19
tags: ["Issues","Issue 7.1 - Fall 2015"]
---

# <span style="color: #386117;">[Introduction to the Edition](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  '  [Note on the Text](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition Copy")  '  [Front Matter](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy-2/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  '  [Maintext of _Some Thoughts_](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy-3/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  ' [Bibliography](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-2/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")</span>

&nbsp;

Some
THOUGHTS
of an
Honest _TORY
_In the Country,
upon the
Late Dispositions of some People to Revolt.
with
Something of the Original and Consequence of their present Disaffection to the Person and Government of the King.

* * *

_In a Letter to an Honest _Tory_ in_ London

* * *

_LONDON:
_Printed for R. Burleigh in _Amen-Corner_. 1716.

(Price Six Pence)

&nbsp;

* * *

 THE INTRODUCTION

As in Matters of Dispute it is always requisite to explain the Terms, so before I proceed to the Case now in hand, I think my self oblig'd to lay down very plainly what I mean, and how I wou'd be understood, when I speak, 1. Of _an _Honest Tory, for some will hardly allow the Term to be just. 2. Of the _Dispositions of some People to Revolt_. 3. Of a _Disaffection to the Person and Government of the King_; and, _lastly_, who I mean by _the King_.

And this I do, because as it is just that every Man should be the Expositor of his own Words, so in this time of universal Misunderstanding, it is highly necessary to every Author to be very explicit, that no room should be left to his Enemies to put false Glosses upon what he writes,[[13]](#_edn13) and to explain Things _for him_, which they cannot so well do when he has explain'd them already _for himself_.

By an _Honest Tory_ then, I mean such a Man as the _Whigs_ themselves mark'd out for us in the time of the late Reign;[[14]](#_edn14) when tho' they believed that the Ministry, and High Church or _Tory Party_, were in the Interest of the _Pretender_;[[15]](#_edn15) yet they always told us, and that not with Charity only but with _Truth_, that it was not to be understood of all the Tory Party, but that there were a great many honest Gentlemen, even among the _Tories_, who were not for the _Pretender_, and these they themselves christned afterwards by the Name of _Revolution Tories_.[[16]](#_edn16) I could quote many Authors for this, besides a Pamphlet said to be written by a Person of Honour, Entitled, _Observations on the State of the Nation_,[[17]](#_edn17) where the Case is plainly distinguished. In a Word then, the Author of this Work is to be understood as a _Revolution Tory_; one, who however he may have been of differing Sentiments from the generality of the People in some time past, is yet sound in his Political Opinion, clear in the Revolution Principle, and by Consequence in that of the Protestant Succession of _Hanover_,[[18]](#_edn18) as now possest by, and entail'd upon, The King, and his Royal Protestant Posterity; and this Man it's hoped may, tho' _a Tory_, be call'd _Honest_ for many good Reasons, too many to enter upon here.

By the Words in the Title _the Disposition of some People to revolt_, it is desir'd to be understood, not that there is a general or a formidable Disposition in the People of this Nation to revolt; but that there is such an unhappy Disposition in some, and that there are Agents at Work busily to spread that Disposition further among the rest; and this cannot be denied; nay, tho' it should be said that this unhappy Disposition has been spread farther, and encreased to a greater height than was thought could have been possible in such a Nation, and under such an Administration as this we now live under.

By the _Disaffection_ to the Person and Government _of the King_, I mean much the same with the Disposition to revolt; with the Addition only of those who are actually in Arms against the King, and of those who openly declare themselves in the Interest of the Rebels.[[19]](#_edn19)

Lastly, by _the King_ I desire always to be understood as a good Subject ought to be, _viz._ to mean King George; and I chuse to explain it thus to avoid the often[[20]](#_edn20) Repetition of his Majesty's Name; as also, that I think no other Name can, but in a criminal manner be added to the Style: and I think the King is a more Honourable Distinction by far, than it would be with the addition of his Majesty's Name, which would but leave room to suggest, that an Usurper might be lawfully spoken of _as King_, and have the Honour of his Majesty's Style added to his Name, which I think cannot be without as much Treason as the Tongue is capable of speaking.

**
Notes**

[[13]](#_ednref13)     _false Glosses_: This phrase occurs frequently in Defoe's writings; see _An Enquiry into the Case of Mr. Asgill's Translation_ [A3v]; _Review_ 1.7; _An Essay on the History of Parties _3; _An Essay on the South-Sea Trade_ 9; _Reasons Against Fighting_ 2; _Argument Proving_ 89; _Turkish Spy_ 220; _Complete English Tradesman_ 203, 301; _Conjugal Lewdness_ 112; _A New Family Instructor_ 14.

[](#_ednref14)

[[14]](#_ednref14)   _the late Reign_: that of Queen Anne (reigned 1702-14).

[[15]](#_ednref15)   _Pretender_: James Francis Edward Stuart (1688-1766), son of the deposed King James II (reigned 1685-88), who led a failed uprising to claim the throne in late 1715.

[[16]](#_ednref16)   _Revolution Tories_: those Tories in support of the 1688-89 English Revolution, by which James II was deposed by Parliament and replaced with his son-in-law and daughter, William III and Mary II (reigned 1688-1702; William _solo_ after Mary's death in 1694).

[[17]](#_ednref17)   _Observations ... Nation_:_ Observations upon the State of the Nation, in January 1712/3_ (January 1713), which Defoe thought was by Daniel Finch, 2nd Earl of Nottingham (1647-1730). See Introduction.

[[18]](#_ednref18)   _Protestant Succession of Hanover_: by the Act of Succession (1701), the monarchy was settled, in the case of both William III and Anne dying without heir, on the next Protestant claimant, Princess Sophia, Electress of Hanover, and her heirs. Sophia predeceased Anne, so Sophia's son became George I (reigned 1714-27).

[[19]](#_ednref19)   _Rebels_: participants in the 1715 Jacobite rising.

[[20]](#_ednref20)   _often_: frequent.

# <span style="color: #386117;">[Introduction to the Edition](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  '  [Note on the Text](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition Copy")  '  [Front Matter](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy-2/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  '  [Maintext of _Some Thoughts_](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-copy-3/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")  ' [Bibliography](http://digitaldefoe.org/2015/10/19/daniel-defoes-some-thoughts-of-an-honest-tory-in-the-country-1716-a-critical-edition-2/ "Daniel Defoe's Some Thoughts of an Honest Tory in the Country (1716): A Critical Edition")</span>