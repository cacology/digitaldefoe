---
layout: post
title: "Privacy and Modernity I: The Family - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<span class="style1 style12">**Readings:   

          ➢	Philippe Ariès, Introduction to _The History of Private Life III: The Passions of the Renaissance_   

          ➢	Michael McKeon, "Chapter 5: Subdividing Inside Spaces" in _The Secret History of Domesticity:   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Public, Private, and the Division of Knowledge_**</span>

<span class="style1 style12">Because a major goal of the course is to enrich and complicate notions of both private and public, students are invited to provide synonyms any time they find themselves using either of these words in discussion or writing. In this way, we can begin to uncover and, where necessary, let go of our assumptions about both categories and the relationships between them. Excerpts from two major histories of privacy ground the rethinking we have already begun: both Philippe Ariès and Michael McKeon narrate privacy's emergence in relation to the development of the modern family.   

Ariès contrasts the communality of medieval Europe--"private was confounded with public" (1)--to the compartmentalized forms of nineteenth-century social life - when private and public separated as the family home became a refuge from a basic state of anonymity everywhere else. According to Ariès, increasingly bureaucratic governments, the flourishing of print and literacy, and internalized religious practices like confession and closet prayer were major cultural factors in the shift from communality to compartmentalization. Early modern privacy consisted not only in more intimate family interactions than ever before in more intimate rooms than ever before, but also in changing discourses and practices of selfhood, including new concerns with bodily modesty, reflexive reading and writing, and friendship, which was increasingly characterized as shared solitude.   

</span>

          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
              <td height="328" valign="top" width="27%" align="right">

[![](8a.jpg)  

              ](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/8mckeon.jpg)

<span class="style45 style45 style45 style45 style45 style1">Fig. 8a. Longleat House, 1570. From Michael McKeon, _The Secret History of Domesticity_, 253. Marquand Library, Princeton University Library.</span>

![](8b.jpg)

<span class="style1">Fig. 8b. Longleat House, c1809. From Michael McKeon, _Secret History of Domesticity_, 254. Marquand Library, Princeton Univesrity Library. By permission of Oxford University Press</span>.
</td>
              <td valign="top" width="73%">

<span class="style1 style12">In his _Secret History of Domesticity_, McKeon situates the increasing coherence and complexity of the private in the seventeenth and eighteenth centuries within a series of interrelated categorical and disciplinary divisions, including the separation of science from the arts and humanities and, most significantly, the separation of workplace from household. Our initial encounter with McKeon's book focuses on his exploration of the architectural corollaries to this process. In the chapter on "Subdividing Inside Spaces," McKeon is interested in how  changing domestic designs mirrored and precipitated the conceptual evolution of privacy in the period. Privacy had traditionally been defined-and designed-as a withdrawal from the fundamental publicness of the household. Later, the generous use of corridors made individual rooms discrete and less permeable (see [Figures 8a](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/8a.jpg) and [8b](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/8b.jpg)), thereby reinforcing the new feeling that privacy was a positive and distinct value. Separate rooms variously accommodated women's desire for distance from men (and vice versa), family members' desire for distance from servants, and the desire of any and all members of the household for distance from outside visitors. McKeon's chapter also provides our third catalogue of the varieties of the closet and cabinet in the period, including the cabinet of curiosities and closet as study, library, boudoir, harbour of secrets, and site of secretarial business.     

 </span>

              </td>
            </tr>
          </tbody></table>
  