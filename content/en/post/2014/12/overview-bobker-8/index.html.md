---
layout: post
title: "Bibliography - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

Addison, Joseph and Richard Steele. _The Spectator_. Ed. Donald Bond. 2 vols. Oxford: Clarendon, 1965. Print.

Ariès, Philippe. Introduction. _The History of Private Life III: The Passions of the
Renaissance_. Ed. Roger Chartier. Cambridge, MA: Harvard UP, 1989. Print.

Armstrong, Nancy. _Desire and Domestic Fiction_. New York: Oxford UP, 1990. Print.

Ballaster, Ros. _Seductive Forms: Women's Amatory Fiction from 1684 to 1740_. Oxford: Oxford UP, 1992. Print.

Brooks, Thomas. _Cabinet of Choice Jewels Or, A Box of precious Ointment_. EEBO. London: 1669. Web. 30 Aug. 2007.
_
Cabinet of Momus_. London: 1786. ECCO. Web. 30 Aug. 2007.

Chartier, Roger. "Libraries without Walls." _Future Libraries_. Spec. Issue of _Representations_ 42 (Spring 1993): 38-52. Print.

Chico, Tita. _Designing Women: The Dressing Room in Eighteenth-Century Literature and Culture_. Lewisburg: Bucknell UP, 2005. Print.

Cleland, John. _Memoirs of a Woman of Pleasure_. Oxford: Oxford UP, 2008. Print. Oxford World's Classics. Print.

Douglas, Mary. _Purity and Danger: An Analysis of Concepts of Pollution and Taboo_. Boston: Routledge, 1979. Print.

Edson, Michael. "'A Closet or a Secret Field': Horace, Protestant Devotion and British Retirement Poetry." _Journal for Eighteenth-Century Studies_ 35.1 (2012): 17-41. Web. 15 Dec. 2012.

Habermas, Jürgen. _The Structural Transformation of the Public Sphere: An Inquiry into a Category of Bourgeois Society_. Trans. Thomas Burger. Cambridge, MA: Massachusetts Institute of Technology P, 1995. Print.

Hamilton, Anthony. _Memoirs of Count Grammont_. Whitefish MT: Kessinger, 2010. Print.

Haywood, Eliza. _Love in Excess_. Peterborough ON: Broadview, 2000. Print.

Jonson, Ben. "To Penshurst." _Norton Anthology of British Literature: Vol 1B_. Ed. George M. Logan, Stephen Greenblatt, and Barbara Lewalski. New York: Norton, 2000. 1399-1401. Print.
_
The Ladies Cabinet broke Open_. London: 1710. ECCO. Web. 30 Aug. 2007.

Laqueur, Thomas. _Solitary Sex: The Cultural History of Masturbation_. New York: Zone Books, 2003. Print.

Locke, John. _Essay Concerning Human Understanding_. Ed. Peter Nidditch. Oxford: Clarendon, 1979. Print.
--. _Two Treatises of Government_. Ed. Peter Laslett. Cambridge: Cambridge UP, 2000. Print.

Mauriès, Patrick. _Cabinets of Curiosity_. New York: Thames and Hudson, 2002. Print.

McKeon, Michael. _The Secret History of Domesticity: Public, Private, and the Division of Knowledge_. Baltimore MD: The Johns Hopkins UP, 2006. Print.
_
Modern Curiosities of Art & Nature. Extracted out of the Cabinets of the most Eminent
Personages of the French Court_. London: 1685. EEBO. Web. 30 Aug. 2007.

Montagu, Mary Wortley. "The Reasons that Induced Dr S[wift] to Write a Poem Call'd the Lady's Dressing Room." _Essays and Poems and Simplicity, A Comedy_. Ed. Robert Halsband and Isobel Grundy. Oxford: Clarendon, 1977. Print.

Pepys, Samuel. _Diary_. Project Gutenberg Literary Editions. Web. 30 Aug. 2007.

Pope, Alexander. "Rape of the Lock" and "The Key to the Lock." New York: Bedford, 2007. Print.

Rambuss, Richard. _Closet Devotions_. Durham NC: Duke UP, 1998. Print.

Richardson, Samuel. _Pamela; or, Virtue Rewarded_. Oxford: Oxford UP, 2008. Print.

Robinson, David M. _Closeted Writing and Gay and Lesbian Literature: Classical, Early
Modern, Eighteenth-Century_. Burlington VT: Ashgate, 2006. Print.

Rolleston, Samuel. _Philosophical Dialogue Concerning Decency_. London: 1751. ECCO. Web. 1 Feb. 2006.

Sabor, Peter. "From Sexual Liberation to _Gender Trouble_: Reading the _Memoirs of a Woman of Pleasure_ from the 1960s to the 1990s." _Eighteenth-Century Studies_ 33.4 (2000): 561-78. Print.

Sedgwick, Eve Kosowsky. Introduction: Axiomatic. _Epistemology of the Closet_. Berkeley: U of California P, 1990. Print.

Sontag, Susan. "Notes on 'Camp.'" _Against Interpretation and Other Essays_. New York:
Picador, 2001. 275-92. Print.

Swift, Jonathan. _Complete Poems_. Ed. Pat Rogers. New Haven: Yale UP, 1983. Print.

Wall, Cynthia. "Writing Things." _The Prose of Things: Transformations of Description in the
Eighteenth Century_. Chicago: U of Chicago P, 2006. Print.

Walpole, Horace. _Castle of Otranto_. Oxford: Oxford UP, 2009. Print. Oxford World's Classics.
--. _The Mysterious Mother. Castle of Otranto_ and _The Mysterious Mother_. Peterborough ON: Broadview, 2003. Print.

Warner, Michael. "Public and Private." _Public and Counterpublics_. New York: Zone Books, 2002. Print.

Wettenhall, Edward. _Enter into thy Closet_. London: 1684. EEBO. Web. 30 Aug. 2007.