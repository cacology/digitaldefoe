---
layout: post
title: "Course Materials - Klein"
date: 2014-12-02
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

**Textbook and Materials**
I chose to use Roger Lonsdale's anthology _Eighteenth-Century Women Poets: An Oxford Anthology_ because it was compact, the poems were ordered by author, and the authors were presented chronologically. Due to the [structure of the course](http://digitaldefoe.org/?p=256) and students' lack of [prior knowledge](http://digitaldefoe.org/?p=254), I felt that working chronologically and by author would best serve our needs. For these reasons I did not use [_British Women Poets of the Eighteenth Century_](http://www.amazon.com/British-Women-Poets-Eighteenth-Century/dp/0801892783), by Paula R. Backscheider and Catherine E. Ingrassia, which is organized by subject and genre of poem. I supplemented the Lonsdale, however, with poems from other anthologies.

In my attempt to keep the syllabus as diverse as possible, I included a day on Phillis Wheatley's poetry, which was not included in the Lonsdale at all. I also included poetry by Scottish poet Joanna Baillie and working-class poets such as Ann Yearsley and Mary Leapor. My reasoning was similar to that of Julie M. Barst, who argues that "we as teachers realize that one of our most important pursuits is to encourage students to understand and consider the positions of peoples within their own communities and around the world who are different from them, not only in terms of gender, race, and sexual orientation but also in terms of religion, class, cultural beliefs and practices, ethnicity, and in many other realms" (149).

I did not want, however, to present these women as writing in isolation from the major literary and political movements of their time period. Thus, I also included some complementary poems by men that demonstrated how male and female poets of the time period interacted with each other in print and how they influenced each other stylistically. For example, we read John Wilmot, 2nd Earl of Rochester's "The Imperfect Enjoyment" alongside Aphra Behn's "The Disappointment," and Robert Burns's "Ae Fond Kiss" alongside Joanna Baillie's "Woo'd and Married and A'" in order to explore how male and female poets used similar forms and styles to explore the same topic from different perspectives.

**Resources for Poems: **

Backsheider, Paula R. and Catherine E. Ingrassia. _British Women Poets of the Eighteenth Century: An Anthology_. Baltimore: Johns Hopkins UP, 2009. Print.

[Eighteenth-Century Collections Online](http://gdc.gale.com/products/eighteenth-century-collections-online/). Gale Publishing Group.

Lonsdale, Roger. Editor. _Eighteenth-Century Women Poets: An Oxford Anthology_. Oxford: Oxford UP, 1990. Print.

_Norton Anthology of English Literature Vol. C: Restoration and Eighteenth-Century and Vol. D: Romanticism_, as well as the _Norton Anthology of Poetry_

**Exploring the Canon: Anthology Activity**

When teaching a course on a topic so seemingly narrow and specialized, I felt it was extremely important to convey issues of [canonization and feminist recovery practices](http://digitaldefoe.org/?p=252) to the students. The students, though unfamiliar with the time period and content, believed that by virtue of its availability, the course proved the "canonicity" of its content. By contrast, when I mentioned to a (male) colleague (who frequently taught courses on Jane Austen) that I would be teaching a summer course on female poets of the eighteenth century, he jokingly asked me, "Oh, were there any?"

In order to illustrate these kinds of problems to my students, I began the course with an activity in which I brought in various kinds of anthologies of literature for the students to look at in pairs. In doing so, I hoped to make students more aware of the practices of canonization. Laura L. Aull notes that "a limitation to contemporary discussions of survey anthologies is that they imply that canon revision of the classroom consists of making anthologies more inclusive--not by having students engage in anthologizing itself" (498). In order to avoid such limitations, I actively encouraged students to partake in the act of "anthologizing" by actively [choosing which poets to read](http://digitaldefoe.org/?p=256) and also by comparing different popular anthologies and their offerings of female poets over time.

Students were instructed to look at the contents of these anthologies and compare them to the syllabus in front of them. Some of the anthologies we looked at were the Norton British literature anthology from the 1960s, one from the 1990s, the _Norton Anthology of Poetry in English_, the Restoration and 18th Century splits of the Norton and the Longman anthologies, as well as the Lonsdale and the Backscheider and Ingrassia specialized anthologies. In this way, the students were able to identify the ways in which [anthologies mold the canon](http://digitaldefoe.org/?p=252). The activity also illustrated how the poets featured in the course were often marginalized, even in anthologies dedicated to the eighteenth century. Consequently, this activity, completed during the second meeting of the class, illustrated how "anthologies function as shapers of canons, from narrating particular frames for texts to adjusting the original context and appearance of texts in fonts and formats" (Aull 499).

**Course Documents**

Below is a link to the course syllabus. At the start of the course, the second half of the schedule of readings was empty. This is what the syllabus looked like once the students chose which poets they wished to study.

Also included in the next section are the assignment prompts for the [biography, the annotated bibliography, and the Wiki](http://digitaldefoe.org/?p=260).

» [Assignment Course Wiki Full ](http://digitaldefoe.org/wp-content/files/teaching/Assignment Course Wiki full.pdf)
» [Assignments - Bio & Bib](http://digitaldefoe.org/wp-content/files/teaching/AssignmentsBioBib.pdf)
» [Syllabus_for_Women_Poetry_Class](http://digitaldefoe.org/wp-content/files/teaching/Syllabus_for_Women_Poetry_Class.pdf)

**Next Section: [Final Student Assignment](http://digitaldefoe.org/?p=260)** . . .