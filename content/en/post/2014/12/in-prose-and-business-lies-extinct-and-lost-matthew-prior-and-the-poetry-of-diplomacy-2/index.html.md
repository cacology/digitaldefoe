---
layout: post
title: "“In Prose and Business lies extinct and lost”: Matthew Prior and the Poetry of Diplomacy"
date: 2014-12-02
tags: ["Issues","Article Notes","Issue 6.1 - Fall 2014"]
---

## NOTES

  <a name="1" id="1"></a>1. Such a reading has long been contested.  In 1972, Otis Fellows, in an article entitled "Prior's 'Pritty Spanish Conceit,'" argued that Don Quixote (or rather Sancho Panza) was a significant influence on "Alma," noting Panza's insistence that the belly governs the heart rather than vice versa.  Fellows uses this hint to undermine the entire system of "Alma":   

> The present writer is still as convinced as he was a few years ago when he wrote that in the debate between Mat and Richard, it is the latter who says that the mind's seat of empire is the belly, and that it is significant that Richard should have the last thirty lines, which are in praise of happiness, or at least contentment. It remains his considered judgement that, in the end, "the pragmatic Richard is closer to Prior's position than Mat, and Prior's poem succeeds only as Mat's system fails, as all systems must fail that employ only the modest agent of human reason for metaphysical speculation. Thus, Mat's system - the reconciling product of intellectual pyrotechnics that are not only magnificent but absurd - collapses before the awesome prospect of Richard's discontented belly. (11)

            [_Return to main text._](http://108.167.133.27/~keellis/?p=118#main1)  

        <a name="2" id="2"></a>2. The sexualization of William of Orange's relationship with his favorites is discountenanced by David Onnekink in his article "Mynheer Benting Now Rules over Us": The 1st Earl of Portland and the Re-Emergence of the English Favourite, 1689−99." Of course, from the point of view of compensatory propaganda, it is the prevalence rather than the accuracy of homophobic identifications which is significant.

        _[Return to main text.](http://108.167.133.27/~keellis/?p=118#main2)_  

        <a name="3" id="3"></a> 3. In "The Pre-Requisites for Decisiveness in Early Modern Warfare," Jamel Ostwald writes, "even Marlborough's most ardent supporters acknowledge it was a Pyrrhic victory. The heavily entrenched French army suffered nine thousand casualties and the Allies twenty-four thousand, losses so high that the well-organized French withdrawal from the field was not even contested" (665).

        _[Return to main text.](http://108.167.133.27/~keellis/?p=118#main3)_ 

###   

          <a name="works" id="works"></a>WORKS CITED

     Auden, W.H. _Collected Shorter Poems 1927−1957_. New York: Random House, 1966. Print.   

Austin, J. L. _How to do Things with Words_. Cambridge, MA: Harvard UP, 1962. Print.   

_Boileau's Lutrin: A Mock Heroic Poem.  In Six Cantos. Render'd into English Verse_. London: Printed for R. Burrough, J, Baker, E. Sanger and E. Curll, 1708. Print.   

Eves, Charles Kenneth. _Matthew Prior: Poet and Diplomatist_. New York: Octagon, 1939. Print.   

Fellows, Otis. "Prior's 'Pritty Spanish Conceit.'" _Modern Language Notes_ 87 (Nov. 1972): 3−11. Print.   

Gildenhuys, Faith. "Convention and Consciousness in Prior's Love Lyrics." _Studies in English Literature, 1500−1900_ 35.3 (Summer 1995): 437−55. Print.   

Johnson, Samuel. _The Lives of the Most Eminent English Poets, with Critical Observations on their Words._ Ed. Roger Lonsdale. 4 vols. Oxford: Clarendon, 2006. Print.   

Legg, L.G. Wickham. "Torcy's Account of Matthew Prior's Negotiations at Fontainebleau in July 1711." _The English Historical Review_ 29.115 (Jul. 1914): 525−32. Print.   

Longinus. _The Works of Dionysius Longinus, On the Sublime: Or, A Treatise Concerning the Sovereign Perfection of Writing. Translated by from the Greek by Mr Welsted_. London: Printed for Sam. Briscoe, 1712. Print.   

Onnekink, David.  "Mynheer Benting now rules over us": The 1st Earl of Portland and the Re-Emergence of the English Favourite, 1689−99." _The English Historical Review_ 121.492 (Jun. 2006): 693−713. Print.   

Ostwald, Jamel. "The 'Decisive' Battle of Ramillies, 1706: Pre-Requisites for Decisiveness in Early Modern Warfare." _Journal of Military History_ 64.3 (July 2000): 649−77. Print.   

Prior, Matthew. _The Literary Works of Matthew Prior_. Ed. J. Bunker Wright and Monroe K. Spears, 2nd ed. 2 vols. Oxford: Clarendon, 1971. Print.   

Rawson, Claude. "War and the Epic Mania in England and France: Milton, Boileau, Prior and English Mock Heroic." _The Review of English Studies_ 64.265 (2013): 433−53. Print.   

Richardson, Jonathan. "Modern Warfare in Early-Eighteenth-Century Poetry." _SEL_ 45.3 (Summer 2005): 557−77. Print.   

Rippy, Frances Mayhew. _Matthew Prior_. Boston: Twayne, 1986. Print.   

Swift, Jonathan. _Works_. Ed. Herbert Davis. 14 vols. Oxford: Blackwell, 1973. Print.   

Williams, Arthur S. "Panegyric Decorum in the Reigns of William III and Anne." _Journal of British Studies_ 21.1 (Autumn 1981): 56-−7. Print.   

    </blockquote>