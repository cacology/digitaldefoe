---
layout: post
title: "Issue 11.1, Fall 2019"
date: 2019-12-27
---

<div class="issue_home_headers">Features</div>
![https://upload.wikimedia.org/wikipedia/commons/b/b2/A_Shipwreck_on_a_Rocky_Coast.jpg](A_Shipwreck_on_a_Rocky_Coast.jpg)

[catlist id=40 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Pedagogies</div>
[catlist id=42 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Reviews</div>
[catlist id=41 customfield_display="article_author" customfield_display_name=no]
<div class="issue_home_headers">Submit to _The Burney Journal_</div>
<div></div>
<div></div>
<div class="issue_home_headers">_[![](Burney-Journal-Ad-Final-page-001-230x300.jpg)](http://digitaldefoe.org/wp-content/uploads/2020/01/Burney-Journal-Ad-Final-page-001.jpg)_</div>