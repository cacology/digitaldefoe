---
layout: post
title: "Introductory Essay"
date: 2022-01-02
tags: ["Defoe.in.Performance","Issue 13.1-Fall 2021","Issues"]
---

THIS PLAY was written to mark the 300th anniversary of the UK's first great stock market crash-the infamous South Sea Bubble of 1720.[[1]](#_edn1) When I started research for the play, it was not long before it became clear that the works of Daniel Defoe would provide the best source material. Defoe's extensive writing about the South Sea Company, his dramatic writing style, and the brilliance of some of the characters in the journals provided a wealth of potential material.

The central theme of the play is the psychological effect of the mania that surrounded the Bubble and the crash. The imagery of the play is rooted in contagion, perhaps inevitably as the initial draft of the play was written at the start of the coronavirus pandemic. Defoe's extensive writing about plague provided many potent metaphors of disease, fever, infection, and corruption. I hope that this has given the play a disturbing relevance.

One important task in writing the play was the selection of extracts from primary sources for inclusion in the text. In this respect, there are three phases in the play. In the early scenes there are a number of extracts from Defoe's 1719 pamphlet _The Anatomy of Exchange Alley: or, A System of Stock-Jobbing_. In this pamphlet, Defoe's attacks on the "scandalous trade" include reference to "Sham Reports, False News, Foreign Letters, &c. are Things that have been often trumpt upon us"-a verb that was irresistible for a play written in 2020 (cover, 17). The central part of the play draws heavily on the journals that were edited by Defoe, but without entering into the debate about which pieces were written by Defoe himself. Most of the extracts from the journals were taken from editions published in 1720, during the period of the Bubble and the crash.[[2]](#_edn2) In the final scene of the play, Defoe is confronted by the corruption at the heart of the South Sea project. This scene uses extracts from a source that was not written by Defoe-_The Several Reports of the Committee of Secrecy to the Honourable House of Commons, Relating to the late South Sea Directors &c._

About two-thirds of the play is based on these primary sources-the rest is my own invention. The sections of the play that are not drawn from sources are mostly the scenes between Defoe and his daughter Hannah. I wanted to introduce a personal dimension into the play and to create opportunities for us to hear what might have been Defoe's private thoughts. It was clear that this would be more dramatically interesting if he had someone to talk to and the character of Hannah grew in importance during the gestation of the play. There may not be sources for everything I have invented, but I hope there is nothing that clashes with the sources. As Hilary Mantel said in her BBC Reith Lectures about historical fiction, "don't lie. Don't go against known facts .... You can select, elide, highlight, omit. Just don't cheat."

Audio-plays work best with a variety of sound worlds, so the selection of locations is crucial. Having decided to place the scenes between Defoe and Hannah in his private study, I needed to set other scenes in more public places. Some are set in the street, but the main "external" setting is Jonathan's Coffeehouse, a specific location referenced by Defoe in _The Anatomy of Exchange Alley_ (35). It is clear from Defoe's works and other sources that the coffeehouses of the time would have been buzzing with South Sea talk.

Pragmatism influenced some of the choices I made when writing the play. There are so many possible lines to follow in dramatizing the story of the South Sea Bubble. It was necessary, for practical reasons, to keep the length of the play to about one hour, so it was not possible to follow all these potential pathways. Regrettably, the play contains only a brief section that recognises the involvement of the South Sea Company with the slave trade. There is also only a passing mention of the issue of the financial structure of the Company and the national debt. There was also not room to accommodate all of Defoe's changing views about the South Sea Company, the Bubble, and its aftermath. Also, when casting the play, it became clear that I had a wealth of female actors to choose from, so I changed the gender of some of the characters. This was not without some basis in the historical record, as there were many female investors in the South Sea Company. Before settling on Defoe as the central character, I briefly considered using the story of Lady Betty Hastings and her half-sisters. Anne Laurence has examined the differing ways in which these women managed their finances in the context of the financial revolution of the early 18th century, and the South Sea Bubble in particular.

It was an enjoyable experience living with Defoe while writing this play. He was one of the most fascinating characters at a time when fascination was not in short supply. So much of his writing is truly engaging and witty. It is a shame that Defoe was something of an anti-theatrical, as I believe he would have made a fine playwright. As Hannah says in the opening monologue of the play, "there was always that twinkle in his eye. The wit that was in so much of his writing was fundamental to the man - it's who Daniel Defoe was. His sense of mischief was infectious, and we loved him for it."

University of Warwick

NOTES

[1.](#_ednref1) I am grateful to Professor Mark Knights for inviting me to write a play about the South Sea Bubble, and for providing me with help and encouragement throughout the project. I would also like to thank Sue Moore, Alison Pollard, Michael Rolfe, Dr Edward Taylor, and Gordon Vallins for their valuable advice.

[2.](#_ednref2) Most of these extracts were taken from Lee, _Daniel Defoe: His Life and Recently Discovered Writings_.

WORKS CITED

Defoe, Daniel. _The Anatomy of Exchange-Alley: Or, a System of Stock-Jobbing_. London, 1719.

Laurence, Anne. "[Lady Betty Hastings, Her Half‐Sisters, and the South Sea Bubble: Family Fortunes and Strategies](https://doi.org/10.1080/09612020500530539)." _Women's History Review_, vol. 15, pp. 533-40.

Lee, William. _Daniel Defoe: His Life and Recently Discovered Writings: Extending from 1716 to 1729_. London, 1869.

Mantel, Hilary, "Can These Bones Live?" _BBC Radio 4_, July 2017. [https://medium.com/@bbcradiofour/can-these-bones-live-b015dc8397c6](https://medium.com/@bbcradiofour/can-these-bones-live-b015dc8397c6).

_The Several Reports of the Committee of Secrecy to the Honourable House of Commons Relating to the Late South Sea Directors &c_. London, 1721. [https://archive.org/details/pp1312061-2001](https://archive.org/details/pp1312061-2001).

&nbsp;