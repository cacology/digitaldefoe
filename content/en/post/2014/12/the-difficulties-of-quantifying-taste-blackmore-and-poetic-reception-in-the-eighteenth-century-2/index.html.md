---
layout: post
title: "The Difficulties of Quantifying Taste: Blackmore and Poetic Reception in the Eighteenth Century"
date: 2014-12-02
tags: ["Issues","Article Notes","Issue 6.1 - Fall 2014"]
---

## NOTES

<a name="1" id="1"></a>1. The variant runs, "Poet, whoe'er thou art, I say God damn thee, /Take my advice and burn thy _Mariamne_" (Vieth 219).   

            [_Return to main text._](http://108.167.133.27/~keellis/?p=116#main1)  

        <a name="2" id="2"></a>2. The Digital Miscellanies Index can be found at [http://www.digitalmiscellaniesindex.org](http://www.digitalmiscellaniesindex.org). The present essay draws in part on experience of working as a Consultant for this invaluable project, in particular the attribution of canonical poets.     

        _[Return to main text.](http://108.167.133.27/~keellis/?p=116#main2)_  

        <a name="3" id="3"></a> 3. First published in the Pope/Swift _Miscellanies_ volume of 1732; see Gay 2.636, for the attribution to him.   

        _[Return to main text.](http://108.167.133.27/~keellis/?p=116#main3)_ 

        <a name="4" id="4"></a> 4.  For the relationship between the poetics of _Creation_ and Whig ideas of the sublime, see Williams 186-9.   

        [_Return to main text._](http://108.167.133.27/~keellis/?p=116#main4)  

        <a name="5" id="5"></a> 5.  _Creation_ appears seven times, though four of these are reprintings in new editions: _Essays in Prose and Verse_ (1775, two extracts); _The Virgin Muse_ (1717, 1722, 1731); and _The Plain Dealer_ (1730, 1734). _Prince Arthur_ makes twenty appearances: Bysshe's _Art of English Poetry_ (1702, two extracts); _Athenian Sport_ (1707, two extracts); _The Agreeable Variety_ (1717, 1724, 1742, five extracts in each); and _The Morning Walk or City Encompassed_ (1751). His only other frequently included poem is from his _Paraphrase on the Book of Job_ (1700): the extracted version of the 2nd Psalm appearing in the volume of religious verse, _Divine Hymns and Poems on Several Occasions_ (1704, 1707, 1709, 1719, 1757).   

        [_Return to main text._](http://108.167.133.27/~keellis/?p=116#main5)  

###   

          <a name="works" id="works"></a>WORKS CITED

     Anon. _The History of Herod and Mariamne; Collected and Compil'd from the Best Historians, and Serving to Illustrate the Fable of Mr. Fenton's Tragedy. _London: T. Corbet, 1723. Print.   

Anon. _Thesaurus Dramaticus. Containing all the Celebrated Passages, Soliloquies, Similes, Descriptions, and other Poetical Beauties in the Body of English Plays_. 2 vols. London: Sam. Aris, for T. Butler, 1724. Print.   

Beattie, James. _Essays. On poetry and music, as they affect the Mind. On laughter, and Ludicrous Composition. On the utility of classical learning_. Edinburgh: William Creech, 1776. Print.   

Bonnell, Thomas. _The Most Disreputable Trade: Publishing the Classics of English Poetry 1765-1810_. Oxford: Oxford UP, 2008. Print.   

Boyle, Roger. _Dramatic Works of Roger Boyle, Earl of Orrery_. Ed. William Smith. 2 vols. Cambridge MA: Harvard UP, 1937. Print.

Boys, Richard C. _Sir Richard Blackmore and the Wits_. Ann Arbor: U of Michigan P, 1949. Print.   

Bysshe, Edward. _The Art of English Poetry_. 2 vols. London: R. Knaplock, E. Castle & B. Tooke, 1702. Print.   

Cibber, Theophilus. Ed. _The Lives of the Poets of Great Britain_. 5 vols. London: R. Griffiths, 1753. Print.   

Cowper, William. _The Letters of William Cowper: Vol 1, 1750−1781_. Ed. James King and Charles Ryskamp. Oxford: Clarendon, 1979. Print.   

Culler, A. D. "Edward Bysshe and the Poet's Handbook," _PMLA_ 63 (1948): 858−85. Print.   

Dennis, John. _The Critical Works of John Dennis_. Ed. Edward N. Hooker. 2 vols. Baltimore: Johns Hopkins UP, 1939−43. Print.   

Derrick, Samuel. _A Poetical Dictionary_. 4 vols. London: J. Newberry, 1761. Print.   

Dodsley, R. _A Collection of Poems by Several Hands_. Ed. M. Suarez. 6 vols. London: Routledge, 1997. Print.   

Dryden, John. _The Works of John Dryden_. Ed. E. N. Hooker, E. H. T. Swedenberg, et al. 20 vols. Berkeley: U of California P, 1956−2000. Print.   

Fairchild, Hoxie. _Religious Trends in English Poetry, I, 1700−40_. New York: Columbia UP, 1939.  Print.   

Fenton, Elijah. _Mariamne. A Tragedy_. London: J. Tonson, 1723. Print.   

Gay, John. _The Poems and Plays of John Gay_. Ed. Vinton Dearing and Charles Beckwith. 2 vols. Oxford: Clarendon, 1978. Print.   

Jacob, Giles. _The Poetical Register_. 2 vols. London: A. Bettesworth, W. Taylor, J. Batley, 1723. Print.   

Jauss, Hans Robert, trans. Timothy Bahti. _Towards an Aesthetic of Reception_. Brighton: Harvester, 1982. Print.   

Johnson, Samuel. _The Lives of the Poets_. Ed. Roger Lonsdale. 4 vols. Oxford: Oxford UP, 2006. Print.   

Langbaine, Gerard. _An Account of the English Dramatick Poets_. 2 vols. London: George West & Henry Clements, 1691. Print.   

Lloyd, Robert, _The Poetical Works of Robert Lloyd_. 2 vols. London: T. Evans, 1774. Print.

Marshall, Ashley. _The Practice of Satire in England, 1658-1770_. Baltimore: Johns Hopkins UP, 2013. Print.   

Oldham, John. _Compositions in Prose and Verse of Mr. John Oldham_. Ed. Edward Thomson. 3 vols. London: W. Flexney, 1770. Print.   

--. _The Poems of John Oldham_. Ed. Raman Selden and Harold Brooks. Oxford: Oxford UP, 1987. Print.   

Pope, Alexander. _The Art of Sinking in Poetry_. Ed. E. L. Steeves. New York: Russell and Russell, 1968. Print.   

--. _The Dunciad in Four Books_. Ed. Valerie Rumbold. London: Longman, 1999. Print.   

--. _The Poems of Alexander Pope_. Ed. John Butt. London: Methuen, 1963. Print.   

Pordage, Samuel. _Herod and Mariamne: a Tragedy_. London: W. Cademan, 1672. Print.   

--. _Azaria and Hushai_. London: Charles Lee, 1682. Print.   

Potter, Robert. _An Inquiry into some Passages in Dr Johnson's <u>Lives of the Poets</u>_. London: J. Dodsley, 1783. Print.   

Reddick, Allen. _The Making of Johnson's Dictionary, 1746−1773_. 2nd ed. Cambridge: Cambridge UP, 1996. Print.   

Rochester, John Wilmot, 2nd Earl. _The Complete Poems_. Ed David Vieth. New Haven: Yale UP, 1968. Print.   

Seward, Anna. _The Letters of Anna Seward_. 6 vols. Edinburgh: Constable, 1811. Print.   

Solomon, Harry M. _The Rape of the Text: Reading and Misreading Pope's <u>Essay on Man</u>_. Tuscaloosa: U of Alabama P, 1993. Print.   

Williams, Abigail. _Poetry and the Creation of a Whig Literary Culture_. Oxford: Oxford UP, 2005. Print.   

Womersley, David. Ed. _Augustan Critical Writing_. Harmondsworth: Penguin, 1997. Print.