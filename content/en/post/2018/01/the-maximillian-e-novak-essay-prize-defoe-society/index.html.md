---
layout: post
title: "The Maximillian E. Novak Essay Prize (Defoe Society)"
date: 2018-01-06
tags: ["Issues","Issue 9.1 - Fall 2017","Announcements"]
---

The [Defoe Society](http://www.defoesociety.org) invites submissions for the best published essay (chapter in a book or journal article) on Daniel Defoe. The winning essay will receive $200 / £125.

Please send the published version of your essay either as an email attachment or in hard copy to Sharon Alker (alkersr@whitman.edu) Whitman College, 345 Boyer Avenue, Walla Walla, WA. 99362. It must arrive by 1 March 2018. Applicants will be notified of the outcome by 15 May 2018.

Eligibility:

- You must be a member in good standing of the Defoe Society. Join [here](http://www.defoesociety.org) for $35 (faculty) / $10 (student). Society membership is also required for attendance at the biennial conference.

- As well as self-nominations, members are welcome to nominate another person's essay, though it will only be considered for the prize if the nominee has joined the Society by the deadline.

- The essay must have been published (in English) with a 2016 or 2017 imprint.

- It must deal substantially with any aspect of Defoe's life or writings, but does not have to be exclusively on Defoe.

- Members of the sub-committee judging the prize are ineligible.

The prize will be judged by a committee comprising members of the Defoe Society Executive Board. The judges reserve the right not to award a prize or to award it jointly (splitting the award). Please direct queries to alkersr@whitman.edu.