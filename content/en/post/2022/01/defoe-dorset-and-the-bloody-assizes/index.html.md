---
layout: post
title: "Defoe, Dorset and the Bloody Assizes"
date: 2022-01-02
tags: ["Notes.2021","Issue 13.1-Fall 2021","Issues"]
---

IN THIS ESSAY, I aim to demonstrate the significance of Dorset for Daniel Defoe in two sections. First, using Defoe's _A Tour Through the Whole Island of Great Britain_ (1724), I establish how this county frames Defoe's early trading life and suggest that Defoe was trading out of Lyme Regis due to his extensive knowledge of the people and customs of the area. Then I turn to Defoe's _Robinson Crusoe_ (1719), with Dorset appearing allegorically as the location for two lifesaving events.

I

Among the various counties Defoe visited during his lifetime, Dorset appears to have had a special place in his heart and mind. It can be established from Defoe's trade as a hosier that visits to the West-County during the early 1680s for woollen products were the probable reason for his intimate knowledge of the area (_Defoe _45). Defoe was also acquainted with Lord Maitland, Earl of Dorset, whose possible patronage gave Defoe access to a small but exclusive English literature society during the same period. Defoe quotes and states the purpose of the society by using founding member Wentworth Dillon, Earl Roscommon's _Essay on Translated Verse_ (1684):

For who did ever in French authors see

The comprehensive English energy?

The weighty bullion of one sterling line,

Drawn to French wire would through whole pages shine. (_An Essay _138)

Defoe's appraisal of Dorset was complimentary in _A Tour_. It is worth mentioning first a recent addition to Dorset, Christchurch (from 1974 formerly in Hampshire):

From hence there are but few towns on the sea coast west, nor are there any harbours, or sea ports of any note, except Pool. As for Christ Church, though it stands at the mouth of the Avon, which, as I have said, comes down from Salisbury, and receives also the Stour and Piddle, two Dorsetshire rivers, which bring with them all the waters of the north part of Dorsetshire; yet it is a very inconsiderable poor place, scarce worth seeing, and less worth mentioning in this account; only, that it sends two members to Parliament, which many poor towns in this part of England do, as well as that.

Defoe obviously did not spend much time in Christchurch, if any, and had he done so, Defoe would have discovered the rich history of this ancient town. Perhaps he rode through on horseback or drove through in a horse and carriage, neglecting to appreciate properly the "worth" of the place.

As Defoe ventures into Dorset, his views, however, changed. The next town he comments on is Wimborne, which unbeknown to the author, would prove an important depository for the manuscripts of his first discovered works, _Meditations_ (1681) and _Historical Collections_ (1682). These priceless items were kept safe after Defoe's death (1731) by his spinster daughter Hannah in Wimborne, until her death on 28 April 1759. Hannah is interred in Wimborne Minster along with her sister, Henrietta, and brother-in-law John Boston. The plaque marking their place of burial has been removed, though through the writings of Nicholas Russell, it is possible to recover their final resting place. The plaque was in the early nineteenth century, "about the middle of the [North] Aile" (_A Historical Account_ 27).

Ironically, Defoe quotes from a burial, in the same church during his visit, as well as providing the fact that Wimborne Minster had a spire:

The church, which is indeed a very great one, ancient and yet very well built, with a very firm strong square tower, considerably high; but was, with out doubt, much finer, when stood on top of it, stood a most exquisite spire, finer and taller, if fame lies not, than that at Salisbury and, by its situation, in a plainer, flatter country, visible, no question, much farther.....[F]or here are the monuments of several noble families; and in particular of one king, viz. King Ethelred, who was slain in battle by the Danes. He was prince famed for piety and religion, and, according to the zeal of these times, was esteemed as a martyr; because venturing his life against the Danes, who were heathens, he died fighting for his religion and his country. The inscription upon his grave is preserved, and has been carefully repaired, so as to be easily read, and is as follows

In hoc loco quiescit Corpus S. Etheldredi, Regis West Saxonum, Martyris, qui Anno Dom. DCCCLXXII. Xxiii. Aprillis per Manus Danorum Paganorum Occubuit.

In English thus:

Here rests the body of Holy Etheldred, King of the West Saxons, and martyr, who fell by the hands of the pagan Danes, in the year of our Lord 872, the 23d of April. (_A_ _Tour_ 206)

After Wimborne, Defoe turns his attentions to Poole; his interest here was the oysters trade, these oysters being, according to him, "the best" in the West-Country, and were pickled in barrels and taken up to London as well as sent to "the West Indies, and to Spain, and Italy, and other parts." Pearls were often found in Poole's oysters, and considered to be "larger than in any other oysters about England" (_A_ _Tour_ 207).

Another industry worthy of Defoe's compliments was the stone quarries of the Isle of Purbeck. He notes:

This part of the country is eminent for vast quarries of stone, which is cut out flat, and used in London in great quantities for paving court-yards, alleys, avenues to houses, kitchens, footways on the sides of the high-street, and the like; and is very profitable to the place, as also in the number of shipping employed in bringing it to London. There are also several rocks of very good marble, only that the veins in the stone are not black and white, as the Italian, but grey, red, and other colours. (_A Tour_ 207)

Defoe mentions Dorchester next, commenting on the liberal attitudes of the town dwellers and remarking that, "the people seemed less divided into factions and parties, than other places; for though here are divisions and the people are not of one mind, either as to religion, or politics, yet they did not seem to separate with so much animosity as in other places." So much so, that he observed a Church of England clergyman and the Dissenting minister taking tea together. And of the town, he awards the honour that "a man that coveted a retreat in the world might as agreeably spend his time, and as well in Dorchester, as in any town I know in England" (_A Tour_ 209).

While information about his residence in Dorset is perhaps coincidental, Nathaniel Mist, the likely author of _A General History of the Pyrates_ (1724, 1728) and an associate of Defoe's, chose Dorset as a place to convalesce. It is also possible that Mist spent time writing in Dorset for seven months and almost certain that he went to Dorchester on Defoe's recommendation (_Nathaniel Mist_ 31).[[1]](#_edn1)

Defoe comments extensively on the surrounding areas. He was amazed at the number of sheep in the downs around the county town. He was advised that there were 600,000 sheep fed on the downs within six miles of the town. This at first he found to be unbelievable, but on closer consideration and inspection, he concludes "I confess I could not but incline to believe it. The grass, or herbage of these downs is full of the sweetest, and the most aromatic plants, such as nourish the sheep to a strange degree, and the sheep's dung again nourishes that herbage to a strange degree" (_Tour_ 209). Defoe's visit to Dorchester appears to have been from 1705 when he wrote to Robert Harley, the then Secretary of State, during a fact-finding mission (_The Letters_ 105).

After Dorchester, Defoe mentions the costal town of six miles distance, Weymouth. Of interest to him here was the trade between the port and France, Portugal, Spain, Newfoundland, and Virginia. Defoe recollects an incident that occurred when he was there some time before of a merchant vessel, homeward bound from Oporto to London, which took shelter from a storm in Portland Road after it lost an anchor and struck her topmast. Giving distress signals, the Weymouth men went to her rescue and worked out what to do:

Upon this, the Weymouth boats came back with such diligence, that in less than three hours, they were on board them again with an anchor and cable, which they immediately bent its place, and let go to assist the other, and thereby secured the ship. 'Tis true, that they took a good price of the master for the help they gave him; for they made him draw a bill on his owners at London for 12l. [£] for the use of the anchor, cable and boat, besides some gratuities to the men. But they saved the ship and cargo by it, and in three or four days the weather was calm, and he proceeded on his voyage, returning the anchor and cable again; so that, upon the whole, it was not so extravagant as at first I thought it to be (_A Tour_ 210-211).

Frank Bastian believes that this incident could well have been Defoe's ship (probably the _Pride of London_) with him on board. The time of this incident, in the late 1680s, links in with the expanding of his trade to incorporate "wines and brandies." P. N. Furbank and W. R. Owens comment that Defoe was at this time "in correspondence with France, Spain, Portugal and America" (_The True-Born_ viii).

II

Another similarity that points to Defoe's participation in this incident appears in _Robinson Crusoe _(1719). As with the ship of the Weymouth storm, Crusoe's ship was in a previous storm, after leaving from Hull on its way to London, with a second storm in Yarmouth Roads:

Towards Evening the Mate and Boat-Swain begg'd the Master of our Ship to let them cut away the Foremast, which he was very unwilling to: But the Boat-Swain protesting to him, that if he did not, the Ship would founder, he consented; and when they had cut away the Foremast, the Main-Mast stood so loose, and shook the Ship so much, they were obliged to cut her away also, and make a clear Deck. (12)

Another trip to Dorset and the West-Country is noted by Bastian as the basis of the information provided by Defoe in his _Tour_. It was in the early summer of 1700, according to his calculations, that Defoe made a trip with his brother-in-law Robert Davis, an inventor of a diving machine, to Cornwall's Polpeor Cove at the Lizard, to look for rich pickings off shipwrecks.

They "rode in view of the sea" to Weymouth and then ferried across "with boat and a rope" to the Isle of Portland where, "tho' seemingly miserable, and thinly inhabited, yet the inhabitants being almost all stone-cutters, we found there was no very poor people among them." Passing the famous swannery at Abbotsbury, they continued along the coast towards Bridport, observing the fishermen seining for mackerel, which that year were so plentiful that the country folk came with carts to buy fish to manure their fields. (_Early Life_ 222)

Portland is mentioned for its stone, being the place "our best and whitest free stone comes, with which the cathedral of St. Paul's, the Monument, and all the public edifices in the city of London, are chiefly built." It is in the following line that his purpose of travelling to a destination is revealed. "Tis wonderful," Defoe exclaims, "and well worth the observation of a traveller to see the quarries in the rock, from whence they are cut out, what stones, and of what prodigious a size are cut out there" (_A Tour_ 211).

Bridport, as mentioned above, was famous for its mackerel. Even so, Defoe relates a curious anecdote about the farmers who came to manure their fields with the fish and who were prevented from buying by "the justices and magistrates of the towns about." After some enquiry, Defoe found out that it was "thought to be dangerous, as to infection." So plentiful was the catch that year, that fish, which were fine and large, "were sold at the sea side a hundred for a penny."

From Bridport, the next stop on the journey west was Lime, where he mentions the Duke of Monmouth briefly, and boasts about its the harbour, "'tis such a one as is not in all Britain besides, if there is such a one in any part of the world" (_A Tour_ 212). Known as the Cobb, it was a

massy pile of building, consisting of high and thick walls of stone. The walls are raised in the main sea, at a good distance from the shore; it consists of one\ main and solid wall of stone, large enough for carts and carriages to pass on the top, and to admit houses and ware houses to be built on it; so that it is broad as a street; opposite to this, but farther into the sea, is another wall of the same workmanship, which crosses the end of the first wall, and comes about with a tail, parallel to the first wall. Between the point of the first or main wall, is the entrance into the port, and the second, or opposite wall, breaking the violence of the sea from the entrance, the ships go into the basin, as into a pier, or harbour, and ride there as secure as in a mill pond, or as in a wet dock. (_A Tour_ 213)

It appears from his _Tour_ that Defoe and his companion, stayed some time at Bridport. Though it is apparent that Defoe's knowledge and connection with the local trade indicate a deeper connection than that of a mere traveller. So much so, that he has the time to explore the friendliness of the people as well as "observe the pleasant way of conversation, as it is managed among the gentlemen of this county, and their families, which are without reflection some of the most polite and well bred people in the isle of Britain." Of the ladies, he was particularly admiring of the way they were treated when it came to marriage, "no Bury Fairs, where the women are scandalously said to carry themselves to market." Defoe ascribes this treatment to the plain fact that "the Dorsetshire ladies are equal in beauty, and maybe superior in reputation. And yet the Dorsetshire ladies, I assure you, are not nuns, they do not go veiled about streets, or hide themselves when visited." So well acquainted was he with the gentry of Dorset, that Defoe could make the statement that he met no equal "in all my observation, through the whole isle of Britain" (_A Tour_ 213-14).

Evidence of Defoe's early trading links includes his visits to Blandford and Stalbridge. Blandford is noted as having "the finest bonelace in England." Stalbridge would appear to have been a well-known place as it used to make "the finest, best, and highest prized knit stockings in England; but that trade now is much decayed by the increase of the knitting-stocking engine, or frame which has destroyed the hand knitting-trade for fine stockings through the whole kingdom."

Defoe was among the men who appeared on the side of the Duke of Monmouth after his landing at Lyme Regis, during the Monmouth Rebellion of 1685.[[2]](#_edn2) Various explanations have been offered for his mentioning involvement; some attribute it to a total fiction, while others suggest he escaped back into London or to Europe.[[3]](#_edn3) But the most in depth and best researched is offered by Bastian, who states that Defoe was only with the Duke and his followers for a short period of time. He left the army and returned to London when the Duke's tactics proved indecisive (_Early Life_ 114). However, it is more likely that Defoe fled abroad and gained his pardon via requests from his wife.

As soon as the news had reached London of the Duke's arrival, the city was sealed off to stop Protestants leaving and joining Monmouth. According to Elizabeth D' Oyley, Defoe "was on the road, a young man of twenty five," when Monmouth landed (_The Duke_ 281). In his _Review_, Defoe reflected that "I remember how boldly abundance of men talked for the Duke of Monmouth, when he first landed; but if half of them had as boldly joined him sword in hand, he had never been routed at Kings-sedg-moor" (_Review_ 154).

As for the pardon, that came some years later (my italics):

1687 May 31. Windsor.

Warrant to the Justices of Assize and Gaol Delivery for the Western Circuit and all other whom it may concern - after reciting that the King had extended his grace and mercy to Thomas Pluse of Edington, Henry Pitman of Yeovell, Wm. Pitman of Sandford Oreas, Daniel Pomroy of Taunton, John Edward of Trull, Azarians Pinny of Axminster, George Mullins, sen., of Taunton, John Collins of Chard. George Pickard of Rhode, Joseph Gayland late of Exeter, Wm. Savage of Taunton, Edward Babke late of Tull, John Oram of Warminster , Thomas Pumphrey late of Worcester, William Horsley late of St. Martin in the Fields, Nicholas Scading of Bhgon Green, James Canyer of Ilminster, John Bovett of Taunton, William Way of Combe St. Nicholas, Robert Hucker of Taunton, woolcomber, William Gaunt of Wapping, Richard Lucus of Dulverton, John Marther alias Marder of Crewkerne, George Puvior of Longport, Benjamin Alsopp late of London, Christopher Eason of Chard, Brian Connory, John Woolters, Andrew Speed, _Daniel Foe_, John Harper, George Richmond, and Martin Goddard who, were engaged in the late rebellion - for causing the said persons to be inserted in the General Pardon, without any condition of transportation. (_State Papers_ 440; my italics)

The fact that Defoe was pardoned in this list among non-combatants indicates that he was in the area as a tradesman, caught up in the mêlée of preparations and the need for transport for battle. A hosier with cart transport would have likely been pressed into service to assist by transporting goods to and wounded from the battlefield.

Defoe mentions the Battle of Sedgemoor later in his life, in _A Tour_:

Had he [Duke of Monmouth] not, either by the treachery, or mistake of his guides, been brought to an unpassable ditch, where he could not get over, in the interval of which, the king's troops took the alarm, by the firing a pistol among the duke's men, whether, also, by accident, and his own fate, conspired to his defeat, he had certainly cut the Lord Ferersham's army (for he commanded them) all to pieces; but by these circumstances, he was brought to a battle on unequal terms, and defeated: the rest I need not mention (_A Tour_ 354).

Among the executed men were three former members of Rev. Charles Morton's Dissenter Academy in Surrey, which Defoe attended. Defoe refers to them in 1712, as the members of the 'West Country Martyres . . . Kitt. Battersby, Young Jenkins, Hewlin' (_The Present State_ 319). As Bastian points out, Battersby, was Christopher Battiscombe, son and heir to a Dorset country estate. The other two were executed at Taunton on 30 September 1685, which could have been Defoe's twenty-sixth birthday.

In _Robinson Crusoe_ (1719), considered by many and mentioned by Defoe as an allegory, he has his character saved in "a strange Concurrence of Days, in the various Providences which befell me," for "The same Day of the Year I was born on, (_viz._) the 30_<sup>th</sup> September_, that same Day I had my Life so miraculously saved 26 Years after" (_Crusoe_ 113). In the preface to _The Reflections of Robinson Crusoe_ (1720), Defoe states: "I _Robinson Crusoe_ being at this Time in perfect and sound Mind and Memory . . . that the Story, though Allegorical, is also Historical; and is the beautiful Representation of a Life of unexplained Misfortunes." This of course would make the year of Defoe's birth 1659, and not 1660 which is often stated. His birth is not registered in any Parish Records that have been consulted to date, and most probably it has not been recorded in any.

This note has endeavoured to articulate the importance to Defoe of the county of Dorset. A large proportion of Defoe's early trading life can be framed in the towns of Stalbridge and Blandford. These two places, with Lyme Regis servings as a trading port, would have offered the answer to one of the great mysteries of Defoe's life. How did he manage to evade capture after the unsuccessful Monmouth Rebellion? If Defoe was trading at Lyme during the landing of Monmouth, then his escape from James II's army was by sea. Defoe, by his own claims, knew the way of life of the gentry very well, even the details of women's marriage arrangements. It can be safe to argue, that Defoe's escape from the Bloody Assizes was from Lyme Regis, probably to Holland where Monmouth's army originated. Two incidences that appear in the early part of _Robinson Crusoe_ stem from this county. With one of Defoe's daughters marrying a Dorset Customs Officer and two daughters' burials in Wimborne Minster, this provides a lasting testimony to Defoe's connection to and fondness for Dorset.

University of Exeter

NOTES

[1.](#_ednref1) For reports of Mist's stay in Dorset see _Daily Journal_.

[2.](#_ednref2) Monmouth and his men landed on the coast of Dorset at Lyme Regis in the afternoon of 11 June 1685. For more information on this invasion, see Earle.

[3.](#_ednref3) Paula Backscheider, for example, believes that Defoe had taken part in the decisive Battle of Sedgmoor and possessed "the kind of pass that merchants got from lord major or the secretary of state. Certainly he had travelled enough on his business to be a more credible traveller than most" (39).

WORKS CITED

Backscheider, Paula. _Daniel Defoe: His Life_. Johns Hopkins UP, 1989.

Bastian, Frank. _Defoe's Early Life_. Macmillan, 1981.

Bialuschewski, Arne. "Daniel Defoe, Nathaniel Mist, and the _General History of the Pyrates_," _The Papers of the Bibliographical Society of America_, vol. 98., no. 1, 2004, pp.21-38.

_Calendar of State Papers, Domestic Series_. James II, Volume II, January 1686 - May 1687. HMSO 1964.

_ __Daily Journal_, 25 April 1722, _The Weekly Journal; Or, British Gazetteer_, 28 April 1722.

Defoe, Daniel. _An Essay Upon Projects_ (1697). Cassell & Company Limited, 1887.

---. _Review_, Vol IX. Columbia UP, 1938.

---. _The True-Born Englishman_ _and Other Writings_ (1701, edited by P. N. Furbank and W. R. Owens, Penguin Books, 1997.

---. _The Letters of Daniel Defoe_, edited by George H. Healy, Oxford UP, 1955.

---. _A Tour Through the Whole Island of Great Britain, _edited by Pat Rogers, Penguin, 1971.

---. _The Present State of Parties in Great Britain_ (1712). Eighteenth Century Collections Online, Gale.

---. _Robinson Crusoe_, edited by Thomas Keymer and annotated by Keymer and James Kelly, Oxford UP, 2007.

D'Oyley, Elizabeth. _The Duke of Monmouth_. Geoffrey Bles, 1938.

Earle, Peter. _Monmouth's Rebels: The Road to Sedgemoor, 1685_. Weidenfeld & Nicolson, 1977.

Russell, Nicholas. _A Historical Account of the Antiquity, Ancient Funeral Monuments and Endowments, of the Colligiate Church of Wimborne Minster in the County of Dorset; and Chapel of St. Margaret_. John Nichols and Son, Red Lion Passage, Fleet Street 1803.

Sutherland, James. _Defoe_. Methuen, 1937.