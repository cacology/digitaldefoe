---
layout: post
title: "Submission Guidelines"
date: 2014-09-25
---

_Send all submissions to [Stephanie Hershinow ](mailto:stephanie.insley@gmail.com)_and_ [Christopher Loar.](mailto:christopher.loar@wwu.edu)_

**Peer Review**
_Digital Defoe: Studies in Defoe & His Contemporaries_ is a peer reviewed journal.  Submissions, regardless of format, will undergo a mutually-anonymous peer review process. (Book reviews and some solicited contributions will not undergo peer review.)

**Format**
_Digital Defoe_: _Studies in Defoe & His Contemporaries_ encourages and accepts submissions in a wide variety of formats, from print documents saved as Word files to hyperlinked documents, pedagogical resources, files with embedded video and audio, movies, 3D and virtual pieces, photo documentaries, databases, and interactive webtexts.

**Length**
As noted above, _Digital Defoe_: _Studies in Defoe & His Contemporaries_ is committed to publishing a diverse range of academic articles, notes, reviews, pedagogical materials and multimedia material. Academic articles should be between 4000 and 7000 words in length. Academic notes should be between 1500 and 3500 words in length. Book reviews should be between 1500 and 2000 words in length. For information on the acceptable length of (or required memory for) pedagogical materials or multimedia material, please contact the journal's co-editors.

**Awards
**Essays published in the journal are eligible for [several awards from the Defoe Society](https://www.defoesociety.org/awards/), including the Sharon Alker, Katherine Ellison, and Holly Nelson Prize, a biennial award for the best published essay in _Digital Defoe_ that is not principally about Daniel Defoe.

**Style**
_Digital Defoe_: _Studies in Defoe & His Contemporaries_ uses the most recent edition of _The Modern Language Association Style Manual and Guide to Scholarly Publishing_. Spelling in Digital Defoe is American.

**Endnotes & Citations**
Parenthetical citations are used in _Digital Defoe_: _Studies in Defoe & His Contemporaries_. If any explanatory endnotes are required, they should be formatted according to the most recent edition of _The Modern Language Association Style Manual and Guide to Scholarly Publishing_ and double-spaced.

**Fees**
_Digital Defoe: Studies in Defoe & His Contemporaries_ does not have charges for article processing or article submission.