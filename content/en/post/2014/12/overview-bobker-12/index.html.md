---
layout: post
title: "Closets Without Walls - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

[DOWNLOAD  SPREADSHEET FOR COMPLETE INFORMATION](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/closetswithoutwalls.htm)

## «1500 - [1550](#1550) - [1600](#1600) - [1650](#1600) - [1700](#1700) - [1710](#1710) - [1720](#1720) - [1730](#1730) - [1740](#1740) - [1750](#1750) - [1760](#1760) - [1770](#1770) - [1780](#1780) - [1790](#1790) - [1800](#1800) » 

**1550 - 1600**<a name="1550" id="1550"></a>  

                  1569	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Closet of Counsells conteining the advice of divers philosophers   

                  1573	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The treasurie of commodious conceits  

                  1591	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Christian mans closet  

                  1596	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Psalmes of confession found in the cabinet of the most excellent King of Portinga  

                  **1600 - 1650**<a name="1600" id="1600"></a>  

                  1608	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A closet for ladies and gentlewomen  

                  1616	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The rich cabinet furnished with varietie of excellent discriptions  

                  1630	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The chyrugians closet  

                  1632	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Two spare keyes to the Jesuites cabinet  

                  1637	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Curiosities: or, the cabinet of nature  

                  1639	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The ladies cabinet opened  

                  1640	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jocabella, or a cabinet of conceit  

                  1641	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cupids cabinet unlock't  

                  1642	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A true narration of the surprizall of sundry cavaliers  

                  1644	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ruperts sumpter, and private cabinet rifled  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Key to the kings cabinet-counsell  

                  1645	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A key to the Kings cabinet  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A satyr, occasioned by the author's survey of a scandalous pamphlet intituled, the King's cabanet opened  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Kings cabinet opened  

                  1646	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Irish cabinet: or His Majesties secret papers  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Lord George Digby's cabinet and Dr Goff's negotiations  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To a vertuous and judicious lady who (for the exercise of her devotion) built a closet  

                  1648	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A key to the cabinet of the Parliament, by their remembrancer  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A vindication of King Charles  

                  1650	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The King of Scotlands negotiations at Rome  

                  **1700 - 1709**<a name="1700" id="1700"></a>  

                  1700	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;England's choice cabinet of rarities; or The famous Mr. Wadham's last golden legacy  

                  1701	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A cabinet of choice jewels  

                  1703	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Country Physician  

                  1704	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Parents Pious Gift  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A rich cabinet of modern curiosities containing many natural and artificial conclusions...  

                  1706	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The accomplish'd Lady's Delight  

                  1707	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Second Volume of the Phenix  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Phenix Volume One  

                  1709	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Poor Man's Help, and Young Man's Guide  

                  **1710 - 1719**<a name="1710" id="1710"></a>  

                  1710	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Compleat English and French Vermin-Killer  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A coppy of verses writt in a Common Prayer Book  

                  1711	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aristotle's Last Legacy  

                  1714	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;England's Mournful Monument  

                  1715	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hocus Pocus  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Who Runs next  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wit's Cabinet  

                  1718	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A collection of curious prints and drawing by the best masters in Europe  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ladies Cabinet broke open, Part 1  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Letters, poems, and tales  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The French Momus  

                  1719	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Book of Psalms Made Fit for the Closet with Collects and Prayers  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Christian's Plea for His God and Saviour Jesus Christ  

                  **1720 - 1729<a name="1720" id="1720"></a>**  

                  1721	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Closet Piece: The Experimental Knowledge of the Ever-Blessed God  

                  1724	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A manual history of Repentance and Impenitence  

                  1725	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A letter from the Man in the Moon to Mr. Anodyne Necklace  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Delights for young Men and Maids  

                  1726	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Genuine Letters of Mary Queen of Scots to James Earl of Bothwell  

                  1728	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mist's Closet Broke Open  

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Christ's famous titles  

                  1729	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A brief history of the Restauration  

                  **1730 - 1739<a name="1730" id="1730"></a>**  

                  1730	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Christian's duty from the sacred scriptures  

                  1731	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A general history of the proceedings and cruelties of the court of inquisition in Spain, Portugal  

                  1732	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duties of the Closet  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Flower-Garden Display'd  

                  1733	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gloria Britannorum or, The British Worthies  

                  1739	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Religion the most delightful employment  

                  **1740 - 1749<a name="1740" id="1740"></a>**  

                  1740	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Incomparable varieties  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seven conferences held in the King of France's Cabinet of Paintings  

                  1743	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Ladies Cabinet  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Book of Rarities: Or, Cabinet of Curiosities Unlock'd  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Lady's companion  

                  1746	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Irish Cabinet  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A call to the unconverted  

                  **1750 - 1759<a name="1750" id="1750"></a>**  

                  1750	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cupid's Cabinet Open'd  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;M----C L----N's cabinet broke open  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The second part of Mother Bunch of the West  

                  1752	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Gentleman and Lady's Palladium  

                  1753	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proposals for publishing by subscription from the curious and elaborate works of Thomas Simon  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fragment of the chronicles of Zimri the Refiner  

                  1754	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cabinet  

                  1755	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Lovers Cabinet  

                  1756	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Pleasing Instructor  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;An Account of a Useful Discovery  

                  1757	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apollo's Cabinet or the Muses Delight  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Cabinet of Jewels opened to the Curious, by a key of Real Knowledge  

                  1758	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A catalogue of the collection of pictures, etc.   

                  1759	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The General State of Education in the Universities  

                  **1760 - 1769<a name="1760" id="1760"></a>**  

                  1760	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The state of France  

                  1762	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The British Phoenix  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Female Pilgrim  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Parallel  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A cabinet of choice jewels  

                  1763	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Believer's Golden Chain  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The private tutor to the british youth  

                  1764	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Christian's New Year's Gift: containing a companion  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mrs. Pilkington's Jests  

                  1765	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Golden Cabinet  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Miss C--Y's cabinet of curiosities  

                  1768	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Art's Master-Piece  

                  1769	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A catalogue of the cabinet of birds, and other curiosities  

                  **1770 - 1779<a name="1770" id="1770"></a>**  

                  1770	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Christian's Closet-Piece: Being An Exhortation to all People To forsake their Sins, Which too much   

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                  Reign in the present Age: As Pride, Envy, ...   

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The New Week's Preparation for a Worthy Receiving of the Lords Supper  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Particulars of and conditions of sale for a large and valuable estate called Goldings  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Spirit of Liberty  

                  1771	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Muses Cabinet  

                  1772	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The riches and extent of free grace displayed  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Christian's Preparation for the worthy receiving of the Holy Sacrament of the Lord's Supper  

                  1773	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thane's second Catalogue  

                  1774	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vox Populi  

                  1775	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A catalogue of the elegant cabinet of natural and artificial rarities of the late ingenious Henry Baker, Esq.   

                  1776	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Thousand Notable Things on Various Subjects  

                  1779	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Catalogue of the genuine, curious, and valuable  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instructions for a prince  

                  **1780 - 1789**<a name="1780" id="1780"></a>  

                  1781	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sunday Thoughts .4  

                  1783	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The cabinet of True Attic Wit  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cyprian Cabinet  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Modern Family Physician  

                  1785	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Elegant Drawing and Cabinet Pictures  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For the Inspection of the CuriousFor the inspection of the curious...a cabinet of royal figures  

                  1786	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Laird of Cool's Ghost  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cabinet of Momus and Caledonian Humorist  

                  1787	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Catalogue of that Superb and Well Known Cabinet of Drawings of John Barnard, Esq.   

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cabinet of Genius  

                  1788	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Every Lady her own Physician or the Closet Companion  

                  **1790 - 1799<a name="1790" id="1790"></a>**  

                  1790	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The housekeeper's valuable present  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Last Night  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Golden Cabinet  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Copys of several conferences and meetings  

                  1791	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A catalogue of a well-chosen and select collection of Pictures  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Elegant and Copious History of France. Number 1.   

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Closet Companion  

                  1792	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Copper Plate Magazine  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A catalogue of pleasing assemblage of prints...  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cabinet of Love  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Royal Jester  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To be seen in Curtius's Cabinet of Curiosities  

                  1793	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monthly Beauties  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Golden Cabinet  

                  1794	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A catalogue of the valuable museum  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Cabinet of Miscellanies  

                  1795	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cabinet of Curiosities, No. 1  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The world's doom: or the cabinet of fate unlocked. Vol.1  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The world's doom: or the cabinet of fate unlocked. Vol. 2	  

                  1796	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cabinet Litteraire  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Coins and medals, in the cabinets of the Earl of Fife  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gale's Cabinet of Knowledge  

                  1797	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;History of Mother Bunch of the West  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;History of Mother Bunch of the West, Part the Second  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Oxford Cabinet  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Specimens of British Minerals selected from the Cabinet of Philip Rasleigh  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cabinet  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The cabinet of wit	  

                  1798	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A key to natural history  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A key to the Six Per Cent Cabinet  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lineal Arithmetic  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Beautiful Cabinet Pictures  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Catalogue of the geniune  

                  1799	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Naturalist's Pocket 1  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Naturalist's Pocket 2  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Naturalist's Pocket 3  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Naturalist's Pocket 4  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Naturalist's Pocket 5  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Naturalist's Pocket 6  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phylaxa Medinae. The cabinet of physick  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Catalogue of the intire Cabinet of Capital Drawings, collected by the late Greffier Francois Fagel  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Cabinet of Fancy  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The cabinet of the arts  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A Companion to Bullock's Museum  

                  **1800<a name="1800" id="1800"></a>**  

                  1800	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cabinet of Beasts  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Curtius's Grand Cabinet of Curiosities  

                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mother Bunch's Closet broke open...Part the Second                