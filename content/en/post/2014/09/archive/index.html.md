---
layout: post
title: "Archive"
date: 2014-09-25
---

###### [Issue 1.1, Spring 2009: Defoe 2.0.](http://english.illinoisstate.edu/digitaldefoe/archive/spring09/index.shtml)

###### [Issue 2.1, Spring 2010: Strangers, Gods, & Monsters.](http://english.illinoisstate.edu/digitaldefoe/archive/spring10/index.shtml)

###### [Issue 3.1, Fall 2011: 18th-Century Studies & the State of Education.](http://english.illinoisstate.edu/digitaldefoe/archive/fall11/index.shtml)

###### [Issue 4.1, Fall 2012: Repositioning Defoe.](http://english.illinoisstate.edu/digitaldefoe/archive/fall12/index_fall12.shtml)

###### [Issue 5.1, Fall 2013: Public Intellectualism & 18th-Century Studies.](http://english.illinoisstate.edu/digitaldefoe/archive/fall13/index_fall13.shtml)

###### [Issue 6.1, Fall 2014: English Poetry, 1690 - 1720.](http://digitaldefoe.org/?page_id=86)

###### [Issue 7.1, Fall 2015: Speed & Sensation in the Eighteenth Century.](http://digitaldefoe.org/issue-7-1-fall-2015)

###### [Issue 8.1, Fall 2016](http://digitaldefoe.org/issue-8-1-fall-2016/)

###### [Issue 9.1, Fall 2017](http://digitaldefoe.org/issue-9-1-fall-2017/)

###### [Issue 10.1, Fall 2018](http://digitaldefoe.org/issue-10-1-fall-2018/)

###### [Issue 11.1, Fall 2019](http://digitaldefoe.org/issue-11-1-fall-2019)

###### [Issue 12.1, Fall 2020](http://digitaldefoe.org/issue--12-1-fall-2020/)

###### [Issue 13.1, Fall 2021](https://digitaldefoe.org/issue-13-1-fall-2021/)