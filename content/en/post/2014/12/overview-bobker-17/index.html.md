---
layout: post
title: "Gothic Collections, Gothic Chambers - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<span class="style1 style12">**Reading: ➢	Horace Walpole, _Castle of Otranto_**</span>

 <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
              <td rowspan="2" height="801" valign="top" width="28%" align="right">

<span class="style43 style44">[![Strawberry Hill, the Seat](24StrawberryHillSeatthumb.jpg)](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/24StrawberryHillSeat.jpg)&nbsp;  

              </span><span class="style45"><span class="style1 style46">Fig. 21. Edward Dayes, Strawberry Hill, the Seat of the Honourable Horace Walpole. Courtesy of The Lewis Walpole Library, Yale University.</span>  

              </span>

[![Gallery at Strawberry Hill](26StrawberryHillGallerythumb.jpg)](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/26StrawberryHillGallery.jpg)

<span class="style45"><span class="style1 style46">Fig. 23. Gallery at Strawberry Hill.  

                Courtesy of The Lewis Walpole Library, Yale University.           </span></span>

[![Strawberry Hill Library](27StrawberryHillLibrarythumb.jpg)](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/27StrawberryHillLibrary.jpg)

<span class="style45"><span class="style1 style46"><span class="style1 style46">Fig. 24. Library at Strawberry Hill.  

                Courtesy of The Lewis Walpole Library, Yale University.           </span></span></span>

[![The Cabinet](28TheCabinetthumb.jpg)](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/28TheCabinet.jpg)

<span class="style45"><span class="style1 style46"><span class="style1 style46">Fig. 25. The Cabinet.  

              Courtesy of The Lewis Walpole Library, Yale University.</span></span></span>
</td>
              <td colspan="2" height="879" valign="top">

[![Strawberry Hill, Before and After](25StrawberryHillthumb.jpg)](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/25StrawberryHillbeforeafter.jpg)

<span class="style1 style47">Fig. 22: Strawberry Hill, Before and After  

                  Courtesy of The Lewis Walpole Library, Yale University.
                </span>

<span class="style1 style12">  

                  In our last week of the course we explore the influence of Horace Walpole's eclectic tastes on the genre of the Gothic novel he invented. Walpole's continual renovations of his estate, Strawberry Hill ([Figures 21](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/24StrawberryHillSeat.jpg) and [22](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/25StrawberryHillbeforeafter.jpg)), reflected his passion not only for feudal architecture but also for his own eccentric collections of antique coins, old and contemporary paintings, and antiquarian curios including Mary Tudor's hair in a gold locket, Cardinal Wolsey's red hat, and an ivory comb from the twelfth century. Walpole was not interested in the empirical systems of classification privileged by many eighteenth-century collectors. Instead he was concerned with immediate affective and imaginative charge of medieval material culture-especially its delightful dreariness, or "gloomth" as he called it-and he went to great lengths to create interior settings appropriate for the display of the things he loved ([Figures 23](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/26StrawberryHillGallery.jpg), [24](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/27StrawberryHillLibrary.jpg), [25](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/28TheCabinet.jpg), and [26](29GBRStawHill.jpg)).   

                  In the introduction to _Castle of Otranto_, Walpole writes that his inspiration for the novel came from a dream he had had about the medieval suit of armor he kept in the main staircase at Strawberry Hill ([Figure 27](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/30StrawberryHillStaircase.jpg)). We approach the novel as the literary corollary of Walpole's unorthodox antiquarianism. In particular, we pay close attention to moments where the very modern immediacy of characterization and dialogue bump up against the romantic plot, settings, and "properties"-such as Mathilda and Isabelle's late-night exchange about their shared attraction for Theodore, for example. Ultimately, we focus on the ideological complexity of Walpole's Gothicism. How is the novel's melodramatic resolution a reflection of this ideological complexity? It seems clear that Walpole's nostalgia is for the surfaces and style of Europe's feudal past, rather than its top-down political and religious institutions. Does he succeed in showing his appreciation for the former but not the latter? Another favorite topic of conversation for students is the relationship between Walpole's homosexuality and his taste, which we might now label as campy or kitschy.   

  **  

  Suggested Presentation Topics:**  

                  Gothic architecture   

                  Strawberry Hill and/or Walpole as collector  

                  Cynthia Wall, "Writing Things" in _The Prose of Things: Transformations of Description in the Eighteenth Century_  

                  Susan Sontag, "Notes on Camp" from _Against Interpretation and Other Essays_  

                Walpole's closet drama, _The Mysterious Mother_</span>

&nbsp;

              </td>
            </tr>
            <tr>
              <td valign="top" width="32%" align="center">

[![GBR Stawberry Hill](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/29GBRStawHill.jpg)](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/29GBRStawHill.jpg)

<span class="style45"><span class="style1 style46"><span class="style1">Fig. 26. Beauclerk Closet, Strawberry Hill.  

                © World Monuments Fund.           </span></span></span>

              </td>
              <td valign="top" width="40%" align="center">

[![Strawberry Hill Staircase](30StrawberryHillStaircasethumb.jpg)](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/30StrawberryHillStaircase.jpg)

<span class="style45">Fig. 27. Staircase at Strawberry Hill.  

              Courtesy of The Lewis Walpole Library, Yale University.             </span>
</td>
            </tr>
          </tbody></table>

         <span class="style1 style12"></span>