---
layout: post
title: "Image Gallery - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<table class="imagetable" border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="center" valign="top" width="20%">

[![Green Closet](1GreenClosetthumb.png)](http://digitaldefoe.org/wp-content/files/teaching/bobker/1GreenCloset.png)

</td>
<td align="center" valign="top" width="20%">[![](2closet.png)](http://digitaldefoe.org/wp-content/files/teaching/bobker/2 - closet, n. - Oxford English Dictionary copy.pdf)</td>
<td align="center" valign="top" width="20%">[ ![](3closet.png)](http://digitaldefoe.org/wp-content/files/teaching/bobker/3 - closet, v. - Oxford English Dictionary.pdf)</td>
<td align="center" valign="top" width="20%">[ ![](http://digitaldefoe.org/wp-content/files/teaching/bobker/Figure 4.jpg)](Figure 4.jpg)</td>
</tr>
<tr>
<td valign="top" height="36">
<div align="center">

Fig. 1. _Green Closet, Frogmore_ © British Library Board, 747.f.3, volume 2, plate opposite page 19.

</div></td>
<td valign="top">
<div align="center">

Fig. 2. _closet,_ n. _Oxford English Dictionary_.

</div></td>
<td valign="top">
<div align="center">

Fig. 3. _closet,_ v. _Oxford English Dictionary._

</div></td>
<td valign="top">
<div align="center">

Fig. 4. The axis of honour in the formal house. From Mark Girouard, _Life in the English Country House_, 145. Courtesy of Yale University Press.

</div></td>
</tr>
<tr>
<td align="center" valign="top" height="84">[![Cromwell Closeted](5CromwellClosetedthumb.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/5CromwellCloseted.jpg)</td>
<td align="center" valign="top">
<div align="center">[![](6thumb.png)](http://digitaldefoe.org/wp-content/files/teaching/bobker/6LongEighteenthCChronologycopy.pdf)</div></td>
<td align="center" valign="top">[![View Through a House](7ViewthroughaHousethumb.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/7ViewthroughaHouse.jpg)</td>
<td align="center" valign="top">[![](http://digitaldefoe.org/wp-content/files/teaching/bobker/8a.jpg)](8a.jpg)</td>
</tr>
<tr>
<td align="right" valign="top" height="36">
<div align="center">

Fig. 5. Patrick Allan-Fraser, _Oliver Cromwell Closeted with the Spy_. Courtesy of the collection of The Patrick Allan-Fraser, Hospitalfield Trust.

</div></td>
<td valign="top">
<div align="center">

Fig. 6. Long eighteenth-century timeline of English political and cultural events.

</div></td>
<td valign="top">
<div align="center">

Fig. 7. Samuel van Hoogstraten, _View of a Corridor_ © National Trust.

</div></td>
<td valign="top">
<div align="center">

Fig. 8a. Longleat House, 1570. From Michael McKeon, _Secret History of Domesticity_, 253. Marquand Library, Princeton University Library.

</div></td>
</tr>
<tr>
<td align="center" valign="top" height="84">[![](http://digitaldefoe.org/wp-content/files/teaching/bobker/8b.jpg)](8b.jpg)</td>
<td align="center" valign="top">[![Warner](http://digitaldefoe.org/wp-content/files/teaching/bobker/9warner.png)](9warner.png)</td>
<td align="center" valign="top" height="84">[![](http://digitaldefoe.org/wp-content/files/teaching/bobker/10.jpg)](10.jpg)</td>
<td align="center" valign="top">[![Miss Hobart and Miss Temple](http://digitaldefoe.org/wp-content/files/teaching/bobker/11misshobart.png)](11misshobart.png)</td>
</tr>
<tr>
<td align="right" valign="top" height="30">
<div align="center">

Fig. 8b. Longleat House, c1809. From Michael McKeon, _Secret History of Domesticity_, 254. Marquand Library, Princeton University Library. By permission of Oxford University Press.

</div></td>
<td valign="top">
<div align="center">

Fig. 9. Public and Private. Michael Warner, _Publics and Counterpublics_, 29-30.

</div></td>
<td valign="top">
<div align="center">

Fig. 10. Private and public. Jürgen Habermas, _The Structural Transformation of the Public Sphere_, 30.

</div></td>
<td valign="top">
<div align="center">

Fig. 11. C. Delort, Miss Hobart and Miss Temple. From Anthony Hamilton, _Memoirs of the Count de Grammont_, Walter Scott, ed. (New York: Dutton, 1905).

</div></td>
</tr>
<tr>
<td align="center" valign="top" height="73">[![Miss Hobart and Miss Temple](http://digitaldefoe.org/wp-content/files/teaching/bobker/12misshobartmisstemple.png)](12misshobartmisstemple.png)</td>
<td align="center" valign="top" height="73">[![](http://digitaldefoe.org/wp-content/files/teaching/bobker/13.jpg)](13.jpg)</td>
<td align="center" valign="top">[![](http://digitaldefoe.org/wp-content/files/teaching/bobker/14genevieve0675.jpg)](14genevieve0675.jpg)</td>
<td align="center" valign="top">[![The Cabinet Maker](http://digitaldefoe.org/wp-content/files/teaching/bobker/15cabinetmaker.jpg)](15cabinetmaker.jpg)</td>
</tr>
<tr>
<td align="right" valign="top" height="36">
<div align="center">
<div align="center">

Fig. 12. L. Boisson after C. Delort, Miss Hobart and Miss Temple. From Anthony Hamilton, _Memoirs of the Count de Grammont_, Henry Vizetelly, ed. (London: Vizetelly, 1889).

</div>
</div></td>
<td valign="top">
<div align="center">

Fig. 13. _Enter into thy Closet_. Frontispiece from Edward Wettenhall, _Enter into they Closet_.

</div></td>
<td valign="top">
<div align="center">

Fig. 14. Franz Ertinger, _Le Cabinet de la Bibliothèque de Sainte Geneviève_ © The Warburg Institute -- University of London.

</div></td>
<td valign="top">Fig. 15. _The Cabinet Maker_ © British Library Board, RB.23.a.18153 plate opposite 73.</td>
</tr>
<tr>
<td align="center" valign="top" height="14">[![](16closetswowalls.png)](http://digitaldefoe.org/wp-content/files/teaching/bobker/closetswithoutwalls.xlsx)</td>
<td align="center" valign="top">[![Rich Cabinet](18richcabinetthumb.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/18richcabinet.pdf)</td>
<td align="center" valign="top">[![](http://digitaldefoe.org/wp-content/files/teaching/bobker/18.jpg)](18.jpg)</td>
<td align="center" valign="top">[![Definition of Closet](22closet.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/22.pdf)</td>
</tr>
<tr>
<td valign="top">
<div align="center">

Fig. 16. _Closet Without Walls_ a bibliography.

</div></td>
<td valign="top">
<div align="center">

Fig. 17. _A Rich Cabinet_. Frontispiece of _A Rich Cabinet_.

</div></td>
<td valign="top">
<div align="center">Fig. 18. A plaine pot of a privie in perfection, John Harington, _Metamorphosis of Ajax_, 196.</div></td>
<td valign="top">
<div align="center">

Fig. 19. _closet_, sb. Eve Kosofsky Sedgewick, _Epistemology of the Closet_, 65.

</div></td>
</tr>
<tr>
<td align="center" valign="top" height="14">[![](http://digitaldefoe.org/wp-content/files/teaching/bobker/20armoire.jpg)](20armoire.jpg)</td>
<td align="center" valign="top">[![Strawberry Hill Seat](24StrawberryHillSeatthumb.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/24StrawberryHillSeat.jpg)</td>
<td align="center" valign="top">[![Strawberry Hill Before and After](25StrawberryHillthumb.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/25StrawberryHillbeforeafter.jpg)</td>
<td align="center" valign="top">[![Strawberry Hill Gallery](26StrawberryHillGallerythumb.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/26StrawberryHillGallery.jpg)</td>
</tr>
<tr>
<td align="right" valign="top" height="31">
<div align="center">

Fig. 20. Jean-Honore Fragonard, _L'Armoire (The Closet)_ © The Metropolitan Museum of Art. Image source: Art Resource, NY.

</div></td>
<td valign="top">
<div align="center">

Fig. 21. Edward Dayes, Strawberry Hill, the Seat of the Honourable Horace Walpole. Courtesy of The Lewis Walpole Library, Yale University.

</div></td>
<td valign="top">
<div align="center">

Fig. 22. Strawberry Hill, Before and After. Courtesy of The Lewis Walpole Library, Yale University.

</div></td>
<td valign="top">
<div align="center">

Fig. 23. Gallery at Strawberry Hill. Courtesy of The Lewis Walpole Library, Yale University.

</div></td>
</tr>
<tr>
<td align="center" valign="top" height="14">[![Strawberry Hill Library](27StrawberryHillLibrarythumb.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/27StrawberryHillLibrary.jpg)</td>
<td align="center" valign="top">[![The Cabinet](28TheCabinetthumb.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/28TheCabinet.jpg)</td>
<td align="center" valign="top">[![GBR Strawberry Hill](http://digitaldefoe.org/wp-content/files/teaching/bobker/29GBRStawHill.jpg)](29GBRStawHill.jpg)</td>
<td align="center" valign="top">[![Strawberry Hill Staircase](30StrawberryHillStaircasethumb.jpg)](http://digitaldefoe.org/wp-content/files/teaching/bobker/30StrawberryHillStaircase.jpg)</td>
</tr>
<tr>
<td align="right" valign="top" height="14">
<div align="center">

Fig. 24. Library at Strawberry Hill. Courtesy of The Lewis Walpole Library, Yale University.

</div></td>
<td valign="top">
<div align="center">

Fig. 25. The Cabinet. Courtesy of The Lewis Walpole Library, Yale University.

</div></td>
<td valign="top">
<div align="center">

Fig. 26. Beauclerk Closet, Strawberry Hill © World Monuments Fund.

</div></td>
<td valign="top">
<div align="center">

Fig. 27. Staircase at Strawberry Hill. Courtesy of The Lewis Walpole Library, Yale University.

</div></td>
</tr>
</tbody>
</table>
&nbsp;