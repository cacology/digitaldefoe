---
layout: post
title: "Paying for Poetry at the Turn of the Eighteenth Century, with Particular Reference to Dryden, Pope, and Defoe "
date: 2014-11-18
tags: ["Issues","Article Notes","Issue 6.1 - Fall 2014"]
---

## NOTES

      I would like to thank James Woolley for his advice and assistance in the preparation of this essay. Stuart Gillespie, Paul Hammond, David Hopkins, Rob Hume, Paul Hunter and Pat Rogers were also kind enough to respond to my queries.   

  <a name="1" id="1"></a>1. It should be noted that Professor Barnard has acknowledged that he has now revised his "unwise statement, made in 1963, that the _Virgil_ was 'through and through a commercial venture'" ("Patrons" 174).   

            [_Return to main text._](http://108.167.133.27/~keellis/?p=73#main1)  

        <a name="2" id="2"></a>2. Until the value of the guinea was fixed at 21 shillings by royal proclamation in December 1717, its value fluctuated. At the accession of George I, it was worth about 21 shillings and sixpence.   

        _[Return to main text.](http://108.167.133.27/~keellis/?p=73#main2)_  

        <a name="3" id="3"></a> 3. It should be borne in mind, however, that if the second subscribers are taken into account, Dryden's Virgil attracted 349 subscribers, two of whom, as Barnard points out, were both first and second subscribers ("Patrons" 180n22).   

        _[Return to main text.](http://108.167.133.27/~keellis/?p=73#main3)_ 

        <a name="4" id="4"></a> 4. _A Second Volume of the Writings of the Author of the True-Born Englishman. Some whereof never before printed_, sig. A3r.    

        [_Return to main text._](http://108.167.133.27/~keellis/?p=73#main4)  

        <a name="5" id="5"></a> 5.  Charles Montagu's well-received _Epistle to Dorset_, "one of the most widely-praised Whig poems" of the 1690s, according to Abigail Williams, cost sixpence (173).   

        [_Return to main text._](http://108.167.133.27/~keellis/?p=73#main5)  

        <a name="6" id="6"></a>6. _A True Collection of the Writings of the Author of the True Born English-man[.] The Second Edition Corrected and Enlarg'd by himself_, sig. A3v.   

        [_Return to main text._](http://108.167.133.27/~keellis/?p=73#main6)  

          <a name="7" id="7"></a>7. All the quotations in this paragraph are taken from the undated folio half-sheet, _The Case of the Booksellers Right to their Copies, or Sole Power of Printing their Respective Books, represented to Parliament_, which was almost certainly published to inform the parliamentary debate on copyright which led to the Copyright Act of 1709. It is interesting that the author should maintain that "the greatest Charge in Printing is setting the Letters together," as opposed to the cost of paper, which was extremely expensive, unless, that is, he was speaking about the cost _after_ the paper had been purchased.  

        [_Return to main text._](http://108.167.133.27/~keellis/?p=73#main7)  

          <a name="8" id="8"></a>8. The invitation is to be found in the June 1693 issue of _The Gentleman's Journal: or the monthly Miscellany. In a Letter to a Gentleman in the Country. Consisting of News, History, Philosophy, Poetry, Musick, Translations, &c_., vol. 3, 195.    

        [_Return to main text._](http://108.167.133.27/~keellis/?p=73#main8)  

          <a name="9" id="9"></a>9. Johnson makes the remark in the _Adventurer_ no. 115 (December 11, 1753): "The present age, if we consider chiefly the state of our own country, may be stiled with great propriety THE AGE OF AUTHORS; for, perhaps, there never was a time, in which men of all degrees of ability, of every kind of education, of every profession and employment, were posting with ardour so general to the press."    

        [_Return to main text._](http://108.167.133.27/~keellis/?p=73#main9)  

          <a name="10" id="10"></a>10. The most recent contribution to the debate is Dustin Griffin's _Authorship in the Long Eighteenth Century_ (2014), esp. Chapter 11, "The Rise of the Professional Author?," but Brean Hammond's _Professional Imaginative Writing in England 1670-1740_ (1997) should also be consulted.   

        [_Return to main text._](http://108.167.133.27/~keellis/?p=73#main10)  

###   

          <a name="works" id="works"></a>WORKS CITED

      Anon. _The Case of the Booksellers Right to their Copies, or Sole Power of Printing their Respective Books, represented to Parliament_. London: no publisher, no date. Print.   

Astbury, Raymond. "The Renewal of the Licensing Act in 1693 and its Lapse in 1695." _The Library_ 33.2 (1978): 296-322. Print.   

Backscheider, Paula R. _Daniel Defoe: His Life_. Baltimore and London: Johns Hopkins UP, 1989. Print.   

Barnard, John. "Dryden, Tonson, and the Patrons of _The Works of Virgil_ (1697)." _John Dryden: Tercentenary Essays_. Ed. Paul Hammond and David Hopkins. Oxford: Clarendon P, 2000. 174-239. Print.   

---. "Dryden, Tonson, and the Subscriptions for the 1697 _Virgil_." _Papers of the Bibliographical Society of America_ 57.2 (1963): 127-51. Print.   

Defoe, Daniel. _Jure Divino: A Satyr_. London, 1706. Print  

--. _Review_. Ed. John McVeagh. 9 vols. London: Pickering & Chatto, 2003-2011. Print.   

--.  _A Second Volume of the Writings of the Author of the True-Born Englishman. Some whereof never before printed_. London: Printed, and Sold by the Booksellers, 1705. Print.   

--.  _A True Collection of the Writings of the Author of the True Born English-man[.] The Second Edition Corrected and Enlarg'd by himself_. London: Printed, and are to be Sold by most Booksellers in London and Westminster, 1705. Print.   

Downie, J. A. "Printing for the Author in the Long Eighteenth Century." _British Literature and Print Culture_. Ed. Sandro Jung. Cambridge: D. S. Brewer, 2013. 58-77. Print.   

Dryden, John. _Examen Poeticum: Being the Third Part of Miscellany Poems, Containing Variety of New Translations of the Ancient Poets. Together with many Original Copies, by the Most Eminent Hands_. London: Jacob Tonson, 1693. Print.   

--. _The Letters of John Dryden_. Ed. Charles E. Ward. Durham, N. C.: Duke UP, 1942. Print [Abbreviated _Letters_].   

Dunton, John. _The Athenian Oracle: Being an Entire Collection Of all the Valuable Questions and Answers in the Old Athenian Mercuries ... By a Member of the Athenian Society_. 3 vols. London: Printed for A. Bell, 1703-04. Print.   

Foxon, D. F. _English Verse 1701-1750: A Catalogue of separately printed poems with notes on contemporary collected editions_. 2 vols.  Cambridge: Cambridge UP, 1975. Print.   

--. _Pope and the Early Eighteenth-Century Book Trade_. Revised and ed. James McLaverty. Oxford: Clarendon P, 1991. Print.   

Griffin, Dustin. _Authorship in the Long Eighteenth Century_. Newark, DE: U of Delaware P, 2014. Print.   

Habermas, Jürgen. _The Structural Transformation of the Public Sphere: An Inquiry into a Category of Bourgeois Society_. Trans. T. Burger with the assistance of Frederick Lawrence. Cambridge: Polity P, 1989. Print.   

Hammond, Brean. _Professional Imaginative Writing in England 1670-1740: "Hackney for Bread." _ Oxford: Clarendon P, 1997. Print.   

Hunter, J. Paul. "Political, satirical, didactic and lyric poetry (I): from the Restoration to the death of Pope." _The Cambridge History of English Literature, 1660-1780_. Ed. John Richetti. Cambridge: Cambridge UP, 2005. 160-208. Print.   

Lockwood, Thomas. "Subscription Hunters and their Prey." _Studies in the Literary Imagination_ 34.1 (2001): 121-35. Print.   

McLaverty, James. "The Contract for Pope's Translation of Homer's _Iliad_: An Introduction and Transcription." _The Library_ 15.1 (1993): 206-25. Print.   

Motteux, Peter Anthony. _The Gentleman's Journal: or the monthly Miscellany. In a Letter to a Gentleman in the Country. Consisting of News, History, Philosophy, Poetry, Musick, Translations, &c_. London: Printed; and are to be sold by R. Baldwin, 1692-94. Print.   

Pope, Alexander. _The Rape of the Lock. An Heroi-Comical Poem. In Five Canto's ... The Third Edition_. London: Bernard Lintott, 1714. Print.   

Rogers, Pat. "Pope and His Subscribers." _Publishing History_ 3 (1978): 7-36. Print.   

Rowe, Elizabeth Singer. _Philomela: Or, Poems by Mrs. Elizabeth Singer, [Now Rowe,] Of Frome in Somersetshire. The Second Edition_. London: E. Curll, 1737. Print.   

--. _Poems On Several Occasions. Written by Philomela_. London: John Dunton, 1696. Print.   

Sherburn, George. "Letters of Alexander Pope, Chiefly to Sir William Trumbull." _Review of English Studies_ 9 (1958): 388-406. Print.   

St Clair, William. _The Reading Nation in the Romantic Period_. Cambridge: Cambridge UP, 2004. Print.   

Tonson, Jacob. _The Annual Miscellany: For The Year 1694: Being the Fourth Part of Miscellany Poems_. London: Jacob Tonson, 1694. Print.   

Williams, Abigail. _Poetry and the Creation of a Whig Literary Culture 1681-1714_. Oxford: Oxford UP. 2009. Print.   

Winn, James Anderson. _John Dryden and His World_. New Haven and London: Yale UP, 1987. Print.

      