---
layout: post
title: "Issue 14.1, Fall 2022"
date: 2022-12-29
---

<div class="issue_home_headers">Features</div>
<div>

[![](crusoe-284x300.jpg)](https://digitaldefoe.org/wp-content/uploads/2022/12/crusoe.jpg)

[catlist id=58 customfield_display="article_author" customfield_display_name=no]

</div>
<div class="issue_home_headers">Distinguished Scholar's Comment</div>
<div>

[![](https://digitaldefoe.org/wp-content/uploads/2022/12/coin2-3.jpg)](coin2-3.jpg)

&nbsp;

[catlist id=59 customfield_display="article_author" customfield_display_name=no]

&nbsp;

</div>
<div></div>
<div></div>
<div class="issue_home_headers">Reviews</div>
[catlist id=57 customfield_display="article_author" customfield_display_name=no]

&nbsp;