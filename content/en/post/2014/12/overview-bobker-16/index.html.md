---
layout: post
title: "(Homo)Erotic Closets - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<span class="style1 style12">**Reading: ➢	John Cleland, _Memoirs of a Woman of Pleasure _ **</span>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
              <td height="922" valign="top" width="27%" align="right">

<span class="style43 style44">[![Eve Kosofsky Sedgwick, Epistemology of the Closet](22closet.jpg)](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/22.pdf)</span>

<span class="style1 ">Fig. 19._ closet_, sb.   

                Eve Kosofsky Sedgwick, _Epistemology of the Closet_, 65</span>.

&nbsp;

[![](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/20armoire.jpg)](20armoire.jpg)

                <div align="center">

<span class="style47">Fig. 20. Jean-Honoré Fragonard, _L'Armoire (The Closet)_   

© The Metropolitan Museum of Art. Image source: Art Resource, NY</span><span class="style1 style46">.</span> 

              </div>              </td>
              <td valign="top" width="73%">

<span class="style1 style12">

                 John Cleland's _Memoirs of a Woman of Pleasure_, the most famous English pornographic novel, focuses our attention on the erotics of privacy, and the network of associations linking privacy, sincerity, and sex. Fanny Hill announces on the first page that her narrative will present "stark, naked truth": "I will not so much as take the pains to bestow the strip of a gauze wrapper on it, but paint situations such as they actually rose to me in nature..." Significantly, she defends the decorousness of her sexual explicitness with reference to domestic space: "The greatest men, those of the first and most leading taste, will not scruple adorning their private closets with nudities, though, in compliance with vulgar prejudices, they may not think them decent decorations of the staircase, or saloon" (1).   

Throughout the novel, not only do people have sex in closets and similarly enclosed spaces, but such rooms also give shape to formative solitary sexual experiences. Notably, Fanny Hill is introduced to heterosexual intercourse by spying from a closet on Mrs Brown, her first madam, and a young soldier (24), and then on Polly, one of her brothel sisters, and an Italian merchant (28). We ask if and how Cleland's depictions of sexual voyeurism seem to serve a metatextual function akin to scenes of reading in other novels. That is, do Cleland's scenes of virtual intimacy also serve to clarify the kind of vicarious learning the author wants his readers to do? The end of the novel provides an important focal point for musing on the novel's apparently contradictory lessons about sex and propriety. Ultimately Fanny claims that her experiences as a prostitute have made it possible for her to recognize the morally _and_ sensually superior pleasures of the reproductive matrimonial bed. For many critics Cleland's turn to married love and virtue in what Fanny calls her "tail-piece of morality" (187) is a cheap parody of the expected finale of the domestic novel. This skepticism may seem less warranted if we recognize the extent to which Cleland has tried to distinguish Fanny's reunion with Charles, her husband-to-be, from all the sexual encounters that have preceded it (181-186). Especially striking in this regard is Cleland's metaphor aligning Charles' penis with a maternal breast at which infants "in the motion of their little mouths and cheeks... extract the milky stream prepar'd for their nourishment."   

We go on to consider the novel as a cabinet of sexual curiosities in which a wide range of sexual practices, including virgin hunting, flagellation, hair and glove fetishes, and sodomy, is gathered and displayed. While Fanny's rhetoric of "taste" and "universal pleasure" accommodates this range (see especially 144), Cleland also links certain practices to social and/or physiological deficiencies. Indeed he often reinforces a new tendency in the period to turn on its head the traditional idea of good blood: the sexual taste of the aristocracy comes off as especially depraved. The publication and reception history of _Memoirs of a Woman of Pleasure_, succinctly summarized in Peter Sabor's 2000 review essay, particularly highlights the importance and complexity of the novel's oft-censored sodomitical theme. On the one hand, sex between men was virulently condemned in the period and Cleland's novel echoes some of the dehumanizing rhetoric associated with this condemnation. On the other hand, there is strong evidence that Cleland's own sexual preference was for men: as David Robinson discusses in his chapter on Cleland in his _Closeted Writing and Gay and Lesbian Literature_, it may make most sense to read this text as sympathetic to sodomites though in a roundabout way.  

                 </span>

                </td>
            </tr>
          </tbody></table>

         <span class="style1 style12">

 Finally, the opening chapter of Eve Kosofsky Sedgwick's _Epistemology of the Closet_ provides a springboard for a conversation about the queer closet, then and now. The private domestic space became our most common metaphor for queer secrecy and shame with the gay and lesbian liberation movements of the 1960s and 70s. How did this special signification of closet take root and what are the implications of this term's use in this context? Sedgwick opens some doors for speculating about the etymology of the queer closet with the selection of _OED_ definitions she includes at the start of her _Epistemology of the Closet_ ([Figure 19](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/22.pdf)). To Sedgwick's suggestions, we add others that seem relevant from the complete _OED_ entries for _closet_ ([Figures 2](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/2 - closet, n. - Oxford English Dictionary copy.pdf) and [3](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/3 - closet, v. - Oxford English Dictionary.pdf)). Definition 3d. of _closet_, n., is especially relevant here, as is definition 1c., which suggests that one historical bridge to our current metaphor may have been the use of the closet as a symbol of a negative, stifling attachment to privacy. Jean-Honoré Fragonard's painting, _L'Armoire_ (translated as _The Closet_) ([Figure 20](http://108.167.133.27/~keellis/wp-content/files/teaching/bobker/20armoire.jpg)), points up the basic spatial connection between the closet and the bad feelings following illicit experiences: near the bed and large enough to hide a lover, the freestanding wardrobe was a logical symbol of sexual shame.   

**Suggested Presentation Topics:**   

The history of pornography   

Peter Sabor, "From Sexual Liberation to _Gender Trouble_: Reading the _Memoirs of a Woman of Pleasure_ from the 1960s to the 1990s"   

Thomas Laqueur, _Solitary Sex: The Cultural History of Masturbation_   

Eve Kosowsky Sedgwick, "Introduction: Axiomatic" in _Epistemology of the Closet_   

David Robinson, "The Closeting of Closeting: Cleland, Smollett, Sodomy, and the Critics" in _Closeted Writing and Lesbian and Gay Literature: Classical, Early Modern, Eighteenth-Century _ </span>