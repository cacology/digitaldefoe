---
layout: post
title: "The Eighteenth Century / The Closet: Two Introductions - Bobker"
date: 2014-12-04
tags: ["Issues","Issue 6.1 - Fall 2014","Pedagogies"]
---

<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td valign="top"><span class="style1 style12">**Reading: ➢ _closet_, [n](http://digitaldefoe.org/wp-content/files/teaching/bobker/2 - closet, n. - Oxford English Dictionary copy.pdf). [v](http://digitaldefoe.org/wp-content/files/teaching/bobker/3 - closet, v. - Oxford English Dictionary.pdf). _Oxford English Dictionary_ **</span>
<table border="0" width="340px" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 344px; text-align: left; vertical-align: top;" valign="top" width="324px">

[![Green Closet, Frogmore](http://digitaldefoe.org/wp-content/files/teaching/bobker/5CromwellClosetedthumb.jpg)](5CromwellClosetedthumb.jpg)

<span class="style1 style 12">Fig. 5. Patrick Allan-Fraser, _Oliver Cromwell Closeted with the Spy_.
Courtesy of the collection of The Patrick Allan-Fraser, Hospitalfield Trust.</span>

</td>
<td valign="top"><span class="style1 style12">The course begins with separate introductions to the closet and eighteenth-century history. Then students are invited to start to connect the two. Perusing _OED_ definitions and citations for _[closet](http://digitaldefoe.org/wp-content/files/teaching/bobker/2 - closet, n. - Oxford English Dictionary copy.pdf)_[, noun](http://digitaldefoe.org/wp-content/files/teaching/bobker/2 - closet, n. - Oxford English Dictionary copy.pdf) (Figure 2) and _[closet](http://digitaldefoe.org/wp-content/files/teaching/bobker/3 - closet, v. - Oxford English Dictionary.pdf)_[, verb](http://digitaldefoe.org/wp-content/files/teaching/bobker/3 - closet, v. - Oxford English Dictionary.pdf) (Figure 3) as well as two paintings of seventeenth- and eighteenth-century English closets ([Figures 1](http://digitaldefoe.org/wp-content/files/teaching/bobker/1GreenCloset.png) and [5](http://digitaldefoe.org/wp-content/files/teaching/bobker/5CromwellCloseted.jpg)) brings many of its now obsolete inflections into view. In this period a closet could be a room for private prayer (1b), family worship (2b), or quiet study (1c), a small bedroom or antechamber (4), a dressing room "for a lady to make her redy in" (1a), a water closet (7), sewer (9), or a repository for valuables or cabinet of curiosities (3a). The closet's role as a site and symbol of politicized intimacies is important throughout the course: a schematic floor plan of an early modern household ([Figure 4](http://digitaldefoe.org/wp-content/files/teaching/bobker/Figure 4.jpg)) helps to make sense of the unique privacy and social capital of this room, filling out definition 2a: "The private apartment of a monarch or potentate; the private council-chamber." The storage function of the marginal architectural spaces we now call closets (3b) has a long history as well. At the end of the eighteenth century, when Jane Austen observed "[a] Closet full of shelves... should... be called a Cupboard rather than a Closet," she was acknowledging, and hoping to curtail, the semantic reduction of the word to this strictly functional space. </span></td>
</tr>
</tbody>
</table>
We also explore the use of the word as a general metaphor for privacy and seclusion. Some of these metaphors are negatively charged: closet as a marker of "mere theories as opposed to practical measures" (1c) or of painful, shameful secrets, including, especially since the late 1960s, secrets about one's sexuality (3c, 3d, and 10b). Other metaphors are more neutral: closet as an analogy for a hidden interior site-"the Closet of your Conscience" (6b)-or as an adjective that qualifies a particular experience or thing as inward-"closet-sins" as opposed to "stage-sins" (10a). It is not surprising that, as the private room known by this name proliferated in English culture, closet began regularly to be used as a verb meaning "to retreat," whether alone or-as in the title of Allan-Fraser's painting ([Figure 5](http://digitaldefoe.org/wp-content/files/teaching/bobker/5CromwellCloseted.jpg))-with another person.

With reference to such events as the Glorious Revolution, the lapse of the Print Licensing Act, and the founding of the Royal Society and the Bank of England listed on a timeline ([Figure 6](http://digitaldefoe.org/wp-content/files/teaching/bobker/6LongEighteenthCChronologycopy.pdf)), my opening lecture characterizes the long eighteenth century as a period of gradual, uneven transition-from absolutism to constitutional monarchy, a land- to commodity- and money-based economy, from manuscript to print culture, and from a court public to a modern public sphere. Then, turning back to the _OED_ definitions and citations, we consider in which of them the closet seems to encapsulate traditional values, in which of them progressive values, and in which a tension between the two. This collective interpretive work helps to ground a basic thesis of the course: that closets became central to eighteenth-century English discourse and culture because they were such flexible and such evocative spaces.</td>
</tr>
</tbody>
</table>
  